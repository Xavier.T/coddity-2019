# Challenge Coddity 2019 : Tech' it !

Réalisé par Xavier TREMILLON


---
## Présentation du projet
**Avant de démarrer, soyez sûrs d'avoir lu la présentation détaillée du projet comprenant la documentation d'analyse ainsi que la documentation technique. Elles se trouvent dans le dossier /RenduFinal**


## L'application

### Accès direct

L'application Tech' it est actuellement en production sur un serveur web. Vous pouvez la consulter et l'utiliser sans limite ! Afin de tester toutes les fonctionnalités, je vous invite à utiliser l'application en tant qu'Explorateur/Exploratrice puis de devenir Ambassadeur/Ambassadrice pour partager des expériences professionnelles ! N'hésitez pas à tester la plateforme à plusieurs, il y a un super système de commentaires et de notifications en temps réel, ce serait dommage de s'en priver !

[Me rendre sur Tech' it !](https://techit.alwaysdata.net)

Lors de l'ouverture du site, il se peut que des problèmes de connexion surviennent, le serveur d'hébergement est une offre gratuite qui ne garantit donc pas des performances incroyables. De ce fait, l'application peut aussi parfois mettre du temps à répondre.

### Déploiement

Si vous souhaitez déployer l'application chez vous, voici la procédure à suivre.
L'application se compose d'une base de donnée MySQL, d'un serveur (back-end) et d'un client React (front-end).

Si vous n'êtes pas familié avec l'utilisation de base de données, utilisez la version en ligne de tech'it directement car le déploiement comporte pas mal d'étapes susceptibles de mal se dérouler.

En cas de difficultés vous pouvez me contacter par mail : xavier.tremillon.pro@gmail.com

#### La base de donnée MySQL


- Installer un client MySQL sur votre machine
- Créer une base de donnée MySQL (mon interclassement par défault est ***latin_swedish_ci***)
- Importer en utf-8 le fichier **techit_demo.sql** contenu dans **project/** dans la base de donnée
- Renommer le fichier **nodemon_example.json** en **nodemon.json** dans le dossier **/project/server**
- Pour l'instant, dans **nodemon.json**, remplissez les champs (vous les avez choisi en créant la base de donnée ):
  - DB_USER
  - DB_PASSWORD
  - DB_NAME
- Lancer le service MySQL, la base de donnée doit normalement être opérationnelle !

#### Le serveur web node.js

- Installez Node.js sur la machine locale.
- Créez une adresse gmail (fonctionnalités non testées avec d'autres services mail) qui servira de mailer au serveur de Tech' it.
- Autorisez l'accès pour les applications les moins sécurisées sur l'adresse mail crée [(Tuto ici)](https://support.google.com/a/answer/6260879?hl=fr).
- Mettre à jour la configuration du mail dans le fichier **nodemon.json** au niveau des champs :
  - MAIL_ADDRESS
  - MAIL_PASSWORD
- Dans le dossier **/project/server**, lancez la commande `npm install` puis, patientez.
- Lancez maintenant la commande `npm run start:dev`
- Des logs en rapport avec MySQL doivent apparaîtrent suivis d'un message
> Synchronisation Effectuée !

Pour des raisons de sécurité évidentes, le fichier **nodemon.json** original n'est pas disponible sur ce git. Par ailleurs, l'addresse e-mail utilisée ne sera jamais transmise sur le réseau internet. Il est conseillé de ne pas utiliser votre mail perso (Recevoir un mail de soi-même confirmant son inscription c'est étrange !).

#### L'application React

Si vous êtes arrivé ici, BRAVO, le plus dur est fait.

- Ouvrez un terminal différent de celui du serveur
- Placez vous dans le dossier **/project/client** puis lancez `npm install` et attendez (ça peut être long).
- Une fois l'installation des packages terminée, lancez `npm start`.

L'application doit normalement ouvrir une page web dans votre navigateur préféré !
Vous pouvez maintenant commencer à naviguer en local sur Tech' it !
