# Introduction

Le sujet proposé pour l’année 2019 s’intitule : Les femmes dans les métiers de la tech. 11 interviews ont été réalisées par les organisateurs du challenge auprès d’hommes et femmes acteurs de la tech. Ces interviews doivent nous permettre de formuler une problématique puis d’y répondre grâce à nos compétences tout en gardant en tête que le but est de promouvoir les femmes dans les métiers de la tech.

# Recherche de la problématique

La première phase du challenge est donc une phase d’analyse. Le sujet étant composé de 23 pages, il m’a semblé important de procéder méthodiquement afin de faire ressortir le plus d'éléments de ces interviews sans en oublier. Négliger certains aspects d’un sujet d’actualité comme celui-ci peut vite conduire à ne plus être objectif et à proposer une solution biaisée par une prise de position potentiellement extrême (féminisme ou masculinisme radical). La méthode que j’ai choisi d’appliquer a donc été de lire une première fois le sujet. On remarque assez vite que les thèmes abordés dans chaque entretien sont récurrents. Pour chacune des personnes, j’ai réécris en raccourcissant leur réponses aux différentes questions. Globalement, les questions sont par exemple : Quelles ont été vos difficultés ? Les hommes sont-ils plus légitimes dans les métiers de la tech ? Ensuite, j’ai regroupé toutes les réponses à chacune des questions ensemble pour avoir une vue globale des avis. Le rassemblement des réponses par thème (automatisé par un script python) se trouve dans le fichier PhaseConception/Analyse/Synthese_generee.txt de mon dépôt gitlab consacré au projet. Pour finir, j’ai résumé les réponses à chaque question dans le fichier resume_synthese_final.txt du même dossier. À partir là, j’ai ensuite pu déterminer une problématique.

Le sujet des femmes dans la tech est très vaste et complexe, une solution répondant à tous les problèmes d’un coup semble ambitieuse pour un projet de 2 mois. Ce que j’ai principalement retenu des entretiens, est que le milieu de la tech n’est pas assez bien expliqué aux jeunes générations. En effet la plupart des personnes sont tombés dans le domaine plus par hasard qu’autre chose. Certaines auraient d’ailleurs aimé bénéficier d’un mentor au début. Si l’on ajoute à cela les stéréotypes répandus concernant les acteurs de la tech (un homme solitaire derrière son ordinateur toute la journée et la nuit par exemple) et le fait que les femmes s’accorderaient moins le droit d’oser, de faire des erreurs que les hommes, alors on peut comprendre que le milieu soit moins attractif pour elles. Ce qui est dommage car la complémentarité homme-femme semblerait être un bon avantage au travail. Évidemment il y a d’autres facteurs en jeu, cependant, ce sont ceux qui ont le plus marqué mon esprit et auxquels je pense qu’il est important de réfléchir. À mon sens, les choix d’orientation des jeunes doivent pouvoir être totalement justifiés et non fondés sur des clichés.

Par ailleurs, la solution que je souhaites proposer pour le challenge devra aussi tenir compte du point suivant. S’il est avéré que les femmes n’ont pas spécialement d’attrait pour les métiers de la tech (ce qui est possible, on ne peut pas à l’heure actuelle affirmer que dans un monde parfaitement égalitaire il y aurait 50% de chacun des sexes dans chaque corps de métier) alors il ne faut pas forcer pour rentrer dans des quotas théoriques. En revanche, si c’est le cas, il faut très vite faire changer les mentalités pour permettre aux femmes de s’épanouir dans ces métiers. Pour l’instant, ce que l’on peut et que l’on doit accomplir, c’est faire prendre conscience à tous les jeunes sans exception de leur potentiel. De leur présenter de la manière la plus transparente possible toutes les orientations qu’ils peuvent embrasser, notamment dans la tech. La problématique retenue sera donc : **Diversifier les acteurs de la tech en la rendant présentable, c’est possible ?**

# Analyse de la problématique retenue

Vous avez dit rendre la tech présentable ? Késako ?

Actuellement, lorsque l'on interroge quelqu'un sur son envie de se lancer dans un métier en lien avec la technologie, la réponse est :

> Mais je n'y connais rien moi !

Même si on ressent dans les interviews que la forte présence masculine à un impact, ce qui ressort finalement c'est que les personnes ont manqué d'aide pendant leur parcours.
La problématique proposée offre donc une nouvelle piste qui n'a pas pour but d'en remettre une couche sur les inégalités entre les hommes et les femmes. L'idée centrale est plutôt de mettre en oeuvre les moyens nécéssaires pour que, lorsque les gens entendent parler de tech, ils ne se sentent pas exclus. Cela doit être valable pour tout le monde.

Lorsque j'écris : diversifier les acteurs de la tech, je fais référence aux femmes tout autant qu'aux jeunes. Dans 20 ans les acteurs de la tech seront les jeunes d'aujourd'hui. Il faut donc une solution qui puisse être acceptée par les générations actuelles et futures. C'est un challenge très intéressant, en effet, réussir à rendre intéressant un sujet qui semble si flou de base pour certaines personnes n'est pas évident. Pour faire passer un message au plus grand nombre le support est donc important. Et ensuite ? Une fois qu'on a un support, quel message veut-on faire passer ? On peut essayer comme ça :

> M. Dupont est DevOps, son métier consiste à s'occuper des missions de développement et d'exploitation !

C'est ce qui se passe actuellement sur internet. Pour un initié cela peut paraître clair, pour une personne extérieure absolument pas. La manière de présenter les choses doit permettre aux gens de se projeter. Essayons comme cela maintenant :

> M. Dupont est DevOps, il aide des personnes ou des sociétés à créer puis à partager leurs applications sur internet de manière efficace !

Certes il s'agit d'une vision simplifiée, cependant c'est compréhensible, on s'identifie mieux. Un non initié sera plus à même de s'en rappeler et donc de potentiellement le partager autour de lui. La différence entre les phrases réside dans la simplification des termes et le fait d'exprimer des opportunités concrètes : aider, créer, partager. Je pense réellement qu'envisager les professions par les opportunités qu'elles offrent plutôt que par leur nom peut changer la vision du monde de la tech.

# La solution : **Tech' it !**
## Présenter la tech grâce aux opportunités qu'elle offre !

Tech' it! est un site web conçu pour permettre à tous de s'identifier aux métiers de la technologie. Le concept est simple, on enfile ses bottes d'explorateur et on va rechercher les métiers en fonction des opportunités que l'on souhaite avoir !
Pas besoin donc de savoir ce qu'est un DevOps pour se lancer. L'expérience peut être vécue sur ordinateur, tablette, smartphone et j'en passe...

## Il manque quelque chose ...

Pour que les utilisateurs se prennent au jeu et aient envie de découvrir l'application, celle-ci est scénarisée. Quand on arrive sur Tech'it, on rentre dans la jungle des métiers de la technologie ! L'objectif est donc de l'explorer. Son exploration peut se faire à 3 niveaux :
- En tant visiteur, c'est à dire qu'on se balade tranquillement dans la jungle
- En tant Explorateur, à partir de ce moment, on a des outils pour affiner ses recherches.
- En tant qu'ambassadeur afin de partager ses expériences professionnelles

La tech c'est vaste, pas facile de s'y retrouver dans une jungle pareille ! Les explorateurs sont donc amenés à découvrir les pistes proposées par les ambassadeurs. Ces pistes correspondent concrètement à des expériences professionnelles. L'objectif est pour un ambassadeur, de partager de la meilleur manière son vécu d'un métier. Si des questions persistent chez nos explorateurs, ils ont la possibilité de réagir, de poser des questions. Le but est de rendre l'expérience interactive !

## Les Explorateurs c'est bien, mais les Exploratrices elles sont où ?

Pas de panique, une fois sur l'application, il suffit juste d'indiquer si l'on souhaite être considéré comme un homme ou une femme. Les textes de s'adaptent alors pour éviter toute frustration lors de l'utilisation ! Évidemment en tant que visiteur, tu peux être l'un ou l'autre. En devenant une Exploratrice à part entière, le site retiendra ta préférence et tu n'auras plus à t'en soucier ! (Sauf en indiquant un genre neutre)

## Petit clin d'oeil
Quelque chose me dit que des gens connus de vous et moi sont déjà présents dans la jungle !

Pour finir, une présentation vaut mieux qu'un long discours, rendez-vous sur [Tech' it!](https://techit.alwaysdata.net)
