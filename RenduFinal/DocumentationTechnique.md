Ne pas oublier de lire le fichier **DocumentationAnalyse** qui explique les motivations du projet !

# Technologies
Le choix des technologies a été extrêment important pour atteindre les objectifs de l'application dans le temps imparti. Je souhaitais une application qui puisse paraître "récente", rapide et intuitive.

## Gestion du code - [Gitlab](https://about.gitlab.com/)
L'utilisation de git pour un projet de cet ampleur était primordiale pour moi. Je n'ai jamais réellement utilisé git à son plein potentiel et j'ai pensé qu'il serait temps s'y mettre. Je ne voulais pas, une semaine avant la fin du projet, me rendre compte d'un bug majeur et ne pas pouvoir revenir en arrière.

## Gestion des données - [MySQL](https://www.mysql.com/fr/)
J'ai toujours eu l'occasion de développer avec des bases administrée avec MySQL par le passé. La documentation sur le sujet est très fournie sur internet. Cette technologie m'étant familière, j'ai décidé de l'utiliser. J'aurais cependant pu faire le choix d'une base de donnée non relationnelle comme on le voit de plus en plus en ce moment. Pourquoi ne pas avoir essayé ? Je pense que la compréhension des systèmes d'information tels que les bases de données recquiert beaucoup de temps. Partir dans une techno en copiant-collant du code est simple. L'utiliser dans un cadre concret avec tous les problème qui peuvent survenir est une autre histoire, surtout en base de donnée. À l'avenir, j'espère être apte à utiliser les bases de données non relationnelles.

Le fait d'avoir déjà pas mal manipulé a fortement influencé mon choix. Par ailleurs, l'application n'a pas besoin dans l'immédiat d'utiliser des To de données, MySQL convient donc très bien. Elle n'est pas non plus trop complexe pour envisager une refonte de sa base de donnée, il n'est pas trop tard pour un éventuel changement.

## Serveur - [Node.js](https://nodejs.org/fr/)
Je souhaitais que le développement du serveur web ne contraigne pas le développement du front-end. Le serveur gère les données et c'est tout. En cherchant, j'ai découvert le concept de RestAPI que j'ai directement adoré. Cela m'a forcé à me renseigner sur Node.js qui permet d'en mettre en place simplement. Développer une RestAPI me laisse maintenant la porte ouverte pour un portage sur mobile !

Le critère m'ayant convaincu par rapport à NodeJS a été la simplicité de déploiement. Un de mes objectifs était clairement dès le départ de pouvoir proposer une application en production. Je ne dis pas que les autres technologies sont difficiles à déployer mais la documentation sur Node est facilement accessible. Par la suite, je suis tombé sur la plateforme d'hébergement **Heroku** qui proposait une mise en production via Node.js et git. C'était tout ce qu'il me fallait ! Côté développement, j'ai été séduit par tous les module disponibles sur internet qui permettent de travailler dans de très bonnes conditions et très vite (ExpressJS par exemple).

## Client - [React](https://reactjs.org/)
Lorsque j'ai découvert le concept de RestAPI, j'ai tout de suite compris l'intérêt de celui-ci couplé à une autre technologie front-end. N'ayant vraiment aucune expérience dans le développement mobile (Il va falloir que je corrige ça d'ailleurs) un site web me semblait un bon choix.

Je n'ai pas cherché longtemps avant d'être confronté au trio Vue/Angular/React. J'ai adoré la manière dont React s'utilise via le JSX, ça me semblait intuitif et ça l'est. Les prérequis pour utiliser Angular ou Vue m'ont paru trop complexes. C'est donc encore sur un coup de coeur que je choisis une technologie pour mon projet.

## Sous le capot du projet

Pour coder le serveur web et l'application front-end j'ai utilisé des packages qui sont pour certains des incontournables au regard de leur utilité. En voici les principaux (La liste entière peut être trouvée dans les fichiers **package.json** du serveur et du client).

Côté serveur:
  - [ExpressJS](https://expressjs.com/fr/)
  - [Nodemon](https://nodemon.io/)
  - [Sequelize](http://docs.sequelizejs.com/)
  - [Socket.io](https://socket.io/)
  - [JWT](https://jwt.io/)
  - [Nodemailer](https://nodemailer.com/about/)

Côté client:
  - [React-router](https://github.com/ReactTraining/react-router)
  - [Axios](https://github.com/axios/axios)
  - [Redux](https://redux.js.org/)
  - [React-rte](https://github.com/sstur/react-rte)
  - [Socket.io-client](https://github.com/socketio/socket.io-client)


# Design - [Material-UI](https://material-ui.com/)
Côté design, je ne me voyais pas réinventer la roue. N'étant réellement familier qu'avec MySQL, je me suis rappelé que le temps dédié à mon apprentissage de Node.js et React sera non négligeable. Faire un projet en apprenant c'est difficile.

L'objectif était donc d'avoir un design correct qui donne confiance en l'application web et sans passer son temps dans le CSS. Il se trouve que Material-UI est un framework React permettant d'implémenter une interface utilisateur agréable similaire à celle des produits Google. Il fournit des composants prêts à l'emploi et totalement customisables.

Par ailleurs, les composants sont pensés pour fournir une bonne expérience utilisateur, devoir penser à comment chaque bouton doit réagir au clic aurait prit beaucoup de temps et je ne suis pas un expert des intéractions Homme-Machine. Grâce à ce framework j'ai pu donner vie à mes idées sans que la conception du design ne mette à mal la réalisation du projet.

## Petit bonus: Le logo Tech' it
Vous l'avez sûrement remarqué le logo est composé d'un cercle contenant un T au centre de 3 cercles reliés entre eux. Ce logo représente le lien que Tech' it tente d'établir entre 3 acteurs autour de la tech : les non initiés, les novices et les acteurs eux-mêmes.

# Difficultés et retour sur expérience
La route fut très difficile et ça même en utilisant des technologie répandues. Pourquoi ? Car je suis un novice du web. Lorsque l'on m'a parlé du concours (Merci à Nathan Séva, gagnant de l'édition 2018) j'étais en plein apprentissage du JavaScript. J'ai donc du me former sur le tas et m'assurer que ce que je développais à l'instant t n'allait pas contraindre la suite du projet.

De plus, étant étudiant, il se trouve que je n'ai pas eu de temps pour réellement développer durant le mois de mars (ça se voit aux commits). C'était plutôt une bonne chose en réalité, j'ai pu prendre le temps de construire l'idée et de mettre à l'écrit le projet. Le dossier Phase Conception du projet est d'ailleurs une ébauche de ce travail.

Durant le mois d'avril, les principales difficultés ont été de toujours garder en tête l'expérience utilisateur avant tout. Je me suis rendu compte que développer une fonctionnalité difficilement utilisable, ça prend du temps pour peu d'impact. À force d'effacer mon code pour le réecrire j'ai commencé à comprendre qu'il était préférable de tout planifier en avance. Surtout lorsque l'on parle de responsive !

Au niveau du développement, le plus dur à été de me familiariser avec l'ORM Sequelize qui faisait l'interface entre le serveur et la base de donnée. Celui-ci permet une bonne flexibilité dans le code javascript. Cependant prendre en main l'outil n'est pas évident. Je ne sais pas si j'utiliserais encore un ORM à l'avenir. D'autant plus que niveau optimisation, à partir d'un certain point, une requête fait main est bien meilleure.

# Autocritique

Concernant le projet en lui même, je suis plutôt fier d'avoir réussi à implémenter tout ce que j'avais en tête. Le fait d'apprendre autant de chose a été très enrichissant durant cette courte période. Pour enchainer sur l'autocritique quoi de mieux qu'un : et si je devais tout refaire ?

Si je devais tout refaire je pense que j'accorderais beaucoup plus de temps à l'élaboration de l'idée et de la solution. À première vue on peut penser que mon idée ne rentre absolument pas dans le thème du concours. La première réaction autour est souvent :
> Mais ça ne rentre pas dans le thème du sujet ton idée, si ?

J'aurais aimé être capable d'apporter une solution qui ne produise pas cet effet mais plutôt :
> Waouh ça c'est une solution !

Je reste cependant convaincu que présenter de manière efficace les métiers de la Tech est une idée  qui, sur le long terme, amènera un réel changement (Je l'explique dans la documentation d'analyse).

En dehors de celà, je ne regrette pas les choix de techno employés, je pense qu'il m'ont permis d'avancer en limitant les difficultés. Excepté pour un aspect : un RTE (Rich Text Editor) sur mobile. Je n'ai pas réussi à en trouver de responsive implémentable rapidement. La conséquence est que les utilisateurs mobiles ne peuvent pas rédiger de description (sauf en allant sur leur ordinateur). J'aurais dû anticiper ce soucis pour pouvoir le gérer dans le temps imparti.

# Perspectives

Les perspectives de Tech'it sont très variés. On peut très bien imaginer travailler le scénario de l'application afin de la rendre très immersive, un peu comme un jeu vidéo. Le concept d'exploration et de découverte dans une jungle peut être poussé encore plus loin. L'avantage c'est que les explorateurs consacreraient du temps à leur avenir et non pas à un autre quelconque fil d'actualité.

Le concept de l'application est d'ailleurs scalable à d'autres domaines professionnels. On peut imager une version de Tech' it pour de nombreuses filières. On présente tellement mal aux jeunes les opportunités qu'ils ont, cela ne pourrait faire que du bien (Quoique, il y a quelques pubs pas mal sur l'orientation sur youtube en ce moment).

Pour finir, peut-être que l'application ou au moins le concept de recherche par opportunités pourrait remplacer les modèles actuels où l'on se renseigne sur un métier par son intitulé. C'est intéressant de découvrir des emplois auxquels on ne s'attend pas mais qui nous correspondent totalement (comme dans la tech !).
