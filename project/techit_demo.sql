-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Lun 29 Avril 2019 à 20:07
-- Version du serveur :  5.7.14
-- Version de PHP :  7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `coddity`
--

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `text` text NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `userId` int(11) NOT NULL,
  `postId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `likes`
--

CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `postId` int(11) DEFAULT NULL,
  `lastpostversionseen` int(11) DEFAULT NULL,
  `lastmessageseen` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `likes`
--

INSERT INTO `likes` (`id`, `userId`, `postId`, `lastpostversionseen`, `lastmessageseen`, `createdAt`, `updatedAt`) VALUES
(87, 1, 25, 3, 0, '2019-04-29 18:51:54', '2019-04-29 19:59:18'),
(88, 2, 26, 3, 0, '2019-04-29 19:03:43', '2019-04-29 20:01:18'),
(89, 3, 27, 2, 0, '2019-04-29 19:12:02', '2019-04-29 20:02:09'),
(90, 4, 28, 2, 0, '2019-04-29 19:16:25', '2019-04-29 20:02:53'),
(91, 5, 29, 2, 0, '2019-04-29 19:21:38', '2019-04-29 20:05:09'),
(92, 6, 30, 2, 0, '2019-04-29 19:30:21', '2019-04-29 19:30:37'),
(93, 7, 31, 1, 0, '2019-04-29 19:34:19', '2019-04-29 19:34:19'),
(94, 7, 32, 1, 0, '2019-04-29 19:36:58', '2019-04-29 19:36:58'),
(95, 9, 33, 1, 0, '2019-04-29 19:43:58', '2019-04-29 19:43:58'),
(96, 10, 34, 1, 0, '2019-04-29 19:54:14', '2019-04-29 19:54:14'),
(97, 11, 35, 1, 0, '2019-04-29 19:57:26', '2019-04-29 19:57:26');

-- --------------------------------------------------------

--
-- Structure de la table `opportunities`
--

CREATE TABLE `opportunities` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `opportunities`
--

INSERT INTO `opportunities` (`id`, `name`, `description`) VALUES
(1, 'Relation client', 'Être en relation directe avec des clients.'),
(2, 'Management', 'Pouvoir diriger un groupe de personne plus ou moins grand.'),
(3, 'Télé-Travail', 'Pouvoir travailler depuis chez soi'),
(4, 'Missions à l\'international', 'Pouvoir travailler à l\'étranger lors de certaines missions'),
(5, 'Partage de connaissance', 'Devoir pour partager ses connaissances à d\'autres personnes'),
(6, 'Formation constante', 'Se former est un des aspects les plus important du métier'),
(7, 'Rédaction', 'Savoir rédiger est plus que nécéssaire pour le métier'),
(8, 'Horaires libres', 'Vous fixez vos propres horaires, l\'important est que le travail soit fait'),
(9, 'Création de contenu', 'Créer et partager du contenu est essentiel pour l\'activité'),
(10, 'Impacter le marché', 'Avoir un impact économique grâce à ses actions sur un secteur donné'),
(11, 'Négociation', 'Avoir l\'occasion de négocier au travail'),
(12, 'Analyse stratégique', 'Devoir analyser pour anticiper le futur'),
(13, 'Veille technologique', 'Être à jour sur les nouvelles technologies'),
(14, 'Projet d\'envergure', 'Travailler sur un projet pouvant impacter énormément de monde'),
(15, 'Conception logiciel', 'Concevoir un logiciel en amont du développement'),
(16, 'Technique', 'Avoir l\'opportunité de maîtriser des concepts technique'),
(17, 'Faire parler les chiffres', 'Analyser des données est un point fondamental');

-- --------------------------------------------------------

--
-- Structure de la table `postopportunity`
--

CREATE TABLE `postopportunity` (
  `postId` int(11) NOT NULL,
  `opportunityId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `postopportunity`
--

INSERT INTO `postopportunity` (`postId`, `opportunityId`) VALUES
(25, 2),
(27, 2),
(32, 2),
(34, 2),
(28, 5),
(30, 5),
(35, 5),
(29, 6),
(33, 6),
(34, 6),
(26, 7),
(27, 7),
(28, 7),
(25, 9),
(28, 9),
(33, 9),
(25, 10),
(29, 10),
(25, 11),
(27, 12),
(30, 12),
(35, 12),
(26, 13),
(27, 13),
(28, 13),
(34, 13),
(26, 14),
(27, 14),
(31, 14),
(35, 14),
(26, 15),
(29, 15),
(33, 15),
(29, 16),
(30, 16),
(31, 16),
(32, 16),
(33, 16),
(34, 16),
(30, 17),
(35, 17);

-- --------------------------------------------------------

--
-- Structure de la table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `version` int(11) NOT NULL DEFAULT '1',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `profileId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `posts`
--

INSERT INTO `posts` (`id`, `title`, `description`, `version`, `createdAt`, `updatedAt`, `profileId`) VALUES
(25, 'Chief Marketing Officer', '## Le concept :\n\nj’utilise le meilleur du digital, de la com, de la data, mais aussi du design, du business afin de mettre des produits et des services qui suscitent l’intérêt sur le marché et de ramener des clients vers eux.\n\n## Ce qui me plaît :\n\nLa variété des sujets et des interlocuteurs avec lesquels je travaille. Actuellement, j’interviens auprès de métiers très business (vente ou négociation contractuelle par exemple) : des business developers, des commerciaux, mais aussi des juristes ou des avocats. Sur ces 10 dernières années, j’ai travaillé avec des services d’édition d’applications et de sites web, donc auprès de développeurs back, front, mais aussi d’UX designers. C’est une source d’enrichissement quotidienne.\n', 3, '2019-04-29 18:51:54', '2019-04-29 19:59:18', 1),
(26, 'CTO à Amazing Content', '## Le concept\n\nConcrètement, je développe un logiciel et je conçois le service. Je mets les mains dans le cambouis. Je travaille dans un contexte où on a 1000 problèmes concrets à résoudre tous les jours.\n\n## Comment suis-je devenu CTO ?\n\nJ\'ai entendu parlé de ce métier assez rapidement et ça m\'attirait. De plus, j\'aime le fait de résoudre des problèmes et de créer quelque chose. Ce sont ces aspects pâte à modeler, maquette, DIY, qui me plaisent, le fait de construire quelque chose. Dans mon cas, c\'est aussi pour le rythme, j\'aime voir que les choses avancent très vite. En début de journée tu as 1000 problèmes à résoudre, et en fin de journée tu en as déjà réglé 900, c\'est très gratifiant. \n', 3, '2019-04-29 19:03:42', '2019-04-29 20:01:18', 2),
(27, 'Directeur du développement et de la croissance', '## Description\n\nJe suis directeur du développement et de la croissance au sein d’une startup qui édite une application qui propose des concertations à grande échelle, pour aider les leaders à engager et conduire leurs projets de transformation. Il y a 3 dimensions : une dimension commerciale (business development, partenariat stratégique), une dimension marketing produit et une dimension stratégie produit (UX). J’interviens en amont du développement, c’est à dire que je réfléchis à ce que l’on veut retrouver dans l‘appli et comment on va le structurer.\n\n## Ce qui me plaît\n\nOn veut traiter des sujets à très grande inertie et qui sont stratégiques pour les entreprises. De fait, c’est très complexe du point de de l’expérience utilisateur parce qu’il faut réussir à attirer et engager les utilisateurs dans l’application et réussir à structurer l’application pour obtenir toutes les fonctionnalités souhaitées.\n', 2, '2019-04-29 19:12:02', '2019-04-29 20:02:09', 3),
(28, 'Journaliste, rubrique média et technologies', '## Description\n\nJe suis journaliste au Figaro depuis 6 ans, au sein de la rubrique média et technologies. Je suis spécialisée dans l’actualité Technologie, c’est à dire l’industrie des nouvelles technologies au sens large : Google, Facebook, Apple et aussi des start-ups françaises ou non. Je travaille sur les enjeux du numérique au quotidien : cyber-harcèlement, modération en ligne, cyber-sécurité, etc. Cela peut prendre la forme d’interviews ou de reportages, publiés sur lefigaro.fr et sur le Figaro papier.\n\n## Ce qui me plaît\n\nLa Tech est un domaine à la fois très précis et très vaste. Aujourd’hui, de nombreux sujets d’actualités ont un rapport avec les nouvelles technologies. Prenons les Gilets Jaunes par exemple : sans Facebook ils n’auraient pas pu exister. Donc, pour comprendre ce mouvement, il est intéressant de se renseigner sur le fonctionnement des algorithmes de Facebook, de sa modération, et cela fait partie de mon travail.\n', 2, '2019-04-29 19:16:25', '2019-04-29 20:02:53', 4),
(29, 'CTO dans une start-up', '## Description\n\nJe suis CTO dans une start-up. Je suis en binôme avec un CEO et ensemble on conçoit un produit. Lui, apporte une vision commerciale, moi j’apporte une vision technique. On prend ensemble des décisions, et avec mon équipe on développe l’application ou ses nouvelles fonctionnalités.\n\n## Ce qui me plaît\n\nTout d’abord, j’apprends de nouvelles choses tous les jours. C’est vraiment un métier où les compétences attendues s’acquièrent en continu. Il y a tous les jours de nouveaux concepts à apprendre. Ensuite, j’aime le fait de voir très vite les résultats de ce que je produis : pas besoin d’attendre plusieurs mois. Enfin, j’aime travailler en équipe, avec un chef de projet et d’autres fonctions. Cela crée une émulsion qui permet de faire émerger le meilleur. C’est beaucoup d’échanges de connaissances, d’idées, etc. On a tous notre propre culture, notre propre expérience ce qui fait qu’on aura toujours de nouvelles idées.\n', 2, '2019-04-29 19:21:38', '2019-04-29 20:05:09', 5),
(30, 'Business Intelligence en ESN', '## Description\n\nLes enjeux du métier sont comment trier la donnée, la découper et la mettre en forme pour que les décideurs puissent avoir du recul.\n\n## Ce qui me plaît\n\nC’est le fait d’avoir une compétence à la fois technique et fonctionnelle. C’est le fait que l’on traite des données qui ont un sens pour le métier. On n’est pas uniquement les mains dans le code. On a quand même beaucoup de relations avec le métier, ce qui nous permet par ailleurs d’aborder un peu la politique en entreprise.\n', 2, '2019-04-29 19:30:21', '2019-04-29 19:30:37', 6),
(31, 'Consultante junior en cyber sécurité', '## Description\n\nJ\'ai pour mission de protéger les informations sensibles qui se rapportent à une société donnée. Il faut donc analyser le système d’information de celle-ci, en vue de proposer les solutions qui s’imposent. \n', 1, '2019-04-29 19:34:19', '2019-04-29 19:34:19', 7),
(32, 'Consulting technique en infrastructures publiques', '## Description\n\nLe but est de délivrer des certificats électroniques. Ce poste mêle technique et gestion de projets !\n\n## N\'hésitez pas à poser vos questions !\n', 1, '2019-04-29 19:36:58', '2019-04-29 19:36:58', 7),
(33, 'Ingénieur étude et développement', '## Description\n\nJe travaille sur des applications web et je suis plutôt front end.\n\n## Ce qui me plaît\n\nC’est le fait de pouvoir créer et de voir le résultat instantanément. Il y a un aspect conception et aspect résolution de problématiques à la fois. Et ensuite, tu vois réellement sur un écran ta solution ou ta création. Cela apporte a beaucoup de satisfaction.\n', 1, '2019-04-29 19:43:58', '2019-04-29 19:43:58', 9),
(34, 'Développeur web en ESN', '## Pourquoi j\'aime ce métier\n\nJ’aime le métier en lui même, ça évolue tout le temps, il y a plein de choses à apprendre. Il y a des projets différents, des technos différentes, des contextes différentes. C’est très riche. Ensuite j’aime cela parce qu’il y a une part de magie dans le dev. Quand je dois développer un algorithme ou une fonctionnalité plus ou moins complexe, que j’y passe du temps et qu’à terme tout fonctionne, ça me rend heureux ! Et je trouve que le mindset des développeurs est plutôt cool, c’est un univers assez détendu.\n', 1, '2019-04-29 19:54:14', '2019-04-29 19:54:14', 10),
(35, 'Architecte Data', '## Description\n\nCela consiste à donner des conseils sur tous les sujets liés à l\'architecture, la technique et les applicatifs\n\n## Ce qui me plaît\n\nC’est d’être au carrefour entre la technique, l\'applicatif et le métier. Je suis très à l’écoute des besoins des utilisateurs, pour toujours comprendre ce qu’ils veulent, et faire l’interface avec la maîtrise d\'oeuvre.\n', 1, '2019-04-29 19:57:26', '2019-04-29 19:57:26', 11);

-- --------------------------------------------------------

--
-- Structure de la table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(11) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `sentence` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `userId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `profiles`
--

INSERT INTO `profiles` (`id`, `firstName`, `lastName`, `sentence`, `description`, `createdAt`, `updatedAt`, `userId`) VALUES
(1, 'Emmanuelle', 'Coddity', 'J\'ai toujours veillé à m\'éclater dans ce que je faisais ...', '# Ma présentation rapide !\n\n### Cursus scolaire\n\n- DEUG d\'économie\n- ENSIA (école d\'ingénieur en statistique)\n\n### Pourquoi la technologie ?\n\nJe n\'ai pas vraiment décidé, les choses se sont faites un peu "comme ça". J\'ai toujours veillé à m\'éclater dans ce que je faisais au niveau de mes études ou de mon métier pour plus tard. Je me suis intéressée à la data parce que je sentais bien que cela devenait un enjeu, et il est vrai que des compétences techniques aident pour appréhender des sujets data vraiment intéressants en termes de volume et de variété de données.\n\n### Pourquoi suis-je sur Tech\' it ?\n\nDans les filières techniques, je pense qu\'il manque des projections visibles assez tôt : des perspectives de métiers et des sujets concrets à aborder. Je pense qu\'il faut améliorer cette visibilité le plus possible, c\'est pour cela que je partage mon expérience sur la plateforme !\n', '2019-04-29 18:46:44', '2019-04-29 18:52:51', 1),
(2, 'Ismael', 'Coddity', 'Les débuts de l\'internet grand public, cétait génial !', '# Ma présentation rapide !\n\n### Mon parcours\n\n- Grande école d\'ingénieur\n- Conseil pendant une dizaine d\'année d\'équipes qui faisaient du logiciel\n- CTO à Amazing Content\n\n### Pourquoi la technologie ?\n\nEn sortant d\'école d\'ingénieur, on était dans les débuts de l\'internet grand public et je trouvais ça génial. J\'adore lire et il y avait une masse incroyable de contenu qui s\'ouvrait à moi en quelques clics. C\'est ce qui m\'attirait.\n', '2019-04-29 18:46:44', '2019-04-29 19:04:12', 2),
(3, 'Louis', 'Coddity', 'De forts liens sont à tisser entre la tech et les sciences sociales', '\n# Ma présentation rapide !\n\n### Mon parcours\n\n- Deux ans de classe préparatoire\n- Centrale Nantes, spé management de l\'économie\n- Directeur du développement et de la croissance au sein d\'une startup\n\n### Pourquoi la technologie ?\n\nJe me suis longtemps posé la question. Je suis parti un peu à l\'aveugle dans la voir royale : Bac S < Prépa < école d\'ingé mais sans trop savoir pourquoi. Et en fait, j\'ai réalisé que la gymnastique intellectuelle dans la tech était celle qui me correspondait le mieux. Ce que j\'aime aujourd\'hui, c\'est la complexité de la déclinaison de la vision en méthodologie et en un produit, ainsi que la complexité du cycle de vente. \n\n###  Si je pouvais tout refaire ...\n\nJe pense que je prendrais plus de temps pour travailler les sciences sociales (anthropologie, socio, psycho), parce que je me rends compte que dans tous les grands axes que je couvre dans mon travail, il y a des liens forts à tisser entre la tech et ces sciences.\n', '2019-04-29 18:46:44', '2019-04-29 18:46:44', 3),
(4, 'Lucie', 'Coddity', 'Joindre utile et agréable dans le travail est indispensable', '\n# Ma présentation rapide !\n\n### Mon parcours\n\n- Science Po pendant 5 ans, dont 2 ans de master en journalisme\n- Journaliste au Figaro depuis 6 ans au sein de la rubrique média et technologies\n\n### Technologie et journalisme...\n\nJ’ai toujours été passionnée de technologies. J’étais également très forte en mathématiques et en écriture. Finalement, le journalisme m’intéressait et j’ai fait un parcours assez classique : cinq ans d\'études à Sciences Po, qui comprenaient deux ans de master en journalisme. Pendant mes études, j’ai conservé cette passion pour les nouvelles technologies: j’allais beaucoup sur des forums, je trifouillais les ordinateurs, etc. En devenant journaliste, j’ai remarqué qu’il y avait une réelle demande de la part des médias généralistes de traiter davantage ce genre de sujets et qu’il y avait peu de journalistes qui s’y connaissaient suffisamment. J’ai donc pu joindre “l’utile à l’agréable” et me spécialiser dans ce domaine. \n', '2019-04-29 18:46:44', '2019-04-29 18:46:44', 4),
(5, 'Marie', 'Coddity', 'Longtemps j’ai eu le cliché de l’ingénieur informaticien des années 2000 ...', '\n# Ma présentation rapide !\n\n### Mon parcours\n\n- Formation d\'ingénieur généraliste\n- CTO dans une start-up\n\n### Mon arrivée dans la tech\n\nQuand mes parents ont eu un ordinateur à la maison, j’ai tout de suite trouvé ça cool et j’ai voulu tout explorer. Je faisais partie de ces enfants qui ouvraient tous les dossiers dans l’ordi sans comprendre ce qui se passait ! Cependant, j\'ai décidé de m\'orienter dans ce milieu assez tard. Longtemps j’ai eu le cliché de l’ingénieur informaticien des années 2000 qui vivait entouré de ses serveurs et de son imprimante.\n\n### Ce que j\'aurais aimé savoir grâce à Tech\' it\n\nQuand j’étais ado, j’ai cru que c’était trop dur donc je n’ai pas pris cette direction. Plus tard, j’ai cru que j’avais “du retard” irrattrapable pour devenir développeuse, ce qui était faux bien entendu. J’aurais aimé savoir que l’on pouvait s’y mettre à n’importe quel âge, et avoir un aperçu des différents métiers.\n', '2019-04-29 18:46:44', '2019-04-29 18:46:44', 5),
(6, 'Marie-Andréa', 'Coddity', 'je pense qu’hommes ou femmes, on a tous une approche différente.', '\n# Ma présentation rapide !\n\n### Mon parcours\n\n- École d\'ingénieur avec option BI (Business Intelligence)\n- Travail dans une ESN\n\n### La technologie, une récente histoire\n\nJ\'ai décidé de m\'orienter après le lycée mais sans vraiment m\'orienter, je ne savais pas quoi faire. On m\'a conseillé de faire une classe préparatoire, ce que j\'ai fait, puis j\'ai intégré mon école avec la spé BI. Je n\'avais jamais entendu parler de mon métier avant ma dernière année d\'étude. Celui-ci requiert des qualités techniques et fonctionnelles, ce qui me plaisait beaucoup, je me suis donc lancée. J\'ai réalisé que je n\'avais pas besoin d\'être dans du code tout le temps. On peut avoir beaucoup de relations sociales avec mon poste actuel.\n', '2019-04-29 18:46:44', '2019-04-29 18:46:44', 6),
(7, 'Mathilde', 'Coddity', 'On n’est pas du tout orientées vers ce genre d’études ... Même dans les forums !', '\n# Ma présentation rapide !\n\n### Mon parcours\n\n- École d\'ingénieur\n- Consultante junior en cyber sécurité\n- Consulting technique autour d\'infrastructures à clés publiques\n\n### La tech c\'est formidable !\n\nL’informatique est un milieu de passionnés, et lorsqu’on arrive dedans un peu “par hasard”, ou je dirais sur le tard, on démarre de très loin ! Certains camarades développent depuis des années… J’imagine que si je m’étais intéressée plus tôt au domaine, ça aurait été plus simple sur de nombreux aspects. De plus, c’est un peu flou : tout le monde connaît plus ou moins ce qu’est l’informatique mais pas forcément ce qu’on peut y faire concrètement. Et c’est un domaine qui ne donne pas très envie si on ne s’y intéresse pas vraiment. Ce qui m\'a motivé à m\'orienter dans ce domaine d\'activité c\'est que j\'aime les mathématiques ainsi que l\'aspect Do It Yourself de la chose, de bidouiller des solutions.\n', '2019-04-29 18:46:44', '2019-04-29 18:46:44', 7),
(8, 'Olivia', 'Coddity', 'J\'aime bien les ordinateurs, j\'ai déjà codé un peu en HTML et ça me plaît un petit peu', '\n# Ma présentation rapide !\n\n### Mon parcours\n\n- Bac S\n- IUT informatique\n- Développement/Validation de tests fonctionnels\n\n### Ce qui m\'a mit sur la voie de la tech\n\nJe l’ai découvert à l’IUT. Venant d’un petit village de campagne, je ne connaissais pas les métiers de l’ingénieur. Pour moi, la classe prépa était quelque chose de grand mais très concurrentiel, alors je n’ai pas voulu tenter. Je n’avais donc aucune idée de la direction que je prenais : quand on n’a peu ou pas d’exemple dans son entourage proche, c’est difficile de savoir ce à quoi s’attendre. Je suis donc entrée à l’IUT en me disant “j ’aime bien les ordinateurs, j’ai déjà codé un peu en HTML, et ça me plait un petit peu” .\n\n### Mes premiers essais\n\nJ\'ai fait du HTML au début des années 2000, c’était la mode des blogs personnels, où on racontait sa vie. Il n’y avait pas d’outil à l’époque, il fallait développer à la main. Je me souviens que j’essayais de rendre beau mon blog !\n', '2019-04-29 18:46:44', '2019-04-29 18:46:44', 8),
(9, 'Sirine', 'Coddity', 'Toutes les femmes ont un rôle à jouer. Il ne s’agit pas que d’accuser les hommes ...', '\n# Ma présentation rapide !\n\n### Mon parcours\n\n- École préparatoire à Tunis\n- École d\'ingénieur en télécom\n- Ingénieur d\'études et développement\n\n### Pourquoi la tech ?\n\nJ’ai suivi une école préparatoire à Tunis puis une école d’ingénieur en télécom. Les cours de développement m’ont beaucoup plu. Avant cela, je ne pensais pas que je pourrais faire du développement, je n’y avais même jamais pensé. Je pense que c’est parce que je ne connaissais pas beaucoup de développeurs dans mon entourage et parce qu’on ne m’avait pas beaucoup parlé de ce métier. J’avais juste l’image de ce que j’avais vu dans des films ou autres, et ça me paraissant être un truc 100% pour des geeks et surtout pour les hommes. Et pourtant, au lycée, je me souviens que j’étais très forte en informatique, en algorithmique… Mais je n’avais jamais pensé à cela comme carrière. C’est bien en école d’ingénieur que j’ai découvert cette voie et décidé de m’y orienter.\n', '2019-04-29 18:46:44', '2019-04-29 18:46:44', 9),
(10, 'Theo', 'Coddity', 'Il y a une part de magie dans le dev !', '\n# Ma présentation rapide\n\n### Mon parcours\n\n- École du web\n- Développeur web dans une petite ESN\n\n### Mon initiation au web\n\nInitialement, je n’y connaissais rien et ça ne me venait pas à l’idée de tout savoir. C’est réellement en terminale que j’ai commencé à comprendre, et c’est encore plus tard que j’ai vraiment découvert tout ce qu’on pouvait faire. Et j’ai encore tellement de choses à découvrir.\n\n### La vie professionnelle sans hésiter !\n\nA l’école, c’est la concurrence qui m’a dérangé. On avait un classement par semestre et ca ne me plaisait pas. Certaines semaines étaient très intensives, et ne reflètent pas forcément la réalité du travail. On avait un temps très réduit pour produire des app de qualité. Dans la vie professionnelle, on définit des échéances cohérentes avec le projet et on prépare l’équipe pour faire en sorte d’adopter une bonne communication.\n', '2019-04-29 18:46:44', '2019-04-29 18:46:44', 10),
(11, 'Veronique', 'Coddity', 'Les réactions des hommes et des femmes ne sont pas les même et c’est important d’avoir les deux.', '\n# Ma présentation rapide\n\n### Mon parcours\n\n- IUT\n- 35 ans dans la data\n- Consultante architecte data\n\n### Une femme dans la data ?\n\nQuand j’ai fait mes études, dans les années 80s, c’était compliqué de faire un choix complètement réfléchi car il n’y avait pas vraiment d’informatique. Moi j’étais une matheuse, une scientifique depuis le lycée, et donc j’ai commencé par un IUT. J’ai vu que c’était plus le côté technique, scientifique et algorithmique qui m’intéressait, donc j’ai continué dans cette voie-là. Par la suite, dans mon parcours professionnel, j’ai souvent axé mes choix sur la technique. \n\nJ’aurais pu m’orienter vers une formation MIAGE, mais j’ai préféré aller vers la technique. J’ai longtemps hésité entre les télécoms et les bases de données d’ailleurs, passant de l’un à l’autre au fur et à mesure de mes expériences. J’ai finalement retenu les BDD car la discipline ne se résume pas uniquement à la technique : quand on s’occupe des données on s’occupe aussi du fonctionnel.\n', '2019-04-29 18:46:44', '2019-04-29 18:46:44', 11);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `sexe` char(255) NOT NULL,
  `validationToken` varchar(255) DEFAULT NULL,
  `validationTokenExpiration` datetime DEFAULT NULL,
  `resetPasswordToken` varchar(255) DEFAULT NULL,
  `resetPasswordTokenExpiration` datetime DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `sexe`, `validationToken`, `validationTokenExpiration`, `resetPasswordToken`, `resetPasswordTokenExpiration`, `createdAt`, `updatedAt`) VALUES
(1, 'emmanuelle', 'fakemail1@gmail.com', '$2a$12$k5eMTHpFI/UBmOHMR/xMveHydi/7YUU1sqfq2H42Es7HqA5bfRKsm', 'F', NULL, NULL, NULL, NULL, '2019-04-29 18:46:44', '2019-04-29 18:46:44'),
(2, 'ismael', 'fakemail2@gmail.com', '$2a$12$k5eMTHpFI/UBmOHMR/xMveHydi/7YUU1sqfq2H42Es7HqA5bfRKsm', 'H', NULL, NULL, NULL, NULL, '2019-04-29 18:46:44', '2019-04-29 18:46:44'),
(3, 'louis', 'fakemail3@gmail.com', '$2a$12$k5eMTHpFI/UBmOHMR/xMveHydi/7YUU1sqfq2H42Es7HqA5bfRKsm', 'H', NULL, NULL, NULL, NULL, '2019-04-29 18:46:44', '2019-04-29 18:46:44'),
(4, 'lucie', 'fakemail4@gmail.com', '$2a$12$k5eMTHpFI/UBmOHMR/xMveHydi/7YUU1sqfq2H42Es7HqA5bfRKsm', 'F', NULL, NULL, NULL, NULL, '2019-04-29 18:46:44', '2019-04-29 18:46:44'),
(5, 'marie', 'fakemail5@gmail.com', '$2a$12$k5eMTHpFI/UBmOHMR/xMveHydi/7YUU1sqfq2H42Es7HqA5bfRKsm', 'F', NULL, NULL, NULL, NULL, '2019-04-29 18:46:44', '2019-04-29 18:46:44'),
(6, 'marieandrea', 'fakemail6@gmail.com', '$2a$12$k5eMTHpFI/UBmOHMR/xMveHydi/7YUU1sqfq2H42Es7HqA5bfRKsm', 'F', NULL, NULL, NULL, NULL, '2019-04-29 18:46:44', '2019-04-29 18:46:44'),
(7, 'mathilde', 'fakemail7@gmail.com', '$2a$12$k5eMTHpFI/UBmOHMR/xMveHydi/7YUU1sqfq2H42Es7HqA5bfRKsm', 'F', NULL, NULL, NULL, NULL, '2019-04-29 18:46:44', '2019-04-29 18:46:44'),
(8, 'olivia', 'fakemail8@gmail.com', '$2a$12$k5eMTHpFI/UBmOHMR/xMveHydi/7YUU1sqfq2H42Es7HqA5bfRKsm', 'F', NULL, NULL, NULL, NULL, '2019-04-29 18:46:44', '2019-04-29 18:46:44'),
(9, 'sirine', 'fakemail9@gmail.com', '$2a$12$k5eMTHpFI/UBmOHMR/xMveHydi/7YUU1sqfq2H42Es7HqA5bfRKsm', 'F', NULL, NULL, NULL, NULL, '2019-04-29 18:46:44', '2019-04-29 18:46:44'),
(10, 'theo', 'fakemail10@gmail.com', '$2a$12$k5eMTHpFI/UBmOHMR/xMveHydi/7YUU1sqfq2H42Es7HqA5bfRKsm', 'H', NULL, NULL, NULL, NULL, '2019-04-29 18:46:44', '2019-04-29 18:46:44'),
(11, 'veronique', 'fakemail11@gmail.com', '$2a$12$k5eMTHpFI/UBmOHMR/xMveHydi/7YUU1sqfq2H42Es7HqA5bfRKsm', 'F', NULL, NULL, NULL, NULL, '2019-04-29 18:46:44', '2019-04-29 18:46:44');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`),
  ADD KEY `postId` (`postId`);

--
-- Index pour la table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`),
  ADD KEY `postId` (`postId`);

--
-- Index pour la table `opportunities`
--
ALTER TABLE `opportunities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `name_2` (`name`),
  ADD UNIQUE KEY `name_3` (`name`),
  ADD UNIQUE KEY `name_4` (`name`),
  ADD UNIQUE KEY `name_5` (`name`);

--
-- Index pour la table `postopportunity`
--
ALTER TABLE `postopportunity`
  ADD PRIMARY KEY (`postId`,`opportunityId`),
  ADD KEY `opportunityId` (`opportunityId`);

--
-- Index pour la table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profileId` (`profileId`);

--
-- Index pour la table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userId` (`userId`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username_2` (`username`),
  ADD UNIQUE KEY `email_2` (`email`),
  ADD UNIQUE KEY `username_3` (`username`),
  ADD UNIQUE KEY `email_3` (`email`),
  ADD UNIQUE KEY `username_4` (`username`),
  ADD UNIQUE KEY `email_4` (`email`),
  ADD UNIQUE KEY `username_5` (`username`),
  ADD UNIQUE KEY `email_5` (`email`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=241;
--
-- AUTO_INCREMENT pour la table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;
--
-- AUTO_INCREMENT pour la table `opportunities`
--
ALTER TABLE `opportunities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pour la table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT pour la table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_10` FOREIGN KEY (`postId`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comments_ibfk_9` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `likes`
--
ALTER TABLE `likes`
  ADD CONSTRAINT `likes_ibfk_10` FOREIGN KEY (`postId`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `likes_ibfk_9` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `postopportunity`
--
ALTER TABLE `postopportunity`
  ADD CONSTRAINT `postopportunity_ibfk_1` FOREIGN KEY (`postId`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `postopportunity_ibfk_2` FOREIGN KEY (`opportunityId`) REFERENCES `opportunities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`profileId`) REFERENCES `profiles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `profiles`
--
ALTER TABLE `profiles`
  ADD CONSTRAINT `profiles_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
