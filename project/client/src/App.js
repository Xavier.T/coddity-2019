import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router'
import PrivateRoute from './Components/Auth/PrivateRoute/PrivateRoute';

import Routes from './routing/routes';
import Home from './containers/Home/Home';
import Auth from './containers/Auth/Auth';
import ProfileRouter from './containers/Profile/ProfileRouter';
import PostRouter from './containers/Post/PostRouter';
import Discover from './containers/Discover/Discover';
import Parameters from './containers/Parameters/Parameters';
import Favorites from './containers/Favorites/Favorites';
import Notifications from './containers/Notifications/Notifications';
import NotFound from './containers/ErrorPages/NotFound/NotFound';
import ServerError from './containers/ErrorPages/ServerError/ServerError';

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

import { Authentication, UserData, AppMode, Notification } from './store/actions/actionsIndex';
import { connect } from 'react-redux';
import Layout from './containers/Layout/Layout';
import RefreshRoute from './Components/RefreshRoute/RefreshRoute';
import CircularProgress from '@material-ui/core/CircularProgress';
import OpportunitySearch from './containers/OpportunitySearch/OpportunitySearch';

const theme = createMuiTheme({
  palette: {
    primary: { main: "#558b2f" }, // Purple and green play nicely together.
    secondary: { main: '#ff3d00' }, // This is just green.A700 as hex.
  },
  typography: { useNextVariants: true },
});

class App extends Component {

  state={
    authProgress: 0,
  };

  componentDidMount() {
    if(!this.props.isErrorWithUserData){
      this.props.onCheckAuthState();
      this.props.setAppModeFromLocalStorage();

      //On indique qu'on a démarré le processus d'authentification
      this.setState({
        ...this.state,
        authProgress: 1,
      })
    }
  }

  static getDerivedStateFromProps(props, state){ //Load auth and user state and quit if there are errors.
  
    const newState = {...state}

    if(state.authProgress === 1) //Si on a bien démarré la requête d'authentification
    {
      if(props.token){ //On a réussi à s'authentifier
        props.onLoadUserData(props.token); //On lance la requête des infos utilisateur
        props.askNotifs(props.token); //On lance la requête pour les notifications
        props.initNotifSocket();

        newState.authProgress = 2;
      }
      else{ //On a pas pu s'authentifier
        newState.authProgress = 3;
      }
    }
    else if(state.authProgress === 2){
      if(props.isUserDataKnown && props.userGender){
        if(props.userGender !== 'O'){
          props.setAppMode(props.userGender);
        }
        newState.authProgress = 3;
      }
      else if(props.isErrorWithUserData){
        props.onLogout();
        props.onResetUserData();

        newState.authProgress = 3;
      }
    }
    else if(newState.authProgress === 3){
      
      if(props.token && !props.isUserDataKnown){ //Si on est authentifié mais qu'on a pas les données utilisateur (cas d'une connexion normale)
        props.onLoadUserData(props.token); //On lance la requête des infos utilisateur

        newState.authProgress = 2;
      }

      //Si on n'est pas authentifié mais qu'on a des données utilisateurs (cas d'un connection timeout)
      if(!props.token && props.isUserDataKnown){
        props.onResetUserData();
        newState.authProgress = 3; //On reste dans le même état, on a juste supprimé les données utilisateurs
      }
    }
    
    return newState;
  }

  render() {
    const progressComponent = (
            <div style={{height: '100%', width: '100%', display:'flex', justifyContent: 'center', alignItems:'center'}}>
              <CircularProgress />
            </div>
    );
    //this.props.getDiscoverTimeInfo(); //On lance la requête des infos sur le time de discover
    return (
          this.state.authProgress !== 3 ?
            progressComponent
          :
            <MuiThemeProvider theme={theme}>
              <Layout history={this.props.history} location={this.props.location}>
                <Switch>
                  <Route path="/" exact component={Home} />
                  <Route path="/auth" component={Auth}/>
                  
                  <Route path="/profile" component={ProfileRouter} />

                  <PrivateRoute path="/myparam" component={Parameters} condition={this.props.token}/>

                  <Route path="/post" component={PostRouter} />

                  <Route path={Routes.discover} component={Discover} />

                  <PrivateRoute path="/explore" component={OpportunitySearch} condition={this.props.token}/>

                  <PrivateRoute path="/favorites" component={Favorites} condition={this.props.token}/>

                  <PrivateRoute path="/notifications" component={Notifications} condition={this.props.token}/>

                  <RefreshRoute path="/refresh" />
                  <Route path={Routes.pageNotFound} component={NotFound} />
                  <Route path={Routes.serverError} component={ServerError} />
                  <Redirect to={Routes.pageNotFound} />
                </Switch>
              </Layout>
            </MuiThemeProvider>
    );
  }
}

const mapStateToProps = state => {
  return {
      token: state.Auth.token,
      authLoading: state.Auth.loading,
      isUserDataKnown: state.UserData.username !== null,
      isErrorWithUserData: state.UserData.error !== null,
      userGender: state.UserData.gender,
      isUserDataLoading: state.UserData.loading,
  };
}

const mapDispatchToProps =  dispatch => {
  return {
      onCheckAuthState: () => dispatch(Authentication.checkAuthState()),
      onLogout: () => dispatch(Authentication.logout()),

      onLoadUserData: (token) => dispatch(UserData.getUser(token)),
      onResetUserData: () => dispatch(UserData.resetUser()),

      setAppMode: (appMode) => dispatch(AppMode.setAppMode(appMode)),
      setAppModeFromLocalStorage: () => dispatch(AppMode.setFromLocaStorage()),

      //openPost: (postId) => dispatch(Notification.openPost(postId)),
      //closePost: (postId) => dispatch(Notification.closePost(postId)),
      askNotifs: (token) => dispatch(Notification.askNotifs(token)),
      initNotifSocket: () => dispatch(Notification.initSocket()),
  } 
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
