import React from 'react';
import {Route} from 'react-router-dom';

const Refresh = ({ path = '/', state = {} }) => (
    <Route
        path={path}
        component={({ history, location, match }) => {
            history.replace({
                ...location,
                pathname : location.pathname.substring(match.path.length),
                //state: state
            });
            return null;
        }}
    />
);

export default Refresh;