import React from 'react';
import { withStyles } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import SendIcon from '@material-ui/icons/Send';
import IconButton from '@material-ui/core/IconButton';
import ToolTip from '@material-ui/core/Tooltip';

const styles = theme => ({

    Container:{
        padding: theme.spacing.unit * 2,
        paddingTop: 0,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    TextContainer: {
        marginRight: theme.spacing.unit,
        flexGrow: 1,
    },
    Text:{
        fontFamily: theme.typography.fontFamily,
        fontSize: '80%',
    },
    
});

const message = ({classes, text, onSend, onChange}) => {
    return (
        <div className={classes.Container}>
            <div className={classes.TextContainer}>
                <TextField
                    id="standard-textarea"
                    placeholder="Une remarque ?"
                    multiline
                    className={classes.text}
                    variant="outlined"
                    margin="normal"
                    value={text}
                    onChange={onChange}
                    fullWidth
                />
            </div>
            <div>
                <ToolTip title="Envoyer" >
                    <div>
                        <IconButton onClick={onSend} disabled={text.length === 0}>
                            <SendIcon />
                        </IconButton>
                    </div>
                </ToolTip>
            </div>
            
        </div>
    );
};

export default withStyles(styles)(message);