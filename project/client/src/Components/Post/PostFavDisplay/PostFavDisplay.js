import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import PersonIcon from '@material-ui/icons/Person';
import SearchIcon from '@material-ui/icons/Search';
import DeleteIcon from '@material-ui/icons/Delete';
import NewIcon from '@material-ui/icons/FiberNew';
import ChatIcon from '@material-ui/icons/Chat';

import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';

const styles = theme => ({
    
    PostContainer: {
        padding: theme.spacing.unit * 1,
        
        marginTop: theme.spacing.unit * 3,
        marginBottom: theme.spacing.unit * 3,

        display: 'flex',
        justifyContent: 'center',
    },
    PostContainerNotif: {
        padding: theme.spacing.unit * 1,
        
        marginTop: theme.spacing.unit * 3,
        marginBottom: theme.spacing.unit * 3,
    },
    Title:{
        color: theme.palette.primary.main,
        fontSize: '100%',
        fontWeight: 'bold',
        
        padding: theme.spacing.unit * 1,
    },
    CaptionContainer:{
        padding: theme.spacing.unit*1,
    },
    NotificationMessage: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    Caption: {
        padding: theme.spacing.unit*1,
        marginLeft: theme.spacing.unit,
        //fontWeight: 'bold',
        fontSize: '100%',
    },
    Grow: {
        flexGrow: 1,
    },
    Actions:{
        display: 'flex',
        justifyContent: 'center',
    },
    Action:{
        //color: theme.palette.secondary.light,
    },
});

const postfavdisplay = ({classes, title, update, comments, onGoToProfile, onGoToPost, onDelete}) => {

    let fav = null;
    if(update === true || comments === true){
        const notif = (
            <div className={classes.CaptionContainer}>
                {
                    update === true ?
                    <div className={classes.NotificationMessage}>
                        <NewIcon color="secondary"/>
                        <Typography variant="caption" className={classes.Caption} color="secondary">La piste a été mise à jour !</Typography>
                    </div>
                    :
                    null
                }
                {
                    comments === true ?
                    <div className={classes.NotificationMessage}>
                        <ChatIcon color="secondary"/>
                        <Typography variant="caption" className={classes.Caption} color="secondary">Des nouvelles réactions sont disponibles !</Typography>
                    </div>
                    
                    :
                    null
                }
            </div>
            
        );
        fav = (
            <Paper className={classes.PostContainerNotif}>
                <div>
                    <Typography variant="h6" className={classes.Title} align='center'>{title}</Typography>
                </div>
                <Divider />
                {notif}
                <div className={classes.Actions}>
                    <IconButton className={classes.Action} onClick={onGoToPost}>
                        <SearchIcon />
                    </IconButton>
                    <IconButton className={classes.Action} onClick={onDelete}>
                        <DeleteIcon />
                    </IconButton>
                </div>
            </Paper>
        );
    }
    else{
        fav = (
            <Paper className={classes.PostContainer}>
                <Typography variant="h6" className={classes.Title}>{title}</Typography>
                <div className={classes.Grow}></div>
                <div className={classes.Actions}>
                    <IconButton className={classes.Action} onClick={onGoToProfile}>
                        <PersonIcon />
                    </IconButton>
                    <IconButton className={classes.Action} onClick={onGoToPost}>
                        <SearchIcon />
                    </IconButton>
                </div>
            </Paper>
        );
    }

    return(
      fav  
    );

};

export default withStyles(styles)(postfavdisplay);