import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { withStyles, Typography, IconButton } from '@material-ui/core';
import RefreshIcon from '@material-ui/icons/Refresh';
import InfiniteScroll from 'react-infinite-scroller';
import Button from '@material-ui/core/Button/Button';

import Message from '../Message/Message';
import Waiting from '../../../Components/UI/Waiting/Waiting';
import Divider from '@material-ui/core/Divider';
import {word, w} from '../../../Helpers/consts/Vocab/vocab';
import Routes from '../../../routing/routes';

const styles = theme => ({

    MessageBox:{
        overflow: 'auto',
        maxHeight: '400px',
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
        paddingLeft: theme.spacing.unit * 4,
        paddingRight: theme.spacing.unit * 4,
    },
    RefreshDiv: {
        display: 'flex',
        justifyContent: 'center',
    },
    Comments:{
        display: 'flex',
        justifyContent: 'column',
        flexFlow: 'column-reverse',
    },
    NotLoggedContainer:{
        textAlign: 'center',
    },
    RegisterButton:{
        marginTop: theme.spacing.unit,
    },
    [theme.breakpoints.down('sm')]: {
        MessageBox: {
            maxHeight: '250px',
        }
    },

});

//Props
//{classes, messages, atStart, atEnd, onUsernameClick, error, onLoadingMoreBottom, onLoadingMoreTop}

class MessageScroller extends Component{

    constructor(props){
        super(props);

        this.refBox = React.createRef();
    }



    goToRegister = () => {
        this.props.history.push(Routes.register);
    }

    scrollToBottom = () => {
        this.refBox.current.scrollTop = this.refBox.current.scrollHeight;
    }

    componentDidMount(){
        this.scrollToBottom();
    }

    componentDidUpdate(prevProps, prevState){

        if(prevProps.messages.length === 0 && this.props.messages.length > 0){  //Scroll si on vient d'ajouter un commentaire
            this.scrollToBottom();
        }
        if(prevProps.messages.length > 0 && this.props.messages.length > 0){    //Scroll si on vient d'écrire un commentaire
            if(this.props.messages[0].userId === this.props.userId){
                this.scrollToBottom();
            }
            else{   //Si ce n'est pas un de nos commentaires, on vérifie qu'on est assez bas pour considérer que l'utilisateur suit une conversation
                const msgScrolls = this.refBox.current.childNodes[0];
                const msgOuterBox = this.refBox.current;
                const lastMsg = msgScrolls.childNodes[1].childNodes[1]
                if(msgScrolls.clientHeight - lastMsg.clientHeight <= msgOuterBox.scrollTop + msgOuterBox.clientHeight){
                    this.scrollToBottom();
                }
            }
        }

    }

    render(){

        const {classes, messages, atStart, atEnd, onUsernameClick, error, onLoadingMoreBottom, onLoadingMoreTop, locked} = this.props;
        let display = null;
        
        if(error){
            display = <Typography variant="caption" align='center' color='error'>Une erreur est survenue pendant le chargement !</Typography>
        }
        else{
            if(messages.length > 0){
                display = (
                    <React.Fragment>
                        <div className={classes.RefreshDiv}>
                            {
                                !atStart ?
                                <IconButton onClick={onLoadingMoreTop}>
                                    <RefreshIcon />
                                </IconButton>
                                :
                                null
                            }
                            
                        </div>
                        <div className={classes.Comments}>
                            {messages.map((msg) => {
                                return (
                                <React.Fragment key={msg.id}>
                                    <Divider />
                                    <Message 
                                        username={msg.username} 
                                        date={msg.createdAt} 
                                        text={msg.text} 
                                        profileId={msg.profileId} 
                                        onUsernameClick={onUsernameClick}
                                    /> 
                                </React.Fragment>
                                )
                                    
                            }) }

                        </div>
                    </React.Fragment>
                    
                ); 
            }
            else{
                display = <Typography variant="caption" align='center'>Il n'y a pas encore de commentaires !</Typography>
            }
        }
    
        return (
            <div className={classes.MessageBox} ref={this.refBox}>
                    {locked ?
                        display = (
                            <div className={classes.NotLoggedContainer}>
                                <Typography variant="caption" align='center' color='error'>Seul {word(w.A, this.props.appMode, true) +' '+ word(w.Explorer, this.props.appMode)}  peut voir ces commentaires</Typography>
                                <Button variant="outlined" color="primary" className={classes.RegisterButton} onClick={this.goToRegister}>Devenir {word(w.Explorer, this.props.appMode)}</Button>
                            </div>         
                        )
                    :
                        <InfiniteScroll
                            loadMore={onLoadingMoreBottom}
                            hasMore={!atEnd}
                            loader={<div className="loader" key={0}><Waiting /></div>}
                            useWindow={false}
                        >
                            {display}
                        </InfiniteScroll>
                    }
                    
            </div>
        );
    }
}


export default withRouter(withStyles(styles)(MessageScroller));