import React from 'react';
import { BrowserView, MobileView } from "react-device-detect";

import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import FormHelperText from '@material-ui/core/FormHelperText';
import RichTextEditor from 'react-rte';

import OpportunityPickerList from '../../Opportunity/OpportunityPickerList/OpportunityPickerList';

const styles = theme => ({
    
    errorMessage: {
        textAlign: 'center',
        paddingTop: theme.spacing.unit * 2,
    },
    Paper: {
        padding: theme.spacing.unit*2,
        maxWidth: '1000px',
        margin: 'auto',
    },
    UserPic: {
        display: 'block',
        borderRadius: '50% 50%',
        width: '150px',
        margin: 'auto',
        marginBottom: theme.spacing.unit *2 ,
    },
    Section : {
        marginBottom: theme.spacing.unit * 6,
    },
    OpportunityChoice: {
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    OpportunityList: {
        width: '40%',
        maxHeight: '500px',
        overflow: 'auto',
    },
    RTEContainer: {
        marginTop: theme.spacing.unit * 1,
        marginBottom: theme.spacing.unit * 1,
    },
    RTE: {
        fontFamily: theme.typography.fontFamily,
    },
    Error: {
        borderColor: 'red',
    },
    Buttons: {
        textAlign: 'center',
    },
    Button: {
        marginTop: theme.spacing.unit * 2,
        marginLeft: theme.spacing.unit * 1,
        marginRight: theme.spacing.unit * 1,
    },
});

const updateform = ({
    classes, 

    title,


    titleError,
    descriptionError,
    opportunityError,
    updateIsStart,

    onChangedTitle,
    onChangeDescription,
    onChangeOpportunity,

    descriptionData,
    currentLength,
    maxDescriptionLength,

    opportunities,
    selectedOpportunities,
    maxCheckedOpportunity,
    checkedOpportunities,
    loadingOpportunities,

    onCancel,
    onConfirm,

    isCreateMode,
}) => {

    const RTE_classes = [classes.RTE];
    if(descriptionError){
        RTE_classes.push(classes.Error);
    }

    return(

        <div className={classes.Paper}>
            <Typography variant="h6" color={descriptionError ? 'error' : 'primary'} align='center'> Intitulé </Typography>
            <div className={classes.Section}>
                <TextField 
                    name='title' 
                    className={classes.textField} 
                    margin="normal" 
                    value={title}
                    onChange={onChangedTitle}
                    error={titleError}
                    required
                    variant="outlined"
                    fullWidth
                />
            </div>
            <div className={classes.Section}>
                <Typography variant="h6" color={descriptionError ? 'error' : 'primary'} align='center'> Les opportunités associées </Typography>
                <OpportunityPickerList
                    opportunities={opportunities}
                    maxCheckedOpportunity={maxCheckedOpportunity}
                    checkedOpportunities={checkedOpportunities}
                    onChangeOpportunity={onChangeOpportunity}
                    selectedOpportunities={selectedOpportunities}
                    opportunityError={opportunityError}
                    loading={loadingOpportunities}
                />
            </div>
            <div className={classes.RTEContainer}>
        
                <BrowserView>
                        <Typography variant="h6" color={descriptionError ? 'error' : 'default'} align='center'> Description </Typography>
                        <Typography variant="subtitle1" color={descriptionError ? 'error' : 'default'} align='center'> {currentLength} / {maxDescriptionLength} </Typography>
                        <RichTextEditor 
                            className={RTE_classes.join(' ')}
                            onChange={onChangeDescription}
                            value={descriptionData}
                        />
                        <FormHelperText>Pour une question d'encodage, le nombre de caractères affichés ci-dessus peut différer du nombre réel de caractères écrits </FormHelperText>
                    </BrowserView>
                <MobileView>
                    <FormHelperText>Utilisez tech'it depuis un ordinateur pour mettre à jour la description du profil. </FormHelperText>
                </MobileView>   
            </div>

            <div className={classes.Buttons}>
                <Button 
                    variant="contained"
                    color="secondary" 
                    className={classes.Button}
                    onClick={ onCancel }
                >
                    Annuler
                </Button>

                <Button 
                    variant="contained"
                    color="primary" 
                    className={classes.Button}
                    disabled={titleError || opportunityError || descriptionError || updateIsStart}
                    onClick={ onConfirm }
                >
                    {isCreateMode ? 'Publier !' : 'Mettre à jour !'}
                </Button>
            </div>
  
        </div>

    );

};

export default withStyles(styles)(updateform);