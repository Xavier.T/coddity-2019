import React from 'react';
import { withStyles, Typography } from '@material-ui/core';


const styles = theme => ({

    Comment:{
  
    },
    Username:{
        
        margin: '0px',
    },
    Clickable:{
        cursor: 'pointer',
        userSelect: 'none',
    },
    Date:{

    },
    Text:{
        fontFamily: theme.typography.fontFamily,
        fontSize: '90%',
        fontWeight: 'bold',
        //fontWeight: 'bold',
        //paddingLeft: theme.spacing.unit * 5,
    },
    
});

const message = ({classes, username, date, text, profileId, onUsernameClick}) => {
    
    const paragraphs = text.split('\n');

    return (
        <div className={classes.Comment}>
            <Typography variant="subtitle1" noWrap className={classes.Username} color={profileId ? 'primary' : 'inherit'} align='right'>
                <span className={profileId ? classes.Clickable : null} onClick={() => onUsernameClick(profileId)}>
                    {username}
                </span>
            </Typography>
            <Typography variant="caption" noWrap className={classes.Date} align='right'>{new Date(date).toLocaleString()}</Typography>
            {
                paragraphs.map((text, id) => {
                    return <p key={id} className={classes.Text}>{text}</p>
                })
            }
        </div>
    );
};

export default withStyles(styles)(message);