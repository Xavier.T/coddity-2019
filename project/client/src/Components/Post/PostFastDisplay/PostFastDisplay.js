import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import OpportunityList from '../../Opportunity/OpportunityList/OpportunityList';
import Divider from '@material-ui/core/Divider';
import Markdown from 'react-markdown';
import Button from '@material-ui/core/Button';

const styles = theme => ({
    Title:{
        padding: theme.spacing.unit * 1,
        fontWeight: 'bold',
    },
    PostContent:{
        padding: theme.spacing.unit * 2,
        paddingTop: '0px',
    },
    Opportunity: {
        overflow: 'auto',
    },
    Description : {
        fontFamily: theme.typography.fontFamily,
    },
    ActionContainer:{
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
    },
    ActionIcon: {
        marginLeft: theme.spacing.unit,
    }
});

const postfastdisplay = ({classes, title, opportunities, description, action}) => {

    return(
        <div className={classes.PostContent}>
            <Typography variant="h6" className={classes.Title} align='center' color="primary">{title}</Typography>
            <div className={classes.Opportunity}>
                <OpportunityList opportunities={opportunities}/>
            </div>
            
            {
                description ?
                    <React.Fragment>
                        <Divider />
                        <Markdown 
                            className={classes.Description}
                            escapeHtml={true}
                            source={description} 
                        />
                    </React.Fragment>
                :
                null
            }
            {
                action ?
                <div className={classes.ActionContainer}>
                    <Button onClick={action.onClick} variant="contained" color="primary">
                        {action.text}
                        <action.icon className={classes.ActionIcon}/>
                    </Button>
                </div>
                :
                null
            }
            
        </div>
    );

};

export default withStyles(styles)(postfastdisplay);