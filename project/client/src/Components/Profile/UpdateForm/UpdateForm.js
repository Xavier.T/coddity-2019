import React from 'react';
import { BrowserView, MobileView } from "react-device-detect";

import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import FormHelperText from '@material-ui/core/FormHelperText';

import RichTextEditor from 'react-rte';

import UserPic from '../../../res/logoUser.svg';
import PageTitle from '../../../Components/PageTitle/PageTitle';

const styles = theme => ({
    
    errorMessage: {
        textAlign: 'center',
        paddingTop: theme.spacing.unit * 2,
    },
    Paper: {
        padding: theme.spacing.unit*2,
    },
    UserPic: {
        display: 'block',
        borderRadius: '50% 50%',
        width: '150px',
        margin: 'auto',
        marginBottom: theme.spacing.unit *2 ,
    },
    SimpleData : {
        maxWidth: '500px',
        margin: 'auto',
    },
    RTEContainer: {
        textAlign: 'center',
        maxWidth: '1000px',
        margin:'auto',
        marginTop: theme.spacing.unit * 1,
        marginBottom: theme.spacing.unit * 1,
    },
    RTE: {
        textAlign: 'left',
        fontFamily: theme.typography.fontFamily,
    },
    Error: {
        borderColor: 'red',
    },
    Buttons: {
        textAlign: 'center',
    },
    Button: {
        marginTop: theme.spacing.unit * 2,
        marginLeft: theme.spacing.unit * 1,
        marginRight: theme.spacing.unit * 1,
    },
})

const updateform = ({
    classes, 

    firstName,
    lastName,
    sentence,

    firstNameError,
    lastNameError,
    sentenceError,
    descriptionError,
    updateIsStart,

    onChangedFirstName,
    onChangedLastName,
    onChangeSentence,
    onChangeDescription,

    descriptionData,
    currentLength,
    maxDescriptionLength,

    onCancel,
    onConfirm,
}) => {

    const RTE_classes = [classes.RTE];
    if(descriptionError){
        RTE_classes.push(classes.Error);
    }

    return(
        <React.Fragment>
            <PageTitle 
                        title={"Modifie ton profil à ta guise."}
                        subtitle="Sauf la photo, il faudra faire avec ahah !"
                    />
            <div className={classes.Paper}>
                <img 
                    src={UserPic} 
                    className={classes.UserPic}
                    alt="my profile" />

                <div className={classes.SimpleData}>
                    <TextField 
                        name='firstName' 
                        label="Prénom" 
                        className={classes.textField} 
                        margin="normal" 
                        value={firstName}
                        onChange={onChangedFirstName}
                        error={firstNameError}
                        required
                        variant="outlined"
                        fullWidth
                    />
                    <TextField 
                        name='lastName' 
                        label="Nom" 
                        className={classes.textField} 
                        margin="normal" 
                        value={lastName}
                        onChange={onChangedLastName}
                        error={lastNameError}
                        required
                        variant="outlined"
                        fullWidth
                    />
                    <TextField 
                        name='sentence' 
                        label="Citation / Devise" 
                        className={classes.textField} 
                        margin="normal" 
                        value={sentence}
                        onChange={onChangeSentence}
                        error={sentenceError}
                        multiline
                        variant="outlined"
                        fullWidth
                    />
                    
                </div>
    
                <div className={classes.RTEContainer}>
                
                    
                    
                        <BrowserView>
                            <Typography variant="h6" color={descriptionError ? 'error' : 'default'}> Ma description</Typography>
                                <Typography variant="subtitle1" color={descriptionError ? 'error' : 'default'}> {currentLength} / {maxDescriptionLength} </Typography>
                                <RichTextEditor 
                                    className={RTE_classes.join(' ')}
                                    onChange={onChangeDescription}
                                    value={descriptionData}
                                />
                                <FormHelperText>Pour une question d'encodage, le nombre de caractères affichés ci-dessus peut différer du nombre réel de caractères écrits </FormHelperText>
                            </BrowserView>
                        <MobileView>
                            <FormHelperText>Utilisez tech'it depuis un ordinateur pour mettre à jour la description du profil. </FormHelperText>
                        </MobileView>   
                    
                    
                </div>

                <div className={classes.Buttons}>
                    <Button 
                        variant="contained"
                        color="secondary" 
                        className={classes.Button}
                        onClick={ onCancel }
                    >
                        Annuler
                    </Button>

                    <Button 
                        variant="contained"
                        color="primary" 
                        className={classes.Button}
                        disabled={firstNameError || lastNameError || sentenceError || updateIsStart || descriptionError}
                        onClick={ onConfirm }
                    >
                        Valider
                    </Button>
                </div>
                

            
            </div>
        </React.Fragment>
    );

};



export default withStyles(styles)(updateform);