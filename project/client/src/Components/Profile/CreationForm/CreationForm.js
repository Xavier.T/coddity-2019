import React, { useState } from 'react';

import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

import Validator from '../../../Helpers/Validation/validation';
import {word, w} from '../../../Helpers/consts/Vocab/vocab';

const validation_functions = {

    name: (value) => Validator.runValidation(value, [
        {check: Validator.isAtLeast, args: {min: 1}},
        {check: Validator.isAtMax, args: {max: 20}},
    ]).valid,

}

const styles = theme => ({
    
    
    errorMessage: {
        textAlign: 'center',
        paddingTop: theme.spacing.unit * 2,
    },
    Paper: {
        paddingBottom: theme.spacing.unit * 2,
        paddingLeft: theme.spacing.unit * 2,
        paddingRight: theme.spacing.unit * 2,
    },
    Button: {
        marginTop: theme.spacing.unit * 2,
    },
})

const creationform = (props) => {

    const { classes } = props;

    //State Hooks ---------------------------------------------------------
    const [textInputs, setTextInputs] = useState({
        firstName: '',
        lastName: '',
    });
    const [validation, setValidation] = useState({
        firstName: null,
        lastName: null,
    });

    //Handlers ---------------------------------------------------------
    const changeTextInputHandler = (e) => {
        //Creating a new state
        const field_name = e.target.name;
        const value = e.target.value;
        const new_textInput = {
            ...textInputs,
        }
        new_textInput[field_name] = value;

        //Validation
        const new_validation = {
            ...validation,
        }
        new_validation[field_name] = validation_functions.name(value);

        //assigning new state
        setTextInputs(new_textInput);
        setValidation(new_validation);
    }

    return(

        <div className={classes.Paper}>

            <TextField 
                name='firstName' 
                label="Prénom" 
                fullWidth
                className={classes.textField} 
                margin="normal" 
                variant="outlined"
                value={textInputs.firstName}
                onChange={changeTextInputHandler}
                error={validation.firstName === false ? true : null}
                required
            />
            <TextField 
                name='lastName' 
                fullWidth
                label="Nom" 
                className={classes.textField} 
                margin="normal" 
                variant="outlined"
                value={textInputs.lastName}
                onChange={changeTextInputHandler}
                error={validation.lastName === false ? true : null}
                required
            />

            <Button 
                variant="contained"
                color="primary" 
                className={classes.Button}
                disabled={! (validation.firstName && validation.lastName) }
                fullWidth
                onClick={() => {props.onCreateProfile({...textInputs});}}
            >
                Devenir {word(w.Ambassador, props.appMode)}
            </Button>
        </div>

    );

}

export default withStyles(styles)(creationform);