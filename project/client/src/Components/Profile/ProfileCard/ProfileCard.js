import React from 'react';

import { Typography, Toolbar } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ToolTip from '@material-ui/core/Tooltip';
import Waiting from '../../../Components/UI/Waiting/Waiting';
import UserPic from '../../../res/logoUser.svg';
import EditIcon from '@material-ui/icons/Edit';
import PersonIcon from '@material-ui/icons/Person';
import AddIcon from '@material-ui/icons/Add';
import grey from '@material-ui/core/colors/grey';
import Markdown from 'react-markdown';

import Error from '../../UI/Error/Error';

const styles = theme => ({
    Card: {
        margin: 'auto',
        width: '90%',
        maxWidth: '1000px',
        color: grey[700],
    },
    ProfilTop: {
        textAlign: 'center',
        padding: theme.spacing.unit * 2,
        
    },
    ToolBar: {
        borderRadius: '5px 5px 0px 0px',
    },
    ToolBarTitle: {
        flexGrow: 1,
        paddingLeft: theme.spacing.unit,
    },
    UserPic: {
        borderRadius: '50% 50%',
        width: '150px',
        marginBottom: theme.spacing.unit *2 ,
    },
    Names: {
        color: grey[800],
    },
    Quote: {
        fontSize: '120%',
        margin: '0px',
        fontFamily: 'Kalam',
        fontStyle: 'italic',
        wordWrap: 'break-word',
    },
    ProfilBot: {
        padding: theme.spacing.unit * 2,
    },
    Description : {
        fontFamily: theme.typography.fontFamily,
    }
});

const profilecard = ({
    classes, 
    username, 
    firstName, 
    lastName, 
    sentence, 
    description,
    
    me,
    onEdit,
    onAdd,

    loading,
    error
    }) => {

    
    if(loading){
        return(
            <div className={classes.Card}>
                <Waiting />
            </div>
        )
    }
    else if(error){
        return(
            <div className={classes.Card}>
                <Error text="Une erreur est survenue durant le chargement du profil"/>
            </div>
        )
    }
    else{
        return (
            <div className={classes.Card}>
    
                <Toolbar className={classes.ToolBar} color="primary">
                    <PersonIcon />
                    <Typography noWrap variant="h6" color="inherit" className={classes.ToolBarTitle}>{username}</Typography>
                    
                    {
                        me ?
                        <React.Fragment>
                            <ToolTip title="Éditer le profil">
                                <IconButton color="secondary" onClick={onEdit}>
                                    <EditIcon />
                                </IconButton>
                            </ToolTip>
                            <ToolTip title="Ajouter une piste">
                                <IconButton color="secondary" onClick={onAdd}>
                                    <AddIcon />
                                </IconButton>
                            </ToolTip>
                        </React.Fragment>
                        
                        :
                        null
                    }
                    
                
                </Toolbar>
                <Divider />
                <div className={classes.ProfilTop}>
                    <img src={UserPic} className={classes.UserPic} alt="userPicture"></img>
                    <Typography variant="h4" className={classes.Names} gutterBottom>{[firstName, lastName].join(' ')}</Typography>
                    {sentence ? <p className={classes.Quote}>{sentence}</p> : null}
                </div>
                <Divider />
                {
                    description ? 
                        <div className={classes.ProfilBot}>
                            <Markdown 
                                className={classes.Description}
                                escapeHtml={true}
                                source={description} 
                            />
                        </div>
                    :
                    null
                }
            </div>  
        );
    }
    
};

export default withStyles(styles)(profilecard);