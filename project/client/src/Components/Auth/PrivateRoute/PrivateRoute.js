import React from 'react';
import {Route, Redirect} from 'react-router-dom';
import Routes from '../../../routing/routes';

const privateRoute = ({ render, component: Component, redirect=Routes.home, condition, downprops, ...rest }) => {
    
    if(render){
        const render_func = condition ? render : (props) => (<Redirect to = {redirect}/>);
        return(
            <Route {...rest} render={render_func} />)
    }
    else if(Component){
        return(
            <Route {...rest} render={(props) => (
                condition
                ? <Component {...props} />
                : <Redirect to = {redirect} />
            )} />);
    }  
};

  export default privateRoute;