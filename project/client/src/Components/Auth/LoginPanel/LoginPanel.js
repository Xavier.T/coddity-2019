import React, { useState } from 'react';


import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Typography } from '@material-ui/core';

const styles = theme => ({
    
    TextField: {
        marginBottom: theme.spacing.unit * 2,
        marginTop: 0,
    },
    errorMessage: {
        textAlign: 'center',
        paddingTop: theme.spacing.unit * 2,
    },
    Paper: {
        paddingBottom: theme.spacing.unit * 2,
        paddingLeft: theme.spacing.unit * 2,
        paddingRight: theme.spacing.unit * 2,
    },
})


const loginPanel = (props) => {
    const {classes} = props; //Classes object

    //State Hooks ---------------------------------------------------------
    const [textInputs, setTextInputs] = useState({
        email: '',
        psw: '',
    });

    //Handlers ---------------------------------------------------------
    const changeTextInputHandler = (e) => {
        //Creating a new state
        const field_name = e.target.name;
        const value = e.target.value;
        const new_textInput = {
            ...textInputs,
        }
        new_textInput[field_name] = value;

        //assigning new state
        setTextInputs(new_textInput);
    }

    const PressedEnter = (target) => {
        if(target.charCode === 13){
            props.onLog({...textInputs});
        }
    }

    //Rendering ---------------------------------------------------------
    const errorTypo = (props.errorMessage) ? <Typography color="error" variant="body1" className={classes.errorMessage}> {props.errorMessage} </Typography> : null;
    return(

        <div className={classes.Paper} onKeyPress={PressedEnter}>
            {errorTypo}

            <TextField 
                name='email' 
                label="Adresse email" 
                fullWidth
                margin="normal" 
                variant="outlined"
                value={textInputs.email}
                onChange={changeTextInputHandler}
                required
                className={classes.TextField}
            />
            <TextField 
                name='psw' 
                fullWidth
                label="Mot de passe" 
                margin="normal" 
                variant="outlined"
                value={textInputs.psw}
                onChange={changeTextInputHandler}
                required
                className={classes.TextField}
                type="password"
            />

            <Button 
                variant="contained"
                color="primary" 
                className={classes.button}
                fullWidth
                onClick={() => {props.onLog({...textInputs});}}
            >
                C'est parti !
            </Button>
        </div>
        
        
    );
}

export default withStyles(styles)(loginPanel);