import React, { useState } from 'react';


import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Divider from '@material-ui/core/Divider';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';

import Validator from '../../../Helpers/Validation/validation';
import { FormHelperText } from '@material-ui/core';

const styles = theme => ({
    
    formControl: {
        margin: theme.spacing.unit * 2,
        //marginTop: theme.spacing.unit * 3,
    },
    errorMessage: {
        textAlign: 'center',
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    Paper: {
        paddingBottom: theme.spacing.unit * 2,
        paddingLeft: theme.spacing.unit * 2,
        paddingRight: theme.spacing.unit * 2,
    },
    WithHelper:{
        marginBottom: '0px',
    },
})

const validation_functions = {

    email: (value) => Validator.runValidation(value, [
        {check: Validator.isEmail},
        {check: Validator.isAtMax, args: {max: 255}},
    ]).valid,
    
    username: (value) => Validator.runValidation(value, [
        {check: Validator.isAtLeast, args: {min: 5}},
        {check: Validator.isAtMax, args: {max: 20}},
    ]).valid,

    psw: (value) => Validator.runValidation(value, [
        {check: Validator.isCorrectPassword},
    ]).valid,
    
    psw_match: (psw, confirm_psw) => psw === confirm_psw,

}

const registerPanel = (props) => {
    const {classes} = props; //Classes object

    //State Hooks ---------------------------------------------------------
    const [textInputs, setTextInputs] = useState({
        email: '',
        username: '',
        psw: '',
        psw_confirm: '',
    });
    const [gender, setGender] = useState('F');
    const [conditionCheck, setConditionCheck] = useState(false);
    const [validation, setValidation] = useState({
        email: null,
        username: null,
        psw: null,
        psw_confirm: null,
    });

    //Use effects ---------------------------------------------------------


    //Handlers ---------------------------------------------------------
    const changeTextInputHandler = (e) => {
        //Creating a new state
        const field_name = e.target.name;
        const value = e.target.value;
        const new_textInput = {
            ...textInputs,
        }
        new_textInput[field_name] = value;

        //Validation
        const new_validation = {
            ...validation,
        }
        switch(field_name){
            case 'email':
                new_validation['email'] = validation_functions.email(value);
                break;
            
            case 'username':
                new_validation['username'] = validation_functions.username(value);
                break;

            case 'psw':
                new_validation['psw'] = validation_functions.psw(value);
                new_validation['psw_confirm'] = validation_functions.psw_match(new_textInput.psw, new_textInput.psw_confirm);
                break;
            
            case 'psw_confirm':
                new_validation['psw_confirm'] = validation_functions.psw_match(new_textInput.psw, new_textInput.psw_confirm);
                break;

            default:
                break;
        }

        //assigning new state
        setTextInputs(new_textInput);
        setValidation(new_validation);
    }

    const PressedEnter = (target) => {
        if(target.charCode === 13){
            props.onRegister({ ...textInputs, gender: gender, conditionCheck: conditionCheck });
        }
    }

    //Rendering ---------------------------------------------------------
    return(
        <div className={classes.Paper} onKeyPress={PressedEnter}>
            <TextField 
                name='email' 
                label="Adresse email" 
                required 
                fullWidth
                margin="normal" 
                variant="outlined"
                value={textInputs.email}
                onChange={changeTextInputHandler}
                error={validation.email === false ? true : null}
                className={classes.WithHelper}
            />
            <FormHelperText>L'email doit être valide et ne doit pas contenir de majuscules.</FormHelperText>

            <TextField 
                name='username'
                label="Pseudo"
                required
                fullWidth
                margin="normal"
                variant="outlined"
                value={textInputs.username}
                onChange={changeTextInputHandler}
                error={validation.username === false ? true : null}
                className={classes.WithHelper}
            />
            <FormHelperText>Le pseudo doit contenir au minimum 5 caractères.</FormHelperText>

            <TextField 
                name='psw' 
                required
                fullWidth
                label="Mot de passe" 
                margin="normal" 
                variant="outlined"
                value={textInputs.psw}
                onChange={changeTextInputHandler}
                error={validation.psw === false ? true : null}
                className={classes.WithHelper}
                type="password"
            />
            <FormHelperText>{"Le mot de passe doit au moins contenir 8 caractères dont 1 chiffre, 1 minuscule, 1 majuscule et un symbole ( @#$%^&* )."}</FormHelperText>

            <TextField 
                name='psw_confirm' 
                required
                fullWidth
                label="Confirmation du mot de passe" 
                margin="normal" 
                variant="outlined"
                value={textInputs.psw_confirm}
                onChange={changeTextInputHandler}
                error={validation.psw_confirm === false ? true : null}
                className={classes.WithHelper}
                type="password"
            />

            <FormControl className={classes.formControl}>
                <FormLabel component="legend">Je suis ...</FormLabel>
                <RadioGroup>

                    <FormControlLabel 
                        value='F' 
                        control={
                            <Radio 
                                checked = { gender === 'F' } 
                                onChange = {(e)=>{setGender(e.target.value)}}
                            /> 
                            }
                            
                        label="Une femme"
                    />
                    <FormControlLabel 
                        value='H' 
                        control={
                            <Radio 
                                color="primary" 
                                checked = {gender === 'H'}
                                onChange = {(e)=>{setGender(e.target.value)}}
                            />
                            } 
                        label="Un homme" 
                    />
                    <FormControlLabel 
                        value='O' 
                        control={
                            <Radio 
                                color="default" 
                                checked = {gender === 'O'}
                                onChange = {(e)=>{setGender(e.target.value)}}
                            />
                            } 
                        label="Neutre" 
                    />

                </RadioGroup>
            </FormControl>
            
            <Divider />
            
            <FormControl required className={classes.formControl}>
                <FormControlLabel 
                    control={
                        <Checkbox checked={conditionCheck} onChange={() => { setConditionCheck(!conditionCheck); }} color="primary"/>
                    }
                    label= "J'accepte les conditions générales d'utilisation du site (on va faire comme s'il y en avait 😉 )" 
                />
            </FormControl>

            <Button 
                variant="contained"
                color="primary" 
                fullWidth
                disabled={! (conditionCheck && validation.email && validation.username && validation.psw && validation.psw_confirm) }
                onClick={ () => { props.onRegister({ ...textInputs, gender: gender, conditionCheck: conditionCheck }); } }
                >
                
                C'est parti !
            </Button>
        </div>
        
    );
}

export default withStyles(styles)(registerPanel);