import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import { withStyles } from '@material-ui/core/styles';
import Badge from '@material-ui/core/Badge';

import LogoIcon from '../Assets/Logo/Logo';
import MenuIcon from '@material-ui/icons/Menu';
import { headBarMobileIndex } from '../../Helpers/consts/zIndex';

const styles = theme => ({
    AppBar: {
        textAlign: 'center',
        paddingTop: '10px',
    },
    ToolBar: {
        display: 'flex', 
        justifyContent:'center',
    },
    Logo: {
        padding: '5px',
    },
    Title: {
        marginLeft: '15px',
        
        fontSize: '225%',
        fontWeight: 'bold',
        letterSpacing: '3px',
    },

    Quote: {
        fontSize: '110%',
        margin: '0px',
        marginBottom: '5px',
        fontFamily: 'Kalam',
        fontStyle: 'italic',
        wordWrap: 'break-word',
    },
    ToolBarNav: {
        display: 'none',
        
    },
    Icon: {
        color: "white",
    },

    [theme.breakpoints.down('sm')]: {
        AppBar: {
            padding: 0,
            position: 'sticky',
            top: '0',
            zIndex: headBarMobileIndex,
            paddingTop: theme.spacing.unit * 1,
            paddingBottom: theme.spacing.unit * 1,
        },
        ToolBar: {
            display: 'none',
        },
        ToolBarNav: {
            display: 'block',
            padding: '0',
            margin: '0',
            marginTop: theme.spacing.unit,
        },
        ToolBarNavElem: {
            display: 'inline-block',
        },
        Quote:{
            display: 'none',
        }
    },
});

const headbar = (props) => {

    const { classes, citation } = props;
    const iconsWidth = (100 / (1+props.menuList.length))+'%';

    //Génération icons
    const icons = props.menuList.map((elem, index) => {
        return (
                <li className={classes.ToolBarNavElem} key={index} style={{width: iconsWidth}}>
                    
                    <IconButton onClick={() => { props.goTo(props.menuList[index].link) }} >
                        <Badge badgeContent={props.menuList[index].notifCount} color="secondary">
                            <elem.icon className={classes.Icon}/>  
                        </Badge>   
                    </IconButton>
                     
                </li>
        );
                
    });

    return(
        <AppBar position="static" className={classes.AppBar}>
            <Toolbar className={classes.ToolBar}>
                <div className={classes.Logo}>
                    <LogoIcon fillColor='white' width='60px' height='60px' />
                </div>
                <Typography variant="h1" className={classes.Title} color='inherit'>Tech' it !</Typography>
            </Toolbar>
            
            <p className={classes.Quote}>{citation}</p>

            <ul className={classes.ToolBarNav}>
                <li className={classes.ToolBarNavElem} style={{width: iconsWidth}}>
                    <IconButton onClick={props.openSideBar} >
                        <MenuIcon className={classes.Icon}/>
                    </IconButton>
                </li>
                {icons}        
            </ul>
            

        </AppBar>
    );
};

export default withStyles(styles)(headbar);