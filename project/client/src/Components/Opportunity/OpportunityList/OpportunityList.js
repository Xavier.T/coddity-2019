import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Opportunity from '../Opportunity';
import List from '@material-ui/core/List';

const styles = theme => ({
    List:{
        display: 'block',
    },
    Add:{
        display: 'inline-block',
    }
});

const opportunityList = ({classes, opportunities}) => {

    return(
        <List className={classes.List}>
            {
                opportunities.map((opp, i) => {
                    return (
                        <Opportunity key={i} opportunity={opp}  className={classes.opp}/>
                    )
                })
            }
        </List>
    );

};

export default withStyles(styles)(opportunityList);