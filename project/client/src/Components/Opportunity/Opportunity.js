import React from 'react';
import { ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import StarIcon from '@material-ui/icons/Star';
import Checkbox from '@material-ui/core/Checkbox';



const styles = theme => ({
    ListItem: {
        display: 'inline-block',
    },
    Name:{
        display: 'inline-block',
    },
    Help:{
        display: 'inline-block',
    },
});

const opportunity = ({classes, opportunity, check}) => {

    let color = 'secondary';
    if(check){
        if(!check.checked){
            color="inherit";
        }
        if(check.disabled){
            color="disabled";
        }
    }

    return(
        <ListItem>
            <ListItemIcon>
                <StarIcon color={color}/>
            </ListItemIcon>
            <ListItemText primary={opportunity.name} className={classes.Name}/>
            {
                check ?
                    <Checkbox
                        {...check}
                    />
                :
                null
            }

        </ListItem>
    );
};

export default withStyles(styles)(opportunity);