import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Opportunity from '../Opportunity';
import List from '@material-ui/core/List';

const styles = theme => ({
    List:{
        display: 'block',
    },
    Add:{
        display: 'inline-block',
    }
});

const opportunityPicker = ({classes, opportunities, maxCheck, checkedId, onChange}) => {

    const createOpportunity = (opp, maxCheck, checkedId) => {
        const checkData = {
            checked:false,
            disabled: false,
            onChange: (e) => onChange(e, opp.id),
        };

        if(checkedId.length >= maxCheck){
            if(checkedId.includes(opp.id)){
                checkData.checked = true;
            }else{
                checkData.disabled = true;
            }   
        }
        else{
            if(checkedId.includes(opp.id)){
                checkData.checked = true;
            }
        }

        return <Opportunity key={opp.id} opportunity={opp} check={checkData} className={classes.opp}/>
    };

    return(
        <List className={classes.List}>
            {
                opportunities.map((opp, i) => {
                    return createOpportunity(opp, maxCheck, checkedId);
                })
            }
        </List>
    );

};

export default withStyles(styles)(opportunityPicker);