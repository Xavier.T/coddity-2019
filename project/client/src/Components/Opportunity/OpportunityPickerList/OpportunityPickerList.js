import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import OpportunityPicker from '../OpportunityPicker/OpportunityPicker';
import OpportunityList from '../OpportunityList/OpportunityList';
import { Typography } from '@material-ui/core';
import Waiting from '../../UI/Waiting/Waiting';

const styles = theme => ({
    
    OpportunityChoice: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        margin: theme.spacing.unit * 3,
    },
    OpportunityList: {
        width: '40%',
    },
    Paper:{
        maxHeight: '500px',
        overflow: 'auto',
    },

    [theme.breakpoints.down('sm')]: {
        OpportunityChoice:{
            flexDirection: 'column',
            marginLeft: 0,
            marginRight: 0,
        },
        OpportunityList: {
            width: '90%',
            margin: 'auto',
            marginTop: theme.spacing.unit * 2,
            marginBottom: theme.spacing.unit * 2,
        },
        Paper:{
            maxHeight: '350px',
        },
    },
});

const OpportunityPickerList = ({
    classes, 
    opportunities,
    maxCheckedOpportunity,
    checkedOpportunities,
    onChangeOpportunity,
    selectedOpportunities,
    opportunityError,
    loading,
}) => {

    let display = null;
    if(loading){
        display = <Waiting />;
    }
    else{
        display = (
            <div className={classes.OpportunityChoice}>
                <Paper className={classes.OpportunityList}>
                    <div className={classes.Paper}>
                        <OpportunityPicker opportunities={opportunities} maxCheck={maxCheckedOpportunity} checkedId={checkedOpportunities} onChange={onChangeOpportunity} />
                    </div>
                </Paper>
                <Paper className={classes.OpportunityList}>
                    <div  className={classes.Paper}> 
                        <Typography align='center' variant='h6' color={opportunityError ? 'error' : 'inherit'}> {selectedOpportunities.length} / {maxCheckedOpportunity} </Typography>
                        <OpportunityList opportunities={selectedOpportunities} />
                    </div>
                </Paper>
            </div>
        );
    }

    return(
        display
    );
};

export default withStyles(styles)(OpportunityPickerList);