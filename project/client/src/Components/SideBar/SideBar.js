import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Backdrop from '../UI/Backdrop/Backdrop';

import { sidebarIndex } from '../../Helpers/consts/zIndex';

const styles = {
    SideBar:{
        position: 'fixed',
        height: '100vh',
        width: '250px',
        zIndex: sidebarIndex,
    },
    SideBarTransition:{
        position: 'fixed',
        height: '100vh',
        width: '250px',
        zIndex: sidebarIndex,

        transition: 'transform 0.5s ease-out',
    },
    Open:{
        backgroundColor: 'green',
        transform: 'translateX(0)',
    },
    Close:{
        backgroundColor: 'grey',
        transform: 'translateX(-100%)',
    },
};

const sidebar = ({classes, open, mobileMode, children, onClose}) => {

    let classNames = [classes.SideBarTransition, open ? classes.Open : classes.Close];
    if(mobileMode){
        return(
            <React.Fragment>
                {open ? <Backdrop onClick={onClose}/> : null}
                <div className={classNames.join(' ')}>
                    { children }
                </div>
            </React.Fragment>
        ) 
    }
    else{
        return (
            <div className={classes.SideBar}>
                { children }
            </div>
        );
    }   
 
};

export default withStyles(styles)(sidebar);