import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const styles = {
    Container:{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%',
    }
}

const waiting = props => {
    
    const { classes } = props;
    
    return(
        <div className={classes.Container}>
            <CircularProgress />
        </div>
    );
};

export default withStyles(styles)(waiting);