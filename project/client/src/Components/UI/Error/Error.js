import React from 'react';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import RefreshIcon from '@material-ui/icons/Refresh';

const error = ({text, onRefresh}) => {
    return (
        <div style={{padding: '10px'}} >
            <Typography color="error" align='center' variant="body1">{text}</Typography>
            <br />
            {
                onRefresh ?
                <div style={{width: '100%', display: 'flex', justifyContent: 'center', padding: '10px'}}>
                    <IconButton onClick={onRefresh}>
                        <RefreshIcon />
                    </IconButton>
                </div>
                
                :
                null
            }
        </div>
    );
};

export default error;