import React from 'react';
import { withStyles } from '@material-ui/core/styles';

import { modalIndex } from '../../../Helpers/consts/zIndex';

const styles = {

        Backdrop: {
            width: '100%',
            height: '100%',
            backgroundColor: '#000',
            opacity: 0.5,
            position: 'fixed',
            top: 0,
            left: 0,
            zIndex: modalIndex,
        },
};

const backdrop = (props) => {

    const { classes } = props;

    return(
        <div className={classes.Backdrop} onClick={(e) => {props.onClick(e);}}>{props.children}</div>
    );   

};

export default withStyles(styles)(backdrop);