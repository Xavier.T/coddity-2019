import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Backdrop from '../Backdrop/Backdrop';

import { modalIndex } from '../../../Helpers/consts/zIndex';

const styles = {

        ModalContainer: {
            position:'fixed',
            top: 0,
            left: 0,
            width: '100vw',
            height: '100vh',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            zIndex: modalIndex,
        },
};

const modal = (props) => {

    const { classes } = props;

    return(

        props.open?
        <React.Fragment>  
            <div 
                className={classes.ModalContainer}
                onKeyPress={(e) => {e.preventDefault()}}
            >
                <Backdrop onClick={(e) => {e.stopPropagation(); props.onClose();}}/>
                {props.children}
                
            </div>
            
        </React.Fragment>
        : null
        
    );
    

};

export default withStyles(styles)(modal);