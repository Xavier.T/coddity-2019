import React from 'react';
import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Modal from '../Modal';
import Divider from '@material-ui/core/Divider';

import ErrorIcon from '@material-ui/icons/Error';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import { modalIndex } from '../../../../Helpers/consts/zIndex';
const styles = theme => ({

    Modal:{
        maxWidth: '500px',
        padding: '15px',
        margin: '15px',
        textAlign: 'center',
        zIndex: modalIndex,
    },
    Button: {
        marginLeft: theme.spacing.unit * 1,
        marginRight: theme.spacing.unit * 1,
    }
}); 

const yesnomodal = (props) => {

    const { classes } = props;

    const onEnterPressed = (target) => {
        if(target.charCode === 13){
            props.onConfirm();
        }
    }

    let color = null;
    let Icon = null;
    switch(props.state){
        case "info": 
            color = "primary";
            Icon = <CheckCircleIcon color={color} />;
            break;
        case "error":
            color = "error";
            Icon = <ErrorIcon color={color} />;
            break;
        default:
            color = "primary";
            Icon = <CheckCircleIcon color={color} />;
            break;
    }

    let lines = null;
    if(Array.isArray(props.text)){
        lines = props.text.map((text, id) => {
            return <Typography key={id} variant="body1" style={{margin: '15px'}}>{text}</Typography>
        });
    }
    else{
        lines = <Typography variant="body1" style={{margin: '15px'}}>{props.text}</Typography>
    }

    return(
        <Modal open={props.open} onClose={props.onClose}>
            <Paper className={classes.Modal} onKeyPress={onEnterPressed}>
                {Icon}
                <Typography variant="h6" color={color}>{props.title}</Typography>
                <Divider />
                {lines}
                <Button variant="contained" color="primary" onClick={props.onClose} className={classes.Button}> {props.textBtnClose ? props.textBtnClose : 'Non !'}</Button>
                <Button variant="contained" color="primary" onClick={props.onConfirm} autoFocus className={classes.Button}> {props.textBtnConfirm ? props.textBtnConfirm : 'Oui !'}</Button>
            </Paper>
        </Modal>
        
        
    );
}

export default withStyles(styles)(yesnomodal);