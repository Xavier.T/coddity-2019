import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
    TitleContainer:{
        marginTop: theme.spacing.unit * 3,
        marginBottom: theme.spacing.unit * 3,
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
    },
    Title:{
        color: theme.palette.primary.light,
    },
    Caption:{
        fontSize: '90%',
        color: theme.palette.primary.dark,
    },
    [theme.breakpoints.down('xs')]: {
        TitleContainer: {
            padding: theme.spacing.unit * 2,
            alignItems: 'flex-start',
            textAlign: 'left',
        },
    },
});

const headTitle = ({classes, title, subtitle}) => {
    
    return (
        <div className={classes.TitleContainer}>
            <Typography variant="h5" className={classes.Title}>{title}</Typography>
            <Typography variant="h6" className={classes.Caption}>{subtitle}</Typography>
        </div>
    );
}

export default withStyles(styles)(headTitle);