import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import Badge from '@material-ui/core/Badge';

import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { withStyles, withTheme } from '@material-ui/core/styles';
import Logo from '../Assets/Logo/Logo';

const styles = theme => ({
    Menu:{
        overflow:'auto',
        marginBottom: theme.spacing.unit * 10,
    },
    AppModeButton: {
        marginTop: theme.spacing.unit * 2,
        marginBottom: theme.spacing.unit * 2,
    },
    Icon: {
        margin: 0,
        color: theme.palette.primary.light,
    },
    Link: {
        textDecoration: 'none',
    },
    LogoDiv: {
      marginBottom: theme.spacing.unit * 2,  
    },
});

const menulist = (props) => {

    const { classes, appMode, onSwitchAppMode, canSwitchMode } = props;

    let mode = null;
    let modeText = "Me considérer comme";
    switch(appMode){
        case 'H':
            mode = 'Un homme';
            break;
        case 'F':
            mode = 'Une femme';
            break;
        default:
            mode = 'Un homme';
    }

    const menuList = props.menuList;
    const menuRender = menuList.map((elem, id, arr) => {
        if(elem !== "sep"){
            return(
                <ListItem button key={id} onClick={() => {props.goTo(elem.link)}} className={classes.Button}>
                    <ListItemText>{elem.name}</ListItemText>
                    <ListItemIcon className={classes.Icon}>
                        <Badge badgeContent={menuList[id].notifCount} color="secondary">
                            <elem.icon/>
                        </Badge>
                    </ListItemIcon>
                </ListItem>
            );
        }
        else{
            return <Divider key={id} />;
        }
    });

    return (
        
        <Paper style={{textAlign: "center", paddingTop:'15px', height:'100%',}} square className={classes.Menu}>
            <div className={classes.LogoDiv}>
                <Logo width='70px' height='70px' fillColor={props.theme.palette.primary.main}/>
            </div>
            {
                canSwitchMode ?
                <React.Fragment>
                    <Typography> {modeText} </Typography>
                    <Button 
                        variant='outlined' 
                        color='primary' 
                        className={classes.AppModeButton} 
                        onClick={onSwitchAppMode} > { mode } 
                    </Button>
                </React.Fragment>
                :
                null
            }
            <List>
                {menuRender}
            </List>    
        </Paper>
    );
       
};

export default withStyles(styles)(withTheme()(menulist));