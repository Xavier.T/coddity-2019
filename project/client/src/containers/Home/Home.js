import  React, { Component } from 'react'
import { connect } from 'react-redux';
import { Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

import HomeStyle from './HomeStyle';
import PageTitle from '../../Components/PageTitle/PageTitle';
import Explore from '../../res/Presentation/explore.svg';
import ExplorePic from '../../res/Presentation/start_trip.svg';
import ProfileDataWoman from '../../res/Presentation/profileDataWoman.svg';
import ProfileDataMan from '../../res/Presentation/profileDataMan.svg';
import CollectionMan from '../../res/Presentation/collectionMan.svg';
import CollectionWoman from '../../res/Presentation/collectionWoman.svg';
import ReminderPic from '../../res/Presentation/reminder.svg';
import { word, w } from '../../Helpers/consts/Vocab/vocab';
import Routes from '../../routing/routes';

const Pic = ({classes, src, alt}) => {
    
    return (
    <div className={classes.PicContainer}>
        <img src={src} alt={alt} className={classes.Picture}/>
    </div>);
    
}

const TextDiv = ({classes, title, texts, src, alt, right, children}) => {
    let textPointClasses = [classes.TextPoint];
    if(right === true){
        textPointClasses.push(classes.Invert)
    }
    return(
        <div className={classes.TextContainer}>
            <PageTitle title={title}/>
            <div className={textPointClasses.join(' ')}>
                <div className={classes.Section}>
                    <Pic src={src} alt={alt} classes={classes}/>
                </div>
                <div className={classes.Section}>
                    {
                    texts.map((text, i) => {
                        return (
                            <React.Fragment key={i+i*100}>
                                <Typography variant="body1" className={classes.Text} >{text}</Typography>
                                <br />
                            </React.Fragment>
                        )
                    })
                    }
                </div>
            </div>
            {children}
        </div>
        
    );
}


class Home extends Component{

    goToRegister = () => {
        this.props.history.push(Routes.register);
    }

    goToDiscover = () => {
        this.props.history.push(Routes.discover);
    }

    goToExplore = () => {
        this.props.history.push(Routes.explore);
    }

    goToBecomeAmbassador = () => {
        this.props.history.push(Routes.createProfile);
    }

    goToProfile = (profileId) => {
        this.props.history.push(Routes.profile + profileId);
    }

    render(){
        let explain = null;
        const { classes, appMode } = this.props;

        if(!this.props.isAuthenticated && !this.props.isAmbassador){    //Visiteur
            explain = (
                <div>     
                    <TextDiv 
                        title={"Bienvenue sur Tech'it " + word(w.Visitor, appMode, true) + " !"}
                        texts={[
                            "Envie de découvrir la jungle des métiers de la technologie ?", 
                            "Oui ? Alors Tech'it est là pour t'équiper et s'assurer que ton exploration se passe bien.",
                            "Ton rôle est de découvrir des pistes que tu ne connais pas encore. Si tu es novice, c'est tant mieux !",
                            "Actuellement, tu ressemble à " + word(w.A, appMode, true) + " " + word(w.Man, appMode, true) + ". Je me trompe ? Si oui, tu peux changer ça dans ton menu d'exploration !"]}
                        src={ReminderPic}
                        alt="Rappel"
                        classes={classes}
                        right
                    />

                    <TextDiv 
                        title="Viens dénicher des opportunités incroyables !"
                        texts={[
                            "Comme tu peux le constater, il te manque une boussole afin d'explorer par toi même cette jungle !", 
                            "Pour le moment tu peux simplement te promener sans vraiment savoir où tu vas... (via l'onglet découverte)",
                            "Afin de rejoindre la communauté et dénicher tes propres pistes, il va falloir t'équiper !"]}
                        src={ExplorePic}
                        alt="Rappel"
                        classes={classes}
                    >
                        <div className={classes.ActionCallContainer}>
                            <Button variant="contained" color="primary" onClick={this.goToDiscover}>
                                Me balader
                            </Button>
                            <div className={classes.ActionSeparator}></div>
                            <Button variant="contained" color="secondary" size="large" onClick={this.goToRegister}>
                                M'équiper !
                            </Button>
                        </div>
                    </TextDiv>
                    

                    <TextDiv 
                        title="Partage tes expériences !"
                        texts={[
                            "Tu as déjà une expérience dans la tech ? N'hésites pas à t'équiper et devenir " + word(w.Ambassador, appMode) + " pour partager les pistes que tu as toi même emprunté !",
                            "Il y a tellements de chemins possibles dans la tech, ton expérience sera la bienvenue !",
                            appMode === 'F' ? "D'ailleurs, les pistes féminines sont très importantes ! Beaucoup d'exploratrice cherchent leur chemin sans trouver ..." : ''
                        ]}
                        src={appMode === 'F' ? ProfileDataWoman : ProfileDataMan}
                        alt="Partager ses pistes"
                        classes={classes}
                        right
                    />
                </div>    
            );
        }
        else if(this.props.isAuthenticated && !this.props.isAmbassador){    //Explorateur
            explain = (
                <div>     
                    <TextDiv 
                        title={"Vis pleinement ton rôle d'" + word(w.Explorer, appMode) + " !"}
                        texts={[
                            "Explore et analyse toutes les pistes qui te parlent ! Tu peux voir ça comme une enquête !", 
                            "Si une piste t'intéresse, tu pourras l'ajouter à ta collection. La piste sera ensuite répertoriée dans l'onglet Mes pistes.",
                            "N'oublie pas que tu es " + word(w.Surrounded, appMode, true) + " d'autres explorateurs et exploratrices ! Tu peux échanger avec eux dans les sections réactions de chaque piste. Qu'attends-tu ?" 
                        ]}    
                        src={Explore}
                        alt="Rappel"
                        classes={classes}
                        right
                    >
                        <div className={classes.ActionCallContainer}>
                            <Button variant="contained" color="secondary" size="large" onClick={this.goToExplore}>
                                Explorer !
                            </Button>
                        </div>
                    </TextDiv>

                    <TextDiv 
                        title="Partage tes expériences !"
                        texts={[
                            "Tu as déjà une expérience dans la tech ? N'hésites pas à t'équiper et devenir " + word(w.Ambassador, appMode) + " pour partager les pistes que tu as toi même emprunté !",
                            "Il y a tellements de chemins possibles dans la tech, ton expérience sera la bienvenue !",
                            appMode === 'F' ? "D'ailleurs, les pistes féminines sont très importantes ! Beaucoup d'exploratrice cherchent leur chemin sans trouver ..." : ''
                        ]}
                        src={appMode === 'F' ? ProfileDataWoman : ProfileDataMan}
                        alt="Partager ses pistes"
                        classes={classes}
                        right
                    >
                        <div className={classes.ActionCallContainer}>
                            <Button variant="contained" color="secondary" size="large" onClick={this.goToBecomeAmbassador}>
                                Partager mes pistes !
                            </Button>
                        </div>
                    </TextDiv>
                </div>    
            );
        }
        else if(this.props.isAuthenticated && this.props.isAmbassador){ //Ambassadeur
            explain = (
                <div>     
                    <TextDiv 
                        title={"En tant qu' " + word(w.Ambassador, appMode) + ", tu te dois d'être présentable !"}
                        texts={[
                            "Tu peux te présenter dans ton profil grâce à une citation et une description ! Fais preuve d'originalité !", 
                            "Rappelle-toi, ton profil doit parler autant " + ((appMode === 'F') ? "aux femmes qu'aux hommes ." : "aux hommes qu'aux femmes. ") + "La jungle des métiers de la tech, c'est pour tous !",
                            "Je suis sûr que la communauté va apprécier tes pistes d'exploration !"
                        ]}    
                        src={appMode === 'F' ? CollectionWoman : CollectionMan}
                        alt="Rappel"
                        classes={classes}
                        right
                    >
                        <div className={classes.ActionCallContainer}>
                            <Button variant="contained" color="secondary" size="large" onClick={() => {this.goToProfile(this.props.profileId)}}>
                                Mirroir viens ici !
                            </Button>
                        </div>
                    </TextDiv>
                </div>    
            );
        }
        
        return (
            

            <div className={classes.Container}>
                {explain}
            </div>


        );
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.Auth.token !== null,
        isAmbassador: state.UserData.profileId && state.UserData.profileId !== -1,
        profileId: state.UserData.profileId,
        appMode: state.AppMode.appMode,
    };
}

export default withStyles(HomeStyle)(connect(mapStateToProps, null)(Home));