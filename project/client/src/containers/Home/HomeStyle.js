const styles = theme => ({
    Container: {
        paddingTop: theme.spacing.unit * 5,
        width: '80%',
        maxWidth: '1200px',
        margin: 'auto',
    },
    TextContainer: {
        marginTop: theme.spacing.unit * 5,
        marginBottom: theme.spacing.unit * 15,
    },
    PicContainer:{
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    Picture: {
        width: '100%',
        margin: 'auto',
        maxWidth: '400px',
    },
    TextPoint: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    Section: {
        width: '50%',
        paddingLeft: theme.spacing.unit * 3,
        paddingRight: theme.spacing.unit * 3,
        paddingBottom: theme.spacing.unit * 3,
    },
    Invert: {
        flexDirection: 'row-reverse',
    },
    Text: {
        color: theme.palette.primary.main,
    },
    ActionCallContainer: {
        display: 'flex',
        justifyContent: 'center',
    },
    ActionSeparator:{
        width: theme.spacing.unit * 2,
        height: 0,
    },
    [theme.breakpoints.down('sm')]: {
        TextContainer: {
            marginTop: theme.spacing.unit * 3,
            marginBottom: theme.spacing.unit * 15,
        },
        TextPoint: {
            flexDirection: 'column',
        },
        Invert: {
            flexDirection: 'column',
        },
        Section: {
            width: '100%',
            marginTop: theme.spacing.unit * 3,
            marginBottom: theme.spacing.unit * 3,
            paddingBottom: 0,
        },
        ActionCallContainer: {
            display: 'flex',
            flexDirection: 'column-reverse',
            alignItems: 'center',
        },
        ActionSeparator:{
            width: 0,
            height: theme.spacing.unit * 2,
        },
    },
});

export default styles;