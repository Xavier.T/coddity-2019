import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';


const styles = theme => ({
    paper: {
        textAlign: 'center',
        padding: theme.spacing.unit * 2, 
        margin: 'auto',
    },
    text: {
        margin: theme.spacing.unit * 2,
    },
});

class ServerError extends Component{


    homeRedirect = () => {
        this.props.history.push('/');
    }

    render(){
        const { classes } = this.props;

        return (
            <div className={classes.paper}>
                <Typography variant="h4" className={classes.text}>Les services Tech'it sont temporairement indisponibles</Typography>
                <Typography variant="subtitle1" className={classes.text}>Veuillez nous excuser pour la gêne occasionée</Typography>
            </div>  
            
        )
    }
}

export default withStyles(styles)(ServerError);