import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import PageTitle from '../../../Components/PageTitle/PageTitle';
import Routes from '../../../routing/routes';

const styles = theme => ({
    paper: {
        textAlign: 'center',
    },
    text: {
        margin: theme.spacing.unit * 2,
    },
});

class NotFound extends Component{

    homeRedirect = () => {
        this.props.history.push(Routes.discover);
    }

    render(){
        const { classes } = this.props;

        return (
            <div className={classes.paper}>
                <PageTitle 
                    title="Attention !"
                    subtitle="La route que tu essayes d'emprunter n'existe pas ..."
                />

                <Button variant="contained" color="primary" className={classes.button} onClick={this.homeRedirect}>
                    Retour dans la jungle
                </Button>
            </div>  
            
        )
    }
}

export default withStyles(styles)(NotFound);