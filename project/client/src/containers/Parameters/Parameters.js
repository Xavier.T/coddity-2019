import  React, { Component } from 'react'
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';

import Axios from 'axios';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';

import * as Requests from '../../Helpers/Requests/Requests';
import { UserData } from '../../store/actions/actionsIndex';
import InfoModal from '../../Components/UI/Modals/InfoModal/InfoModal';
import YesNoModal from '../../Components/UI/Modals/YesNoModal/YesNoModal';
import {word, w} from '../../Helpers/consts/Vocab/vocab';
import * as grades from '../../Helpers/consts/grades';
import { withRequestsCanceller } from '../../HOC/RequestsCanceller/RequestsCanceller';
import PageTitle from '../../Components/PageTitle/PageTitle';

const styles = theme => ({
    Panel:{
        margin: 'auto',
        width: '90%',
        maxWidth: '1000px',
        overflow: 'hidden',
    },
    Cell:{
        padding: '0px',
    },
    Answer:{
        color: theme.palette.primary.main,
        textAlign: 'right',
        padding: '0px',
    },
    Title:{
        marginTop: theme.spacing.unit * 4,
        marginBottom: theme.spacing.unit * 2,
    },
    Action: {
        display: 'block',
        padding: theme.spacing.unit,
        '&:hover':{
            textDecoration: 'underline',
            cursor: 'pointer',
        }
    },
});

class Pärameters extends Component{

    signalCancel = Axios.CancelToken.source();

    state = {
        profileId: null,
        deleteFailure: false,
        deleteSuccess: false,
        
        wantDeleteProfile: false,
        wantDeleteAccount: false,
    }

    resetErrorState = () => {
        this.setState({
            deleteFailure: false,
            wantDeleteProfile: false,
            wantDeleteAccount: false,
        })
    }

    onSuccess = () => {
        this.resetErrorState();
        this.props.onResetUserData();
    }

    onDeleteProfile = () => {
        Requests.wrappedRequest(this.props.signalCancelToken, Requests.deleteProfile, {
            token: this.props.token,
            profileId: this.props.userData.profileId,
        })
        .then(res => {
            this.setState({
                deleteFailure: false,
                deleteSuccess: true,
                wantDeleteProfile: false,
            });
        })
        .catch(err => {
            if(err !== Requests.cancelErrorCode){
                this.setState({
                    ...this.state,
                    deleteFailure: err,
                    deleteSuccess: false,
                    wantDeleteProfile: false,
                });
            }
        });
    }

    onDeleteAccount = () => {

        Requests.wrappedRequest(this.props.signalCancelToken, Requests.deleteUserData, {
            token: this.props.token,
        })
        .then(res => {
                this.setState({
                    deleteFailure: false,
                    deleteSuccess: true,
                    wantDeleteAccount: false,
                });
        })
        .catch(err => {
            if(err !== Requests.cancelErrorCode){
                this.setState({
                    ...this.state,
                    deleteFailure: err,
                    deleteSuccess: false,
                    wantDeleteAccount: false,
                });
            }
        });
    }

    componentWillUnmount(){
        this.signalCancel.cancel('canceling requests');
    }

    render(){
        const { classes } = this.props;

        let gender = null;
        switch(this.props.userData.gender){
            case 'H':
                gender = 'Homme';
                break;
            case 'F':
                gender = 'Femme';
                break;
            case 'O':
                gender = 'Neutre';
                break;
            default:
                break;
        }

        let status = null;
        switch(this.props.userData.profileId){
            case grades.explorer:
            status = word(w.Explorer, this.props.appMode);
                break;
            default:
            status = word(w.Ambassador, this.props.appMode);
                break;
        }

        return (
            
            <React.Fragment>
                <InfoModal
                    state="error"
                    open={this.state.deleteFailure}
                    onClose={this.resetErrorState}
                    onConfirm={this.resetErrorState}
                    title="La suppresion a échouée"
                    text={this.state.deleteFailure}
                />
                <InfoModal
                    state="info"
                    open={this.state.deleteSuccess}
                    onClose={this.onSuccess}
                    onConfirm={this.onSuccess}
                    title="Suppresion réussie !"
                    text={null}
                />
                <YesNoModal
                    state="error"
                    open={this.state.wantDeleteProfile}
                    onClose={this.resetErrorState}
                    onConfirm={this.onDeleteProfile}
                    title="Procéder à la suppression du profil ?"
                    text={["Cette opération est irréversible !", "Toutes vos pistes et réactions seront supprimées"]}
                    textBtnClose="Annuler"
                    textBtnConfirm="Supprimer"
                />
                <YesNoModal
                    state="error"
                    open={this.state.wantDeleteAccount}
                    onClose={this.resetErrorState}
                    onConfirm={this.onDeleteAccount}
                    title="Procéder à la suppression du compte ?"
                    text={["Cette opération est irréversible !", "Si vous êtes "+ word(w.Ambassador, this.props.appMode) +", vos pistes et vos réactions seront supprimées" ]}
                    textBtnClose="Annuler"
                    textBtnConfirm="Supprimer"
                />

                <PageTitle 
                    title="Ici tu peux gérer tes affaires."
                    subtitle="Si tu décide de rendre ta boussole, tu es au bon endroit."
                />

                <div className={classes.Panel}>
                    <Typography variant="h5" className={classes.Title}>Mes informations</Typography>
                    <Table>
                        <TableBody>
                            <TableRow>
                                <TableCell className={classes.Cell}>Adresse e-mail</TableCell>
                                <TableCell className={classes.Answer}>{this.props.userData.email}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell className={classes.Cell}>Pseudo</TableCell>
                                <TableCell className={classes.Answer}>{this.props.userData.username}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell className={classes.Cell}>Genre</TableCell>
                                <TableCell className={classes.Answer}>{gender}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell className={classes.Cell}>Statut</TableCell>
                                <TableCell className={classes.Answer}>{status}</TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>

                    <Typography variant="h5" className={classes.Title}>Mon compte</Typography>
                    {
                        this.props.userData.profileId === -1 ?
                            null
                        :
                            <Typography variant="body1">
                                <span className={classes.Action} onClick={() => {this.setState({...this.state, wantDeleteProfile:true,})}} >
                                    Supprimer mon profil d'{word(w.Ambassador, this.props.appMode)} et rester {word(w.Explorer, this.props.appMode)}
                                </span>
                            </Typography>
                    }

                    <Typography variant="body1">
                        <span className={classes.Action} onClick={() => {this.setState({...this.state, wantDeleteAccount:true,})}}>
                            Supprimer entièrement mon compte de la plateforme tech'it
                        </span>
                    </Typography>

                </div>
            </React.Fragment>
            
        );
    }
}

const mapStateToProps = state => {
    return {
        token: state.Auth.token,
        userData: state.UserData,
        appMode: state.AppMode.appMode,
    };
}

const mapDispatchToProps =  dispatch => {
    return {
        onResetUserData: () => dispatch(UserData.resetUser()),
    } 
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(withRequestsCanceller(Pärameters)));