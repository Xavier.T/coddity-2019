import React, { Component } from 'react';
import { UserData } from '../../../store/actions/actionsIndex';
import { connect } from 'react-redux';
import {withStyles} from '@material-ui/core/styles'
import { withRouter } from 'react-router-dom';
import { withRequestsCanceller } from '../../../HOC/RequestsCanceller/RequestsCanceller';

import InfiniteScroll from 'react-infinite-scroller';

import * as Requests from '../../../Helpers/Requests/Requests';
import Routes from '../../../routing/routes';
import Waiting from '../../../Components/UI/Waiting/Waiting';
import PostDisplay from '../../Post/PostDisplay/PostDisplay';
import ProfileDisplay from '../ProfileDisplay/ProfileDisplay';
import Error from '../../../Components/UI/Error/Error';

const PostToLoadAtOnce = 3;

const styles = theme => ({
    PostContainer:{
        marginTop: theme.spacing.unit * 10,
        marginBottom: theme.spacing.unit * 10,
    }
});

class Profile extends Component{

    constructor(props){
        super(props);
        this.state = {
            profileId: props.match.path === Routes.myprofile ? +props.profileId : +props.match.params.id,
            posts: [],
            postsLoading: true,
            loadingError: null,
            currentPostPage: -1,
            reachEndOfPosts: false,
        }
    }

    goBack = () => {
        this.setState({
            ...this.state,
            loadingError: null,
        });
    }

    onLoadMorePost = () => {
        return new Promise((resolve, reject) => {
            const nextPage = this.state.currentPostPage+1;
            Requests.wrappedRequest(this.props.signalCancelToken, Requests.getAllPosts, {
                profileId: this.state.profileId,
                page: nextPage,
                pageSize: PostToLoadAtOnce,
            })
            .then(res => {
                this.setState({
                    ...this.state,
                    posts: [...this.state.posts, ...res.posts],
                    postsLoading: false,
                    currentPostPage: nextPage,
                    reachEndOfPosts: res.posts.length === 0,
                }, resolve);
            })
            .catch(err => {
                if(err !== Requests.cancelErrorCode){
                    this.onLoadingError(err);
                }   
            })
        })
    }

    onPostDeleted = (postId) => {
        const newPosts = [...this.state.posts];
        const index = newPosts.findIndex(el => el.id === postId);
        newPosts.splice(index, 1);
        this.setState({
            ...this.state,
            posts: newPosts,
        });
    }

    onLoadingError = (err) => {
        this.setState({
            ...this.state,
            loadingError: err,
        });
    }

    componentDidMount(){
        this.onLoadMorePost();
    }

    render(){
        const {classes} = this.props;
        const infiniteScroller = (
            this.state.posts.length > 0 && !this.state.headPost?
                <InfiniteScroll
                    pageStart={0}
                    loadMore={ () => { if(!this.state.reachEndOfPosts){this.onLoadMorePost()} } }
                    hasMore={!this.state.reachEndOfPosts}
                    loader={<div className="loader" key={0}><Waiting /></div>}
                >
                    {
                        this.state.posts.map((post) => {
                            return (<div className={classes.PostContainer} key={post.id}>
                                        <PostDisplay
                                            post={post} 
                                            displayedOnProfile={true} 
                                            openComments={false}
                                            history={this.props.history} 
                                            me={this.state.profileId === this.props.profileId}
                                            onDelete={this.onPostDeleted}
                                            />
                                    </div>);
                        })
                    }
                </InfiniteScroll>
            :
            null
        );

        return(
            <React.Fragment>
                <ProfileDisplay 
                    profileId={this.state.profileId}
                />
                {infiniteScroller}
                {
                    this.state.loadingError ?
                    <Error text="Une erreur est survenue durant le chargement des posts" />
                    :
                    null
                }
            </React.Fragment>      
        ); 
    }

};

const mapStateToProps = state => {
    return {
        userId: state.Auth.userId,
        profileId: state.UserData.profileId,
        token: state.Auth.token,
        grade: state.UserData.grade,
    };
};

const mapDispatchToProps =  dispatch => {
    return {
        onLoadUserData: (token) => dispatch(UserData.getUser(token)),
        onResetUserData: () => dispatch(UserData.resetUser()),
    } 
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(withRequestsCanceller(Profile))));
