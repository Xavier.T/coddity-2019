import React, { Component } from 'react';
import { connect } from 'react-redux';
import {withStyles} from '@material-ui/core/styles'
import { withRouter } from "react-router";
import { withRequestsCanceller } from '../../../HOC/RequestsCanceller/RequestsCanceller';

import * as Requests from '../../../Helpers/Requests/Requests';
import Routes from '../../../routing/routes';
import ProfileCard from '../../../Components/Profile/ProfileCard/ProfileCard';

const styles = theme => ({

});

class ProfileDisplay extends Component{

    constructor(props){
        super(props);

        this.state = {
            profileId: props.profileId,
            profileUserId: null,

            username: null,
            firstName: null,
            lastName: null,
            sentence: null,
            description: null,
            errorOnProfileLoad: null,
            loading: true,
        }
    }

    goToEdit = () => {
        this.props.history.push(Routes.updateProfile);
    }

    goToPostEdit = () => {
        this.props.history.push(Routes.createPost);
    }

    setProfileInfoInState = (res) => {
        return new Promise((resolve, reject) => {
            let new_me = false;
            if(res.userId === +this.props.userId){
                new_me = true;
            }
            this.setState({
                ...this.state,
                profileUserId: res.userId,
                profileId: res.profileId,
                username: res.username,
                firstName: res.firstName,
                lastName: res.lastName,
                sentence: res.sentence,
                description: res.description,
                loading: false,
                postsLoading: false,
                me: new_me,
            }, resolve());
        });
    }

    setProfileInErrorState = (err) => {
        this.setState({
            ...this.state,
            errorOnProfileLoad: err,
            loading: false,
            postsLoading: false,
        });
        
    }

    componentDidMount(){
        //Récupération du profil
        Requests.wrappedRequest(this.props.signalCancelToken, Requests.getProfile, {profileId: this.props.profileId} )
        .then(this.setProfileInfoInState)
        .catch((err) => {
            if(err !== Requests.cancelErrorCode){
                this.setProfileInErrorState(err)
            }
        });
    }

    render(){
        return(
            <ProfileCard 
                me = {this.state.me}
                username={this.state.username}
                firstName={this.state.firstName}
                lastName={this.state.lastName}
                sentence={this.state.sentence ? this.state.sentence : null}
                description={this.state.description ? this.state.description.toString('markdown') : null}
                onEdit = {this.goToEdit}
                onAdd = {this.goToPostEdit}
                loading = {this.state.loading}
                error = {this.state.errorOnProfileLoad}
            />
        ); 
    }
};

const mapStateToProps = state => {
    return {
        userId: state.Auth.userId,
        token: state.Auth.token,
    };
};

export default withRouter(connect(mapStateToProps, null)(withStyles(styles)(withRequestsCanceller(ProfileDisplay))));
