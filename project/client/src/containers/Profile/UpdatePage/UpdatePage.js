import  React, { Component } from 'react'
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { withRequestsCanceller } from '../../../HOC/RequestsCanceller/RequestsCanceller';

import RichTextEditor from 'react-rte';

import { UserData } from '../../../store/actions/actionsIndex';
import * as Requests from '../../../Helpers/Requests/Requests';
import UpdateForm from '../../../Components/Profile/UpdateForm/UpdateForm';
import InfoModal from '../../../Components/UI/Modals/InfoModal/InfoModal';
import Routes from '../../../routing/routes';
import Validator from '../../../Helpers/Validation/validation';
import Waiting from '../../../Components/UI/Waiting/Waiting';
import { goBack } from '../../../routing/utils';

const maxDescriptionLength = 1000;

const validation_functions = {

    name: (value) => Validator.runValidation(value, [
        {check: Validator.isAtLeast, args: {min: 1}},
        {check: Validator.isAtMax, args: {max: 20}},
    ]).valid,

    sentence : (value) => Validator.runValidation(value, [
        {check: Validator.isAtMax, args: {max: 100}},
    ]).valid,

    description: (value) => Validator.runValidation(value, [
        {check: Validator.isAtMax, args: {max: maxDescriptionLength}},
    ]).valid,

}

const styles = theme => ({
    Panel: {
        maxWidth: '1200px',

        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: '40px',
    },
    WaitDiv: {
        display: 'flex',
    }
});

class UpdatePage extends Component{

    constructor(props){
        super(props);
        
        const startingInfo = {
            firstName: '',
            lastName: '',
            sentence: '',
            description: RichTextEditor.createValueFromString('', 'markdown'),
            descriptionPlainText: '',
        }

        const validation_info = {
            updateIsStart: true,

            firstName: null,
            lastName: null,
            sentence: null,
            description: null,
        }

        this.state = {
            initialInfo: {...startingInfo},
            updateInfo: {...startingInfo},
            validation: validation_info,
            profileId: props.profileId,
            loading: true,
            updatedProfile: false,
            updateError: null,
        }
    }
    //Handlers ------------------------------------------------------------------------------

    onUpdateProfileHandler = () => {

        const info = this.state.updateInfo;
        Requests.wrappedRequest(this.props.signalCancelToken, Requests.updateProfile, {
            token: this.props.token,
            profileId: this.state.profileId,
            firstName: info.firstName,
            lastName: info.lastName,
            sentence: info.sentence,
            description: info.descriptionPlainText,
        })
        .then(res => {
            this.setState({
                ...this.state,
                updatedProfile: true,
            })
        })
        .catch(err => {
            if(err !== Requests.cancelErrorCode){
                this.setState({
                    ...this.state,
                    updatedError: err,
                })
            }
        });
    }

    goToProfile = () => {
        this.props.onResetUserData();
        this.props.history.push(Routes.myprofile);
    }

    goBack = () => {
        goBack(this.props);
    }

    getNewInputValidation = (newInputs, forceUpdateIsStart=false) => {
        
        const new_validation = {
            ...this.state.validation,
        }
        new_validation.firstName = validation_functions.name(newInputs.firstName);
        new_validation.lastName = validation_functions.name(newInputs.lastName);
        new_validation.sentence = validation_functions.sentence(newInputs.sentence);
        new_validation.description = validation_functions.description(newInputs.descriptionPlainText);
        
        //Checking update or not
        new_validation.updateIsStart = true;
        if(!forceUpdateIsStart){
            if(
                newInputs.firstName !== this.state.initialInfo.firstName ||
                newInputs.lastName !== this.state.initialInfo.lastName ||
                newInputs.sentence !== this.state.initialInfo.sentence ||
                (newInputs.descriptionPlainText !== this.state.initialInfo.descriptionPlainText)
            ){
                new_validation.updateIsStart = false;
            }
        }

        return new_validation;
    }

    changeTextInputHandler = (e) => {
        //Creating a new state
        const field_name = e.target.name;
        const value = e.target.value;
        const new_info = {
            ...this.state.updateInfo,
        }
        new_info[field_name] = value;

        this.setState({
            ...this.state,
            updateInfo: new_info,
            validation: this.getNewInputValidation(new_info),
        });
    }

    changeDescriptionHandler = (data) => {
        const new_info = {
            ...this.state.updateInfo,
        }
        new_info.description = data;
        new_info.descriptionPlainText = (data.toString('markdown'));

        this.setState({
            ...this.state,
            updateInfo: new_info,
            validation: this.getNewInputValidation(new_info),
        });
    }

    //Lifecycle ------------------------------------------------------------------------------
    componentDidMount() {
        //Setup initial infos
        Requests.wrappedRequest(this.props.signalCancelToken , Requests.getProfile, { profileId: this.state.profileId })
        .then(res => {
            //Check if we have the right to edit this profile
            if(res.userId === +this.props.id){
                if(!res.sentence){
                    res.sentence = '';
                }
                res.descriptionPlainText = res.description;
                res.description = RichTextEditor.createValueFromString(res.descriptionPlainText, 'markdown');

                this.setState({
                    ...this.state,
                    initialInfo: {...res},
                    updateInfo: {...res},
                    validation: this.getNewInputValidation(res, true),
                    loading: false,
                });
            }
            else{
                this.goBack();
            }
        })
        .catch(this.goBack);
    }

    render() {
        if(this.state.loading){
            return(
                <Waiting />
            );
        }
        else{
            return (
                <React.Fragment>
                    <InfoModal 
                        title="Profil mis à jour avec succès !" 
                        text="Les autres utilisateurs vous verront maintenant sous un nouveau jour."
                        state='info'
                        textBtn = 'Jeter un oeil'
                        open={this.state.updatedProfile} 
                        onClose = {this.goToProfile}
                        onConfirm = {this.goToProfile}
                    />
    
                    <InfoModal 
                        title="Impossible de mettre à jour le profil !" 
                        text={this.state.updateError}
                        state='error' 
                        open={this.state.updateError} 
                        onClose = {this.goBack}
                        onConfirm = {this.goBack}
                    />
    
                    <UpdateForm 
                        firstName={ this.state.updateInfo.firstName }
                        lastName= { this.state.updateInfo.lastName }
                        sentence= { this.state.updateInfo.sentence }

                        onChangedFirstName = { this.changeTextInputHandler }
                        onChangedLastName = { this.changeTextInputHandler }
                        onChangeSentence = { this.changeTextInputHandler }
                        onChangeDescription = { this.changeDescriptionHandler }
    
                        firstNameError = { !this.state.validation.firstName }
                        lastNameError = { !this.state.validation.lastName }
                        sentenceError = { !this.state.validation.sentence }
                        descriptionError = { !this.state.validation.description }
                        updateIsStart = { this.state.validation.updateIsStart }
    
                        descriptionData = {this.state.updateInfo.description}
                        currentLength = {this.state.updateInfo.descriptionPlainText.length}
                        maxDescriptionLength = { maxDescriptionLength }
    
                        onConfirm={ this.onUpdateProfileHandler }
                        onCancel = { this.goBack }
                    />
                </React.Fragment>      
            );
        }
    }
}

const mapStateToProps = state => {
    return {
        token: state.Auth.token,
        id: state.Auth.userId,
        profileId: state.UserData.profileId,
    };
}

const mapDispatchToProps =  dispatch => {
    return {
        onResetUserData: () => dispatch(UserData.resetUser()),
    } 
  }

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(withRequestsCanceller(UpdatePage)));