import React, { Component } from 'react'
import { Switch, Redirect, Route } from 'react-router-dom'
import { connect } from 'react-redux';

import Routes from '../../routing/routes';
import PrivateRoute from '../../Components/Auth/PrivateRoute/PrivateRoute';

import CreationPage from './CreationPage/CreationPage';
import UpdatePage from './UpdatePage/UpdatePage';
import Profile from './Profile/Profile';

class ProfileRouter extends Component{
    
    render(){
        return (
                <Switch> 
                    
                    <PrivateRoute //Create ambassador profile
                        exact 
                        path = { this.props.match.path + '/create' }
                        component= { CreationPage }
                        condition={!this.props.isAmbassador && this.props.isAuthenticated}
                    />

                    <PrivateRoute //Update ambassador profile
                        exact 
                        path = { this.props.match.path + '/edit' }
                        component= { UpdatePage }
                        condition={this.props.isAmbassador && this.props.isAuthenticated}
                    />

                    <PrivateRoute //Update ambassador profile
                        exact 
                        path = { this.props.match.path + '/me' }
                        component= { Profile }
                        condition={this.props.isAmbassador && this.props.isAuthenticated}
                    />

                    <Route //Proceed to a profile
                        exact
                        path = { this.props.match.path + '/:id' }
                        component= {Profile}
                    />

                    <Redirect to = { Routes.pageNotFound } />

                </Switch>     
        )
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.Auth.token !== null,
        isAmbassador: state.UserData.profileId && state.UserData.profileId !== -1,
    };
}


export default connect(mapStateToProps, null)(ProfileRouter);