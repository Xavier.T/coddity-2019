import  React, { Component } from 'react'
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { withRequestsCanceller } from '../../../HOC/RequestsCanceller/RequestsCanceller';

import { UserData } from '../../../store/actions/actionsIndex';
import CreationForm from '../../../Components/Profile/CreationForm/CreationForm';
import InfoModal from '../../../Components/UI/Modals/InfoModal/InfoModal';
import Routes from '../../../routing/routes';
import * as Requests from '../../../Helpers/Requests/Requests';
import RichTextEditor from 'react-rte';
import {word, w} from '../../../Helpers/consts/Vocab/vocab';
import PageTitle from '../../../Components/PageTitle/PageTitle';

const styles = theme => ({
    Panel: {
        maxWidth: '500px',

        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: '40px',
    },
});

class CreationPage extends Component{

    constructor(props){
        super(props);

        this.state = {
            createdProfile:false,
            creationError:false,
        }
    }

    onCreateProfileHandler = (args) => {
        //Requête de création de profil
        if(!args.description){
            args.description = RichTextEditor.createValueFromString(word(w.Proud, this.props.appMode)+' d\'être '+ word(w.Ambassador, this.props.appMode) +' !', 'markdown').toString('markdown');
        }
        Requests.wrappedRequest(this.props.signalCancelToken, Requests.createProfile, {...args, token: this.props.token })
        .then(res => {
            this.setState({
                ...this.state,
                creationError: false,
                createdProfile: true,
            });
        })
        .catch(err => {
            if(err !== Requests.cancelErrorCode){
                this.setState({
                    ...this.state,
                    creationError: err,
                    createdProfile: false,
                });
            }
        })
    }

    goToProfile = () => {
        this.props.onResetUserData();
        this.props.history.push(Routes.myprofile);
    }

    goHome = () => {
        this.props.history.push(Routes.home);
    }

    render(){

        const {classes} = this.props;
        
        return (
            <div className={classes.Panel}>
                <InfoModal 
                    title="Profil crée avec succès !" 
                    text="Vous pouvez le personnaliser dans la section Profil."
                    state='info' 
                    open={this.state.createdProfile} 
                    onClose = {this.goToProfile}
                    onConfirm = {this.goToProfile}
                />

                <InfoModal 
                    title="Impossible de créer le profil" 
                    text={this.state.creationError}
                    state='error' 
                    open={this.state.creationError} 
                    onClose = {this.goHome}
                    onConfirm = {this.goHome}
                />

                <PageTitle 
                    title="Partage les pistes que tu as toi-même prise !"
                    subtitle={"Si tu deviens "+ word(w.Ambassador, this.props.appMode) + ", il te faut une identité."}
                />

                <CreationForm 
                    appMode={this.props.appMode}
                    onCreateProfile={this.onCreateProfileHandler}/>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        token: state.Auth.token,
        id: state.Auth.userId,
        appMode: state.AppMode.appMode,
    };
}

const mapDispatchToProps =  dispatch => {
    return {
        onResetUserData: () => dispatch(UserData.resetUser()),
    } 
  }

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(withRequestsCanceller(CreationPage)));