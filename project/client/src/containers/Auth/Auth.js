import React, { Component } from 'react'
import { Switch, Redirect, Route} from 'react-router-dom'
import { connect } from 'react-redux';


import Routes from '../../routing/routes';
import PrivateRoute from '../../Components/Auth/PrivateRoute/PrivateRoute';

import Login from './Login/Login';
import Register from './Register/Register';
import Logout from './Logout/Logout';
import Validation from './Validation/Validation';

class Auth extends Component{

    render(){
        return (
                <Switch> 
                    
                    <PrivateRoute //Login and registration
                        exact 
                        path = { this.props.match.path + '/login' }
                        component= {Login}
                        condition={!this.props.isAuthenticated}
                    />

                    <PrivateRoute //Login and registration
                        exact 
                        path = { this.props.match.path + '/register' }
                        component= {Register}
                        condition={!this.props.isAuthenticated}
                    />

                    <Route  //Logout route
                        path = { this.props.match.path + '/logout'}
                        component = { Logout }
                    />
                    
                    <Route //Validation route
                        path = { this.props.match.path + '/validation'}
                        component ={ Validation }
                    />


                    <Redirect to = { Routes.pageNotFound } />

                </Switch>     
        )
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.Auth.token !== null,
    };
}


export default connect(mapStateToProps, null)(Auth);