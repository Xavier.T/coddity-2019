import React, { Component } from 'react'
import axios from 'axios';

import Routes from '../../../routing/routes';
import ServerEndPoints from '../../../routing/serverEndpoints';
import { withStyles, Typography } from '@material-ui/core';

import Paper from '@material-ui/core/Paper';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';

const styles = theme => ({
    Panel: {
        maxWidth: '500px',

        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: '40px',

        padding: theme.spacing.unit * 2,
        textAlign: 'center',
    },
});

class Validation extends Component{


    constructor(props) {
        super(props);

        //Etats utiles pour la validation de l'email
        let validationStep = null;
        let validationInfo = null;
        const params = new URLSearchParams(this.props.location.search); 
        const validationToken = params.get('validationToken');
        const username = params.get('username');
        if(validationToken && username){
            validationStep = 'processing';
            validationInfo = {
                validationToken: validationToken,
                username: username,
            }
        }

        if(!validationStep){
            props.history.push(Routes.home);
        }

        this.state = {
            validationStep: validationStep,
            validationInfo: validationInfo,
        };

        this.validationHandler()
    }

    //Handlers ---------------------------------------------------------------------


    validationHandler(){
        const delay = 1500;
        axios.post(ServerEndPoints.serverDomain + ServerEndPoints.auth.confirmEmail, this.state.validationInfo)
        .then(res => {
            setTimeout(() => {
                this.setState({...this.state, validationStep: 'success'});
            }, delay);
        })
        .catch(err => {
            setTimeout(() => {
                this.setState({...this.state, validationStep: 'fail'});
            }, delay);
        })
    }

    goHome = () => {
        this.props.history.push(Routes.home);
    }

    goLogin = () => {
        this.props.history.push(Routes.login);
    }

    //Lifecycle  ----------------------------------------------------------------------

    render(){
        const {classes} = this.props;

        if(this.state.validationStep === 'processing'){
            return (
                <Paper className={classes.Panel}>
                    <Typography variant="h5">Validation en cours</Typography>
                    <CircularProgress style={{margin: '15px'}}/>
                </Paper>);
        }
        else if(this.state.validationStep === 'success'){
            return (
                <Paper className={classes.Panel}>
                    <Typography variant="h5">Validation réussie !</Typography>
                    <Typography variant="body1" style={{margin: '15px'}}>Bienvenue à toi nouvel explorateur !</Typography>
                    <Button variant="contained" color="primary" onClick={this.goLogin}>C'est parti !</Button>
                </Paper>);
        }
        else if(this.state.validationStep === 'fail'){
            return (
                <Paper className={classes.Panel}>
                    <Typography variant="h5">La validation a échouée</Typography>
                    <Typography variant="body1" style={{margin: '15px'}}>Impossible de confirmer le compte (Il est peut-être déjà confirmé ou bien a expiré)</Typography>
                    <Button variant="contained" color="primary" onClick={this.goHome}>Retourner à l'accueil</Button>
                </Paper>);
        }
        return null;
    }
}

export default withStyles(styles)(Validation);