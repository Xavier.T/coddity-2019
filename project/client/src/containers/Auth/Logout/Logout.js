import { Component } from 'react';
import Routes from '../../../routing/routes';

import { connect } from 'react-redux';
import { Authentication } from '../../../store/actions/actionsIndex';

class Logout extends Component{

    state = {

    };


    static getDerivedStateFromProps(props, state){
        
        if(props.isAuthenticated === true){
            props.onUserLogout();
        }
        else{
            props.history.push(Routes.home);
        }
        
        return state;
    }

    render(){
        return null;
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.Auth.token !== null,
    };
}

const mapDispatchToProps =  dispatch => {
    return {
        onUserLogout: () => dispatch(Authentication.logout()),
    } 
}

export default connect(mapStateToProps, mapDispatchToProps)(Logout);