import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';

import { Authentication } from '../../../store/actions/actionsIndex';

import Routes from '../../../routing/routes';
import LoginPanel from '../../../Components/Auth/LoginPanel/LoginPanel';
import InfoModal from '../../../Components/UI/Modals/InfoModal/InfoModal';
import PageTitle from '../../../Components/PageTitle/PageTitle';

const styles = theme => ({
    Panel: {
        maxWidth: '500px',

        marginLeft: 'auto',
        marginRight: 'auto',
    },
});

class Login extends Component{

    constructor(props) {
        super(props);

        //Etats utiles pour la connexion
        let redirectURL = Routes.home;
        if(this.props.location.state){
            redirectURL = this.props.location.state.redirect;
        }

        this.state = {
            redirect: redirectURL,
            currentError: null,
        };
    }

    //Handlers ---------------------------------------------------------------------
    resetErrorHandler = () => {
        this.setState({
            ...this.state,
            currentError: null,
        })
        this.props.onUserLeave(); 
    }

    loginHandler = (args) => {
        if(args.email.length === 0 || args.psw.length === 0){
            this.setState({
                ...this.state,
                currentError: 'Il semble qu\'un ou plusieurs champ ne soit pas rempli !',
            });
            this.props.onUserLeave(); 
        }
        else{
            this.props.onUserLogin(args.email, args.psw);
        }
        
    }

    //Lifecycle  ----------------------------------------------------------------------

    static getDerivedStateFromProps(props, state){
        if(props.authError){
            state = {
                ...state,
                currentError: props.authError,
            }
        }
        return state;
    }

    render(){
        const {classes} = this.props;
        return (
            <React.Fragment>
                
                <PageTitle 
                    title="Récupère ta boussole ..."
                    subtitle={"Et n'oublie pas tes bottes !"}
                />

                <InfoModal
                    state="error"
                    open={this.state.currentError}
                    onClose={this.resetErrorHandler}
                    onConfirm={this.resetErrorHandler}
                    title="Connexion impossible"
                    text={this.state.currentError}
                />

                <div className={classes.Panel}> 
                    <LoginPanel 
                        onLog={this.loginHandler} 
                        //errorMessage={this.state.currentError}
                        loading={this.props.isAuthLoading}
                    />
                </div>
                
            </React.Fragment>
            
            
        )
    }
}

//Redux ------------------------------------------------------------------
const mapStateToProps = state => {
    return {
        isAuthenticated: state.Auth.token !== null,
        isAuthLoading: state.Auth.loading,
        authError: state.Auth.error,
    };
}

const mapDispatchToProps =  dispatch => {
    return {
        onUserLogin: (email, password) => dispatch(Authentication.authenticate(email, password)),
        onUserLeave: () => dispatch(Authentication.authReset()),
    } 
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Login));