import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles';
import { withRequestsCanceller } from '../../../HOC/RequestsCanceller/RequestsCanceller';
import * as Requests from '../../../Helpers/Requests/Requests';
import { connect } from 'react-redux';

import Routes from '../../../routing/routes';
import RegisterPanel from '../../../Components/Auth/RegisterPanel/RegisterPanel';
import InfoModal from '../../../Components/UI/Modals/InfoModal/InfoModal';
import PageTitle from '../../../Components/PageTitle/PageTitle';
import {word, w} from '../../../Helpers/consts/Vocab/vocab';

const styles = theme => ({
    Panel: {
        maxWidth: '500px',

        marginLeft: 'auto',
        marginRight: 'auto',
    },
});

class Register extends Component{


    constructor(props) {
        super(props);

        this.state = {
            registrationError: null,
            registrationSuccess: false,
        };
    }

    //Handlers ---------------------------------------------------------------------
    resetErrorState = () => {
        this.setState({...this.state, registrationError: null});
    }

    redirect = () => {
        this.setState({
            registrationError: null,
            registrationSuccess: null,
        });
        this.props.history.push(Routes.home);
    }

    registerHandler = (args) => {
        Requests.wrappedRequest(this.props.signalCancelToken, Requests.postRegister, {
            email : args.email,
            username: args.username,
            gender: args.gender,
            password: args.psw,
            password_confirm: args.psw_confirm,
        })
        .then(res => {
            this.setState({
                ...this.state,
                registrationSuccess: res.delay,
            });
        })
        .catch(err => {
            if(err !== Request.cancelErrorCode){
                this.setState({
                    ...this.state, 
                    registrationError: err,
                });
            }
        })
    }

    generateErrorText = () => {
        let text = "";
        if(this.state.registrationError){
            this.state.registrationError.forEach(e => {
                text += e + '\n';
            });
            return text;
        }
    }

    //Lifecycle  ----------------------------------------------------------------------

    render(){
        const {classes} = this.props;

        return (

            <React.Fragment>
                
                <InfoModal
                    state="error"
                    open={this.state.registrationError}
                    onClose={this.resetErrorState}
                    onConfirm={this.resetErrorState}
                    title="L'inscription a échouée"
                    text={this.state.registrationError}
                />
                
                <InfoModal
                    state="info"
                    open={this.state.registrationSuccess}
                    onClose={this.redirect}
                    onConfirm={this.redirect}
                    title="Inscription réussie !"
                    text= { [
                        "Vous disposez de "+ this.state.registrationSuccess +" minutes afin de confirmer votre inscription grâce au mail qui vous a été envoyé !", 
                        "Après ces "+this.state.registrationSuccess+" minutes sans confirmation, il faudra recréer un compte."
                    ] }
                />
                
                <PageTitle 
                    title={word(w.Ready, this.props.appMode) + " à démarrer l'aventure ?"}
                    subtitle={"Remplis les champs suivants et reçoit ta boussole d'"+ word(w.Explorer, this.props.appMode) +"!"}
                />

                <div className={classes.Panel}>
                    <RegisterPanel onRegister={this.registerHandler} />
                </div> 
            </React.Fragment>
            
        )
    }
}

const mapStateToProps = state => {
    return {
        appMode: state.AppMode.appMode,
    };
}

export default connect(mapStateToProps, null)(withStyles(styles)(withRequestsCanceller(Register)));