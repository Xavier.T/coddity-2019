import React, { Component } from 'react';
import {withStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import LinearProgress from '@material-ui/core/LinearProgress';
import Fade from '@material-ui/core/Fade';
import * as Requests from '../../Helpers/Requests/Requests';
import SearchIcon from '@material-ui/icons/Search';

import PostFastDisplay from '../../Components/Post/PostFastDisplay/PostFastDisplay';
import Routes from '../../routing/routes';
import { withRequestsCanceller } from '../../HOC/RequestsCanceller/RequestsCanceller';
import { Typography } from '@material-ui/core';
import Waiting from '../../Components/UI/Waiting/Waiting';
import Error from '../../Components/UI/Error/Error';
import PageTitle from '../../Components/PageTitle/PageTitle';

const transitionTime = 600;

const styles = theme => ({

    Container: {
        width: '80%',
        maxWidth: '1000px',
        margin: 'auto',
        paddingBottom: theme.spacing.unit * 10,
    },
    WaitingErrorContainer:{
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
    },
    [theme.breakpoints.down('xs')]: {
        Container: {
            overflow: 'auto',
            width: '95%',
        },
    },
});

class Discover extends Component {

    constructor(props){
        super(props)

        this.timeIntervalId = null;
        this.updatePostTimeout = null;
        this.transitionTimeout = null;

        this.state = {
            timeError: null,
            postError: null,
            timeLoading: true,
            postLoading: true,

            startDate: null,
            interval: null,
            remainingTime: null,

            currentPost: null,
            loadingNew: false,

            currentlyCrash: false,
        }
    }

    getTimeInfo(){
        this.setState({
            ...this.state,
            timeLoading: true,
            timeError: false,
        }, () => {
            Requests.wrappedRequest(this.props.signalCancelToken, Requests.getDiscoverTimeInfo)
            .then(res => {
                this.setState({
                    ...this.state,
                    timeLoading: false,
                    startDate: res.startDate,
                    interval: res.interval,
                    remainingTime: this.getRemaningMilli(res.interval, res.startDate),
                });
                
            })
            .catch(err => {
                if(err !== Requests.cancelErrorCode){
                    this.setState({
                        ...this.state,
                        timeLoading: false,
                        timeError: err,
                    })
                }
            })
        })
        
    }

    updateRemainingTime(){
        if(this.state.interval && this.state.startDate){
            this.setState({
                remainingTime: this.getRemaningMilli(this.state.interval, this.state.startDate),
            });
        }
    }

    getPostInfo(){
        this.setState({
            ...this.state,
            postLoading: true,
            postError: false,
        }, () => {

            Requests.wrappedRequest(this.props.signalCancelToken, Requests.getDiscoverPostInfo)
            .then(res => {
                if(this.state.currentPost){
                    this.transitionTimeout = setTimeout(() => {
                        
                        this.setState({
                            currentPost: {...res},
                            postLoading: false,
                            currentlyCrash: false,
                        });
                        if(this.updatePostTimeout){
                            clearTimeout(this.updatePostTimeout);
                        }
                        this.updatePostTimeout = setTimeout(this.getPostInfo.bind(this), this.state.remainingTime);
                    }, transitionTime*2);
                }
                else{
                    this.setState({
                        ...this.state,
                        currentPost: {...res},
                        postLoading: false,
                        currentlyCrash: false,
                    });
                    this.updatePostTimeout = setTimeout(this.getPostInfo.bind(this), this.state.remainingTime);
                }
            })
            .catch(err => {
                if(err !== Requests.cancelErrorCode){
                    this.setState({
                        ...this.state,
                        postLoading: false,
                        postError: err,
                        currentlyCrash: true,
                    })
                }
            });

        });
        
    }

    refresh = () => {
        this.getTimeInfo(); //On lance la requête des infos sur le time de discover
        this.getPostInfo(); //On lance la requête du profil à découvrir
        if(this.timeIntervalId){
            clearInterval(this.timeIntervalId);
        }
        this.timeIntervalId = setInterval(this.updateRemainingTime.bind(this), 1000);
    }

    getRemaningMilli = (interval, startDate) => {
        return interval - ((Date.now() - startDate) % interval);
    }

    getWrittenTiming = () => {
        const duration = this.state.remainingTime;
        let seconds = parseInt((duration / 1000) % 60);
        let minutes = parseInt((duration / (1000 * 60)) % 60);
        minutes = (minutes < 10) ? "0" + minutes : minutes;
        seconds = (seconds < 10) ? "0" + seconds : seconds;
        return minutes + ":" + seconds ;
   }

   goToProfilePost = () => {
        this.props.history.push(Routes.post + this.state.currentPost.id);
    }

    componentDidMount(){
        this.refresh();
    }

    componentWillUnmount(){
        if(this.timeIntervalId){
            clearInterval(this.timeIntervalId);
        }
        if(this.transitionTimeout){
            clearTimeout(this.transitionTimeout);
        }
        if(this.updatePostTimeout){
            clearTimeout(this.updatePostTimeout);
        }
    }

    render(){
        const {classes} = this.props;
        if(this.state.postError || this.state.timeInfoError){
            return (
                <Error text={this.state.postError} onRefresh={this.refresh}/>
            );
        }
        else if(this.state.timeLoading || (!this.state.currentPost && this.state.postLoading) || this.state.currentlyCrash){
            return (
                <Waiting />
            );
        }

        let timer = null;
        if(this.state.remainingTime !== null){
            timer = (
                <div className={classes.Container}>
                    <Typography align='center' variant="h5">{this.getWrittenTiming()}</Typography>
                    <br />
                    <LinearProgress variant="determinate" value={Math.floor((this.state.remainingTime / this.state.interval ) *100)}/>
                </div>
            );
        }

        let content = null;
        if(this.state.currentPost && this.state.remainingTime !== null){ //Si on a un post déjà découvert
            content = (
                    <div className={classes.Container}>
                        <Fade in={!this.state.postLoading} timeout={transitionTime}>
                            <Paper className={classes.PostContainer} >
                                {<PostFastDisplay 
                                    key={this.state.currentPost.id}
                                    title={this.state.currentPost.title}
                                    opportunities={this.state.currentPost.opportunities}
                                    action={{
                                        text: 'Découvrir',
                                        onClick: this.goToProfilePost,
                                        icon: SearchIcon,
                                    }}
                                />}
                            </Paper>
                        </Fade>
                    </div>
            );
        }

        const discoverOne = (
            <React.Fragment>
                <PageTitle 
                    title={"Promène toi en ouvrant l'oeil et le bon !"}
                    subtitle={"Les pistes s'estompent rapidement par ici..."}
                />
                {timer}
                {content}
            </React.Fragment>
        );

        return(discoverOne)
    };
    
};


export default withStyles(styles)(withRequestsCanceller(Discover));