import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import StarIcon from '@material-ui/icons/Star';
import InfiniteScroll from 'react-infinite-scroller';
import Paper from '@material-ui/core/Paper';
import SearchIcon from '@material-ui/icons/Search';

import { withRequestsCanceller } from '../../HOC/RequestsCanceller/RequestsCanceller';
import OpportunityPickerList from '../../Components/Opportunity/OpportunityPickerList/OpportunityPickerList'
import * as Requests from '../../Helpers/Requests/Requests';
import Waiting from '../../Components/UI/Waiting/Waiting';
import Error from '../../Components/UI/Error/Error';
import PostFastDisplay from '../../Components/Post/PostFastDisplay/PostFastDisplay';
import Routes from '../../routing/routes';
import compareIntArray from '../../Helpers/compareIntArray';
import { Explore } from '../../store/actions/actionsIndex';
import { Typography } from '@material-ui/core';
import PageTitle from '../../Components/PageTitle/PageTitle';

const maxCheckedForSearch = 5;

const styles = theme => ({
    
    Container: {
        width: '90%',
        maxWidth: '1200px',
        margin: 'auto',
        paddingBottom: theme.spacing.unit * 10,
    },
    SearchButtonContainer:{
        marginTop: theme.spacing.unit * 5,
        marginBottom: theme.spacing.unit * 5,
        display: 'flex',
        justifyContent: 'center',
    },
    ActionIcon: {
        marginLeft: theme.spacing.unit,
    },
    PostContainer: {
        marginTop: theme.spacing.unit * 10,
        marginBottom: theme.spacing.unit * 10,
    },
    [theme.breakpoints.down('xs')]: {
        Container: {
            overflow: 'auto',
            width: '95%',
        },
    },
});

class OpportunitySearch extends Component {
    
    constructor(props){
        super(props);

        this.resultStart = React.createRef();

        let selectedOpportunities = [];
        if(this.props.ExploreOpportunities.length > 0){
            selectedOpportunities = [...this.props.ExploreOpportunities];
        }

        this.state = {
            opportunitiesLoading: true,
            error: null,
            opportunities: [],
            selectedOpportunities: selectedOpportunities,
            previousSelectedOpportunities: [],

            startedResearch: false,
            reachEndOfPosts: true,
            loadedPosts: [],
            completePostList: [],
            loading: false,
        }
    }

    goToPost = (id) => {
        this.props.history.push(Routes.post + id);
        this.props.saveSearch(this.state.selectedOpportunities);
    }

    changeOpportunityHandler = (event, id) => {
        const newSelectedOpportunities = [...this.state.selectedOpportunities];
        if(event.target.checked){ //Soit on ajoute
            newSelectedOpportunities.push(id);
        }
        else{ //Soit on enlève
            let pos = newSelectedOpportunities.indexOf(id);
            newSelectedOpportunities.splice(pos, 1);
        }

        this.setState({
            ...this.state,
            selectedOpportunities: newSelectedOpportunities,
        })
    }

    generateSelectedOpportunities = () => {
        const selected = [];
        this.state.opportunities.forEach(elem => {
            if(this.state.selectedOpportunities.includes(+elem.id)){
                selected.push(elem);
            }
        });
        return selected;
    }

    loadAllOpportunities = () => {
        return Requests.wrappedRequest(
            this.props.signalCancelToken,
            Requests.getAllOpportunities,
            {
                token: this.props.token,
            },
        )
        .then(res => {
            this.setState({
                ...this.state,
                opportunities: res.opportunities,
                opportunitiesLoading: false,
            });
        })
        .catch(this.setErrorMode);
    }

    onLoadMorePost = (force = false) => {
        
        if(this.state.completePostList.length > 0 || force === true){
            if(this.state.completePostList.length === this.state.loadedPosts.length){
                return this.setState({
                    ...this.state,
                    reachEndOfPosts: true,
                });
            }
            Requests.wrappedRequest(
                this.props.signalCancelToken,
                Requests.getPost,
                {
                    postId: this.state.loadedPosts[this.state.completePostList.length],
                }
            )
            .then(res => {
                const isStart = this.state.completePostList.length === 0;
                const newLoadedPosts = [...this.state.completePostList];
                newLoadedPosts.push(res);
                this.setState({
                    ...this.state,
                    completePostList: newLoadedPosts,
                    loading: false,
                }, () => {
                    
                    if(isStart && newLoadedPosts.length > 0){
                        console.log(isStart, newLoadedPosts.length > 0);
                        this.scrollOnStartOfPosts();
                    }
                })
            })
            .catch(this.setErrorMode);
        }
    }

    getResearchList = () => {
        this.setState({
            ...this.state,
            startedResearch: true,
            loading: true,
            completePostList: [],
            loadedPosts: [],
        }, () => {
            Requests.wrappedRequest(
                this.props.signalCancelToken,
                Requests.getPostByOpportunities,
                {
                    token: this.props.token,
                    opportunities: this.state.selectedOpportunities,
                }
            )
            .then(res => {
                this.setState({
                    ...this.state,
                    reachEndOfPosts: false,
                    loadedPosts: res.matchingPosts,
                    previousSelectedOpportunities: this.state.selectedOpportunities,
                }, () => {this.onLoadMorePost(true)});
            })
            .catch(this.setErrorMode);

        });
        
        
    }

    setErrorMode = (err) => {
        if(err !== Requests.cancelErrorCode){
            this.setState({
                ...this.state,
                error: err,
                opportunitiesLoading: false,
            });
        }
    }

    scrollOnStartOfPosts = () => {
        this.resultStart.scrollIntoView({behavior: 'smooth', block:'start', inline: 'nearest'});
    }

    opportunitySelectionDidChange = () => {
        return !compareIntArray(this.state.selectedOpportunities, this.state.previousSelectedOpportunities)
    }

    componentDidMount(){
        this.loadAllOpportunities()
        .then(() => {
            if(this.state.selectedOpportunities.length > 0){
                this.getResearchList();
            }
        });
        
    }

    render(){
        const {classes} = this.props;

        const errorDisplay = (
            this.state.error ?
                <div className={classes.Container}>
                    <Error text={this.state.error}/>
                </div>
            :
                null
        );

        const postFastDisplayList = (
            this.state.completePostList.length > 0 ?
            this.state.completePostList.map(post => {
                return (
                    <Paper className={classes.PostContainer} key={post.id}>
                        {<PostFastDisplay 
                            title={post.title}
                            opportunities={post.opportunities}
                            action={{
                                text: 'Observer',
                                onClick: () => {this.goToPost(post.id)},
                                icon: SearchIcon,
                            }}
                        />}
                    </Paper>
                );
            })
            :
                this.state.startedResearch && !this.state.loading?
                <div className={classes.PostContainer}>
                    <Typography variant="h5" color='primary' align='center'>Oups, aucune piste n'a pu être trouvée !</Typography>
                </div>
                :
                this.state.loading ?
                <Waiting />
                :
                null
        );
        return(
            <React.Fragment>
                <PageTitle 
                    title="Explore les pistes grâce aux opportunités qu'elles offrent !"
                    subtitle="Ajoute à ta collection celles qui te plaisent le plus"
                />
                {!this.state.error ?
                    <div className={classes.Container}>
                        <OpportunityPickerList 
                            loading={this.state.opportunitiesLoading}
                            opportunityError={this.state.error}
                            opportunities={this.state.opportunities}
                            selectedOpportunities={this.generateSelectedOpportunities()}
                            maxCheckedOpportunity={maxCheckedForSearch}
                            checkedOpportunities={this.state.selectedOpportunities}
                            onChangeOpportunity={this.changeOpportunityHandler}
                        />
                        {
                            !this.state.opportunitiesLoading ?
                                <div className={classes.SearchButtonContainer}>
                                    <Button 
                                        color='secondary' 
                                        variant="contained" 
                                        onClick={this.getResearchList} 
                                        disabled={!this.opportunitySelectionDidChange() || this.state.selectedOpportunities.length === 0}>
                                            Explorer
                                        <StarIcon className={classes.ActionIcon}/>
                                    </Button>
                                </div>
                            :
                                null
                        }
                        
                        <div>
                            <InfiniteScroll
                                pageStart={this.state.completePostList.length}
                                loadMore={this.onLoadMorePost}
                                hasMore={!this.state.reachEndOfPosts && this.state.startedResearch}
                                loader={<div className="loader" key={0}><Waiting /></div>}
                            >
                                <div ref={(el) => { this.resultStart = el; }}></div>

                                {postFastDisplayList}

                            </InfiniteScroll>
                        </div>
                    </div>
                :
                    errorDisplay
                }
            </React.Fragment>
            

            
        );
    }
};

const mapStateToProps = state => {
    return {
        token: state.Auth.token,
        userGender: state.UserData.gender,
        ExploreOpportunities: state.Explore.selectedOpportunities,
    };
  }

const mapDispatchToProps =  dispatch => {
    return {
        saveSearch: (selectedOpportunities) => dispatch(Explore.saveSearch(selectedOpportunities)),
        resetSearch: () => dispatch(Explore.resetSearch()),
    } 
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(withRequestsCanceller(OpportunitySearch)));