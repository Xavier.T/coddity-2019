import React, { Component } from 'react';
import { connect } from 'react-redux';
import  { withStyles } from '@material-ui/core/styles';

import { withRequestsCanceller } from '../../HOC/RequestsCanceller/RequestsCanceller';
import PostFavDisplay from '../../Components/Post/PostFavDisplay/PostFavDisplay';
import { Notification } from '../../store/actions/actionsIndex';
import Routes from '../../routing/routes';
import PageTitle from '../../Components/PageTitle/PageTitle';

const styles = ({
    
    Container:{
        width: '80%',
        maxWidth: '1000px',
        margin: 'auto',
    },

});

class Notifications extends Component{

    constructor(props){
        super(props);

        this.state = {}
    }

    goToPost = (postId) => {
        this.props.history.push(Routes.post + postId);
    }

    goToProfile = (profileId) => {
        this.props.history.push(Routes.profile + profileId);
    }

    deleteNotif = (postId) => {
        this.props.closePost(this.props.token, postId);
    }

    render(){
        const {classes} = this.props;

        const notifs = (
            this.props.notifs.map(notif => {
                return (
                    <div key={notif.postId}>
                        <PostFavDisplay
                            title={notif.postTitle}
                            update={notif.update}
                            comments={notif.comments}
                            onGoToProfile={() => { this.goToProfile(notif.profileId); }}
                            onGoToPost={() => { this.goToPost(notif.postId); }}
                            onDelete={() => { this.deleteNotif(notif.postId); }}
                        />
                    </div>);
            })
        )  

        const noNotif = (
            this.props.notifs.length === 0 ?
            <PageTitle 
                subtitle="En l'occurence rien n'a changé "
            />
            :
            null
        );

        return (
            <React.Fragment>
                <PageTitle 
                    title="Une piste mise à jour ? Des nouvelles réactions ?"
                    subtitle="C'est ici que tu en seras notifié !"
                />
                {noNotif}
                <div className={classes.Container}>
                    {notifs}
                </div>
            </React.Fragment>  
        );
    };
}

const mapStateToProps = state => {
    return {
        token: state.Auth.token,
        notifs: state.Notifications.notifs,
    };
  }

const mapDispatchToProps =  dispatch => {
    return {
        closePost: (token, postId) => dispatch(Notification.closePost(token, postId)),
    } 
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(withRequestsCanceller(Notifications)));