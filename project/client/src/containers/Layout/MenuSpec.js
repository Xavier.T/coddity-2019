import StarIcon from '@material-ui/icons/Stars';
import NotificationIcon from '@material-ui/icons/Notifications';
import SettingsIcon from '@material-ui/icons/Settings';
import ArrowLeftIcon from '@material-ui/icons/ArrowLeft';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import ExploreIcon from '@material-ui/icons/Explore';
import FavoriteIcon from '@material-ui/icons/Favorite';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Home from '@material-ui/icons/Home';

import Routes from '../../routing/routes';

import {word, w} from '../../Helpers/consts/Vocab/vocab';

function NavItem(link, name, icon, notifCount = 0){
    this.link = link;
    this.name = name;
    this.icon = icon;
    this.notifCount = notifCount;
}

let login = new NavItem(Routes.login, '', ArrowRightIcon);
const become_explorator = new NavItem(Routes.register, '', ExploreIcon);
const become_ambassador = new NavItem(Routes.createProfile, '', SupervisorAccountIcon);
const discover_job = new NavItem(Routes.discover, "Découverte", StarIcon/*WorkIcon*/);
const search_by_opportunity = new NavItem(Routes.explore, "Exploration", ExploreIcon/*StarIcon*/);
const myfavorite = new NavItem(Routes.favorites, "Mes pistes", FavoriteIcon);
const notifications = new NavItem(Routes.notifications, "Nouveautés", NotificationIcon)
const parameters = new NavItem(Routes.myparam, "Paramètres", SettingsIcon);
const my_profile = new NavItem(Routes.myprofile, "Profil", AccountCircle);
const tech_it = new NavItem(Routes.home, "Camp de base", Home);
const logout = new NavItem(Routes.logout, "Déconnexion", ArrowLeftIcon);

//Functions
export const setMode = (mode) => {
    login.name = "Connexion";
    become_explorator.name = "Devenir "+word(w.Explorer, mode);
    become_ambassador.name = "Devenir "+word(w.Ambassador, mode);
}

export const setNotifCount = (count) => {
    notifications.notifCount = count;
}

//MenuList items
export const VisitorMenuList = [
    discover_job,
    "sep",
    login,
    become_explorator,
    "sep",
    tech_it,
];

export const ExplorerMenuList = [
    discover_job,
    search_by_opportunity,
    myfavorite,
    notifications,
    "sep",
    tech_it,
    become_ambassador,
    "sep",
    parameters,
    logout,
];

export const AmbassadorMenuList =  [
    discover_job,
    search_by_opportunity,
    my_profile,
    myfavorite,
    notifications,
    "sep",
    tech_it,
    "sep",
    parameters,
    logout,
];

//HeadBar List
export const VisitorHeadBar = [
    tech_it,
    discover_job,
    become_explorator,
    login,
];

export const ExplorerHeadBar = [
    discover_job,
    search_by_opportunity,
    myfavorite,
    notifications,
];

export const AmbassadorHeadBar = [
    discover_job,
    search_by_opportunity,
    my_profile,
    notifications,
];