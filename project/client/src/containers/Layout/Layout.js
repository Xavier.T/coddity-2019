import React, { Component } from 'react';
import { withStyles, withTheme } from '@material-ui/core/styles';

import HeadBar from '../../Components/HeadBar/HeadBar';
import MenuList from '../../Components/MenuList/MenuList';
import SideBar from '../../Components/SideBar/SideBar';

import { AppMode } from '../../store/actions/actionsIndex';
import { connect } from 'react-redux';
import * as grades from '../../Helpers/consts/grades';
import { VisitorMenuList, ExplorerMenuList, AmbassadorMenuList } from './MenuSpec';
import { VisitorHeadBar, ExplorerHeadBar, AmbassadorHeadBar } from './MenuSpec';
import { setMode } from './MenuSpec';
import { setNotifCount } from './MenuSpec';

const styles = theme => ({

    App: {
        minWidth: '300px',
    },
    Page: {
        marginLeft: '250px',
        minHeight: '100vh',
    },
    Content: {
        //marginTop: '40px',
        padding: '10px',
    },

    [theme.breakpoints.down('sm')]: {
        Menu: {
            display: 'none',
        },
        Page: {
            marginLeft: '0',
        }
    },
});

class Layout extends Component{

    state = {
        openSideBar: false,
        mobileMode: null,
        update: false,
    }

    //Handlers ---------------------------------------------------------------------------------------------

    goTo = (url) => {
        this.setState({...this.state, openSideBar: false});
        this.props.history.push(url);
    }

    getMenuList = () => {
        switch(this.props.profileId){
            case grades.visitor: return VisitorMenuList;
            case grades.explorer: return ExplorerMenuList;
            default: return AmbassadorMenuList;
        }
    }
    getHeadBarList = () => {
        switch(this.props.profileId){
            case grades.visitor: return VisitorHeadBar;
            case grades.explorer: return ExplorerHeadBar;
            default: return AmbassadorHeadBar;
        }
    }

    onToggleSideBar = () => {
        this.setState({...this.state, openSideBar: !this.state.openSideBar});
    }

    detectMobileMode = () => {
        const mobileMode = window.innerWidth < this.props.theme.breakpoints.values.md;
        //Ouvrir la sidebar quand on passe en mode écran
        if(this.state.mobileMode && !mobileMode){
            this.setState({ 
                ...this.state, 
                mobileMode: mobileMode,
                openSideBar: true,
            });
        }
        //Fermer la sidebar quand on passe en mode mobile
        else if(!this.state.mobileMode && mobileMode){
            this.setState({ 
                ...this.state, 
                mobileMode: mobileMode,
                openSideBar: false,
            });
        }
    }

    onSwitchAppMode = () => {
        this.props.onSwitchAppMode(this.props.appMode);
    }

    //Lifecycle ---------------------------------------------------------------------
    componentDidMount(){
        this.detectMobileMode();
        window.addEventListener('resize', this.detectMobileMode);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.detectMobileMode);
    }

    render(){
        setMode(this.props.appMode);
        setNotifCount(this.props.notifCount);
        
        const { classes } = this.props;

        return (
            <div className={classes.App}>
                
                <SideBar open={this.state.openSideBar} onClose={this.onToggleSideBar} mobileMode={this.state.mobileMode}>
                    <MenuList  
                        appMode={this.props.appMode}
                        onSwitchAppMode={this.onSwitchAppMode}
                        menuList={this.getMenuList()}
                        canSwitchMode={(this.props.userGender === null || this.props.userGender === 'O')}
                        goTo={this.goTo}
                    />
                </SideBar>
                
                <div className={classes.Page}>
                    <HeadBar 
                        citation="Entre dans la jungle..." 
                        menuList={this.getHeadBarList()} 
                        goTo={this.goTo}
                        openSideBar = {this.onToggleSideBar}
                    />
                    <div className={classes.Content}>
                        {this.props.children} 
                    </div>
                </div>      
       
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        profileId: state.UserData.profileId,
        userGender: state.UserData.gender,
        appMode: state.AppMode.appMode,
        notifCount: state.Notifications.notifs.length
    };
}

const mapDispatchToProps =  dispatch => {
    return {
        onSwitchAppMode: (currentMode) => dispatch(AppMode.toggleAppMode(currentMode)),
    } 
  }

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(withTheme()(Layout)));