import  React, { Component } from 'react'
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { withRequestsCanceller } from '../../../HOC/RequestsCanceller/RequestsCanceller';
import RichTextEditor from 'react-rte';

import * as Requests from '../../../Helpers/Requests/Requests';
import PostUpdateForm from '../../../Components/Post/UpdatePostForm/UpdatePostForm';
import InfoModal from '../../../Components/UI/Modals/InfoModal/InfoModal';
import Routes from '../../../routing/routes';
import Validator from '../../../Helpers/Validation/validation';
import Waiting from '../../../Components/UI/Waiting/Waiting';
import compareIntArray from '../../../Helpers/compareIntArray';
import { goBack } from '../../../routing/utils';
import PageTitle from '../../../Components/PageTitle/PageTitle';

const maxDescriptionLength = 2500;
const maxOpportunityToSelect = 5;

const validation_functions = {

    title: (value) => Validator.runValidation(value, [
        {check: Validator.isAtLeast, args: {min: 1}},
        {check: Validator.isAtMax, args: {max: 50}},
    ]).valid,

    description: (value) => Validator.runValidation(value, [
        {check: Validator.isAtMax, args: {max: maxDescriptionLength}},
    ]).valid,

    opportunities: (value) => value.length >= 1 && value.length <= 5,
}

const styles = theme => ({
    Panel: {
        maxWidth: '1200px',

        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: '40px',
    },
    WaitDiv: {
        display: 'flex',
    }
});

class PostUpdatePage extends Component{

    constructor(props){
        super(props);
        
        let isCreateMode = true;
        if(this.props.match.params.id){
            isCreateMode = false;
        }

        const startingInfo = {
            title: 'Une super piste !',
            selectedOpportunities: [],
            description: RichTextEditor.createValueFromString('### Une description à venir qui va déchirer !', 'markdown'),
            descriptionPlainText: 'Une description à venir qui va déchirer !',
        }

        const validation_info = {
            updateIsStart: true,

            title: true,
            description: true,
        }

        this.state = {
            initialInfo: {...startingInfo},
            updateInfo: {...startingInfo},
            validation: validation_info,
            loading: !isCreateMode,
            opportunityLoading: true,
            updatedPost: false,
            updateError: null,
            isCreateMode: isCreateMode,
            postId: !isCreateMode ? this.props.match.params.id : null,

            opportunities: [],
        }
    }
    //Handlers ------------------------------------------------------------------------------

    onUpdatePostHandler = () => {
        
        if(this.state.isCreateMode){    //Si on est en mode création
            Requests.wrappedRequest(this.props.signalCancelToken, Requests.createPost, {
                token: this.props.token,
                title: this.state.updateInfo.title,
                description: this.state.updateInfo.descriptionPlainText,
                opportunities: this.generateSelectedOpportunities(),
            })
            .then(res => {
                this.setState({
                    ...this.state,
                    updatedPost: true,
                })
            })
            .catch(err => {
                if(err !== Requests.cancelErrorCode){
                    this.setState({
                        ...this.state,
                        updateError: err,
                    })
                }
            })
        }
        else{   //Sinon
            Requests.wrappedRequest(
                this.props.signalCancelToken,
                Requests.updatePost,
                {
                    token: this.props.token,
                    postId: this.state.postId,
                    title: this.state.updateInfo.title,
                    opportunities: this.generateSelectedOpportunities(),
                    description: this.state.updateInfo.descriptionPlainText,
                }
            )
            .then(res => {
                this.setState({
                    ...this.state,
                    updatedPost: true,
                })
            })
            .catch(err => {
                if(err !== Requests.cancelErrorCode){
                    this.setState({
                        ...this.state,
                        updateError: err,
                    })
                }
            });
        }
        
    }

    goToProfile = () => {
        this.props.history.push(Routes.myprofile);
    }

    goToPost = () => {
        this.props.history.push(Routes.post + this.state.postId);
    }

    goBack = () => {
        goBack(this.props);
    }

    getNewInputValidation = (newInputs, forceUpdateIsStart=false) => {
        
        const new_validation = {
            ...this.state.validation,
        }
        new_validation.title = validation_functions.title(newInputs.title);
        new_validation.description = validation_functions.description(newInputs.descriptionPlainText);
        new_validation.opportunities = validation_functions.opportunities(newInputs.selectedOpportunities);

        //Checking update or not
        new_validation.updateIsStart = true;
        if(!forceUpdateIsStart){
            if(
                newInputs.title !== this.state.initialInfo.title ||
                !compareIntArray(newInputs.selectedOpportunities, this.state.initialInfo.selectedOpportunities) ||
                (newInputs.descriptionPlainText !== this.state.initialInfo.descriptionPlainText)
            ){
                new_validation.updateIsStart = false;
            }
        }

        return new_validation;
    }

    changeTitleHandler = (e) => {
        const new_info = {
            ...this.state.updateInfo,
        };
        new_info.title = e.target.value;
        this.setState({
            ...this.state,
            updateInfo: new_info,
            validation: this.getNewInputValidation(new_info)
        });
    }

    changeDescriptionHandler = (data) => {
        const new_info = {
            ...this.state.updateInfo,
        }
        new_info.description = data;
        new_info.descriptionPlainText = (data.toString('markdown'));

        this.setState({
            ...this.state,
            updateInfo: new_info,
            validation: this.getNewInputValidation(new_info),
        });
    }

    changeOpportunityHandler = (event, id) => {
        const newSelectedOpportunities = [...this.state.updateInfo.selectedOpportunities];
        if(event.target.checked){ //Soit on ajoute
            newSelectedOpportunities.push(id);
        }
        else{ //Soit on enlève
            let pos = newSelectedOpportunities.indexOf(id);
            newSelectedOpportunities.splice(pos, 1);
        }

        const new_info = {
                ...this.state.updateInfo,
                selectedOpportunities: newSelectedOpportunities,
        }
        this.setState({
            ...this.state,
            updateInfo: new_info,
            validation: this.getNewInputValidation(new_info),
        })
    }

    setErrorState = (err) => {
        return new Promise((resolve, reject) => {
            this.setState({
                ...this.state,
                updateError: err,
                loading: false,
                opportunityLoading: false,
            }, resolve);
        })
    }

    generateSelectedOpportunities = () => {
        const selected = [];
        this.state.opportunities.forEach(elem => {
            if(this.state.updateInfo.selectedOpportunities.includes(+elem.id)){
                selected.push(elem);
            }
        });
        return selected;
    }

    //Lifecycle ------------------------------------------------------------------------------
    componentDidMount() {

        //Chargement des différentes opportunités
        Requests.wrappedRequest(
            this.props.signalCancelToken,
            Requests.getAllOpportunities,
            {
                token: this.props.token,
            },
        )
        .then(res => {
            return new Promise((resolve, reject) => {
                this.setState({
                    ...this.state,
                    opportunities: res.opportunities,
                    opportunityLoading: false,
                }, resolve);
            })
        })
        .catch((err) => {
            if(err !== Requests.cancelErrorCode){
                this.setErrorState(err);
            }
        })
        .then(() => {
            if(!this.state.isCreateMode){ //Chargement du profil à éditer

                Requests.wrappedRequest(
                    this.props.signalCancelToken, 
                    Requests.getPost, 
                    {
                        postId: this.state.postId,
                    },
                )
                .then(res => {
                    const infos = {
                        title: res.title,
                        selectedOpportunities: res.opportunities.map(opp => opp.id),
                        descriptionPlainText: res.description,
                        description: RichTextEditor.createValueFromString(res.description, 'markdown'),
                    };
                    this.setState({
                        ...this.state,
                        initialInfo: infos,
                        updateInfo: infos,
                        postId: res.id,
                        validation: this.getNewInputValidation(infos, true),
                        loading: false,
                    })
                })
                .catch(err => {
                    if(err !== Requests.cancelErrorCode){
                        this.setErrorState(err);
                    }
                });
            }
        })
        
    }

    render() {
        if(this.state.loading){
            return(
                <Waiting />
            );
        }
        else{
            return (
                <React.Fragment>
                    <InfoModal 
                        title={this.state.isCreateMode ? 'Nouvelle piste créée avec succès !' : 'La piste a été mise à jour avec succès !'}
                        text= {this.state.isCreateMode ? 'Vous pouvez déjà la voir dans votre profil !' : 'Vous pouvez consulter les changements dès maintenat !'}
                        state='info'
                        textBtn = 'Jeter un oeil'
                        open={this.state.updatedPost} 
                        onClose = {this.state.isCreateMode ? this.goToProfile : this.goToPost }
                        onConfirm = {this.state.isCreateMode ? this.goToProfile : this.goToPost }
                    />
                    <InfoModal 
                        title="Une erreur est survenue !" 
                        text={this.state.updateError}
                        state='error' 
                        open={this.state.updateError} 
                        onClose = {this.goBack}
                        onConfirm = {this.goBack}
                    />

                    <PageTitle 
                        title="Partage une piste ..."
                        subtitle="En décrivant de manière enthousiasmante !"
                    />

                    <PostUpdateForm 
                        title={ this.state.updateInfo.title }

                        onChangedTitle = { this.changeTitleHandler }
                        onChangeDescription = { this.changeDescriptionHandler }
                        onChangeOpportunity = { this.changeOpportunityHandler }

                        titleError = { !this.state.validation.title }
                        descriptionError = { !this.state.validation.description }
                        opportunityError = { !this.state.validation.opportunities }
                        updateIsStart = { this.state.validation.updateIsStart }
    
                        descriptionData = {this.state.updateInfo.description}
                        currentLength = {this.state.updateInfo.descriptionPlainText.length}
                        maxDescriptionLength = { maxDescriptionLength }
    
                        opportunities={this.state.opportunities}
                        selectedOpportunities={this.generateSelectedOpportunities()}
                        maxCheckedOpportunity={maxOpportunityToSelect}
                        checkedOpportunities={ this.state.updateInfo.selectedOpportunities }
                        loadingOpportunities = { this.state.opportunityLoading }

                        onConfirm={ this.onUpdatePostHandler }
                        onCancel = { this.goBack }
                        isCreateMode = {this.state.isCreateMode }
                    />
                </React.Fragment>      
            );
        }
    }
}

const mapStateToProps = state => {
    return {
        token: state.Auth.token,
        id: state.Auth.userId,
    };
}


export default connect(mapStateToProps, null)(withStyles(styles)(withRequestsCanceller(PostUpdatePage)));