import React, { Component } from 'react';
import { connect } from 'react-redux';
import {withStyles} from '@material-ui/core/styles'
import { withRouter } from 'react-router-dom';

import { withRequestsCanceller } from '../../../HOC/RequestsCanceller/RequestsCanceller';
import * as Requests from '../../../Helpers/Requests/Requests';
import PostDisplay from '../PostDisplay/PostDisplay';
import {goBack} from '../../../routing/utils';
import Waiting from '../../../Components/UI/Waiting/Waiting';
import Error from '../../../Components/UI/Error/Error';
import Routes from '../../../routing/routes';

const styles = theme => ({
    PostContainer:{
        marginTop: theme.spacing.unit * 10,
        marginBottom: theme.spacing.unit * 20, 
    },
    SeeAllPostContainer:{
        display: 'flex',
        justifyContent: 'center',
        marginBottom: theme.spacing.unit * 5,
        marginTop: theme.spacing.unit * 5,
    },
    [theme.breakpoints.down('sm')]: {
        PostContainer:{
            marginTop: theme.spacing.unit * 10,
            marginBottom: theme.spacing.unit * 10, 
        },
    },
});

class Post extends Component{

    constructor(props){
        super(props);

        this.state = {
            me: null,
            post: null,
            loading: true,
            loadingError: null,
        }
    }

    goBack = () => {
        goBack(this.props);
    }

    goToProfile = () => {
        this.props.history.push(Routes.myprofile);
    }

    componentDidMount(){
        //Récupération du post
        Requests.wrappedRequest(this.props.signalCancelToken, Requests.getPost, 
            {
                postId: this.props.match.params.id,
        })
        .then(res => {
            this.setState({
                post: res,
                me: res.profileId === this.props.profileId,
                loading: false,
            })
        })
        .catch(err => {
            if(err !== Requests.cancelErrorCode){
                this.setState({
                    loading: false,
                    loadingError: err,
                });
            }
        })
    }

    render(){
        const {classes} = this.props;


        let display = null;
        if(this.state.loading){
            display = <Waiting />;
        }
        else if(this.state.loadingError){
            display = <Error text="Chargement de la piste impossible"/>;
        }
        else{
            display = (
                    <PostDisplay
                        post={this.state.post} 
                        me={this.state.me} 
                        displayedOnProfile={false} 
                        openComments={true}
                        onDelete={this.goToProfile}
                    />
            );
        }
        return (
            <React.Fragment>
                <div className={classes.PostContainer} ref={(el) => { this.HeadPostRef = el; }}>
                    {display}
                </div> 
            </React.Fragment>
            
        );
    }
};

const mapStateToProps = state => {
    return {
        userId: state.Auth.userId,
        token: state.Auth.token,
        profileId: state.UserData.profileId,
        grade: state.UserData.grade,
    };
};

export default withRouter(connect(mapStateToProps, null)(withStyles(styles)(withRequestsCanceller(Post))));
