import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from "react-router";
import { withStyles } from '@material-ui/core/styles';

import Paper from '@material-ui/core/Paper';
import { Typography, Toolbar } from '@material-ui/core';
import ToolTip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import FavoriteIcon from '@material-ui/icons/Favorite';
import PersonIcon from '@material-ui/icons/Person';
import grey from '@material-ui/core/colors/grey';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import MessageIcon from '@material-ui/icons/Message';

import { withRequestsCanceller } from '../../../HOC/RequestsCanceller/RequestsCanceller';
import Routes from '../../../routing/routes';
import PostFastDisplay from '../../../Components/Post/PostFastDisplay/PostFastDisplay';
import YesNoModal from '../../../Components/UI/Modals/YesNoModal/YesNoModal';
import InfoModal from '../../../Components/UI/Modals/InfoModal/InfoModal';
import * as Requests from '../../../Helpers/Requests/Requests';
import PostCommentSection from '../PostCommentSection/PostCommentSection';
import { Notification } from '../../../store/actions/actionsIndex';


const styles = theme => ({
    Post:{
        width: '80%',
        maxWidth: '1000px',
        margin: 'auto',
        color: grey[700],
    },
    PostHead: {
        textAlign: 'center',
    },
    ToolBar:{
        backgroundColor: theme.palette.primary.main,
        color: "white",
    },
    [theme.breakpoints.down('sm')]: {
        Post:{
            width: '95%',
        }
    },
});

class PostDisplay extends Component {

    constructor(props){
        super(props);

        this.state = {
            post: {...props.post},
            me: props.me,
            displayedOnProfile: props.displayedOnProfile,
            onDelete: false,
            deleteError: null,

            likeInfo: {},
            likeInfoError: null,
            openComments: props.openComments,
        }

        if(props.openComments){
            props.openPost(this.state.post.id);
        }
    }

    resetErrorState = () => {
        this.setState({
            ...this.state,
            deleteError: null,
        })
    }

    onEditHandler = () => {
        this.props.history.push(Routes.updatePost + this.state.post.id);
    }

    onDelete = () => {
        Requests.wrappedRequest(
            this.props.signalCancelToken,
            Requests.deletePost,
            {
                token: this.props.token,
                postId: this.state.post.id,
            }
        )
        .then(res => {
            this.setState({
                ...this.state,
                onDelete: false,
                likeInfo: {} //avoid request on unmounting deleted post
            })
            this.props.onDelete(this.state.post.id)
        })
        .catch(err => {
            if(err !== Requests.cancelErrorCode){
                this.setState({
                    ...this.state,
                    deleteError: err,
                })
            }
        })
        
    }

    onGoToProfile = () => {
        this.props.history.push(Routes.profile + this.state.post.profileId);
    }

    onToggleFavorite = () => {  //On veut enlever le like
        if(this.state.likeInfo.id){
            Requests.wrappedRequest(
                this.props.signalCancelToken,
                Requests.unlikePost,
                {
                    token: this.props.token,
                    postId: this.state.post.id,
                }
            )
            .then(res => {
                this.loadLastLikeInfo();
                this.props.removeLike(this.state.post.id);
            })
            .catch(this.onLikeInfoLoadingError);
        }
        else{   //On veut liker
            Requests.wrappedRequest(
                this.props.signalCancelToken,
                Requests.likePost,
                {
                    token: this.props.token,
                    postId: this.state.post.id,
                }
            )
            .then(res => {
                this.loadLastLikeInfo();
                this.props.addLike(this.state.post.id);
            })
            .catch(this.onLikeInfoLoadingError);
        }
    }

    onLikeInfoLoadingError = (err) => {
        if(err !== Requests.cancelErrorCode){
            this.setState({
                ...this.state,
                likeInfoError: err,
            })
        }
    }

    OnToggleDelete = () => {
        this.setState({
            ...this.state,
            onDelete: !this.state.onDelete,
        })
    }

    toggleMessageBox = () => {
        this.setState({
            ...this.state,
            openComments: !this.state.openComments,
        },() => {
            if(this.state.openComments === true){
                this.props.openPost(this.state.post.id);
            }
            else{
                if(this.state.likeInfo.id){
                    this.props.closePost(this.props.token, this.state.post.id);
                }
            }
        });
    }

    loadLastLikeInfo(){
        Requests.wrappedRequest(
            this.props.signalCancelToken,
            Requests.loadLikeInfo,
            {
                token: this.props.token,
                postId: this.state.post.id,
            }
        )
        .then(res => {
            this.setState({
                ...this.state,
                likeInfo: {...res},
            });
        })
        .catch(this.onLikeInfoLoadingError)
    }

    componentDidMount(){
        this.loadLastLikeInfo();
    }

    componentWillUnmount(){
        
        if(this.state.likeInfo.id && this.state.openComments === true){
            this.props.closePost(this.props.token, this.state.post.id);
        }
    }

    render(){
        const { classes, post } = this.props;

        const errorOnPostCommentModal = (
            this.state.errorOnPostCommentModal ?
            <InfoModal
                    state="error"
                    open={this.state.commentSendError}
                    onClose={() => {this.setState({...this.state, commentSendError: false,})}}
                    onConfirm={() => {this.setState({...this.state, commentSendError: false,})}}
                    title="L'envoi du commentaire a échoué !"
                    text={this.state.commentSendError}
                />
            :
            null
        );
        const goToProfile = (
            !this.props.displayedOnProfile ?
                <ToolTip title="Voir le profil complet">
                    <IconButton color="inherit" onClick={this.onGoToProfile}>
                        <PersonIcon />
                    </IconButton>
                </ToolTip>
            :
                null
        );
        const myPostToolbarOptions = (
            <React.Fragment>
                <ToolTip title="Éditer la piste">
                    <IconButton color="inherit" onClick={this.onEditHandler}>
                        <EditIcon />
                    </IconButton>
                </ToolTip>
                <ToolTip title="Supprimer la piste">
                    <IconButton color="inherit" onClick={this.OnToggleDelete}>
                        <DeleteIcon />
                    </IconButton>
                </ToolTip>
            </React.Fragment>
        );
        const otherPostToolbarOptions = (
            this.props.token  && !this.state.me ?
            <React.Fragment>
                        <ToolTip title={this.state.likeInfo.id ? "Supprimer de mes pistes" : "Ajouter à mes pistes."}>
                            <IconButton onClick={this.onToggleFavorite} color={this.state.likeInfo.id ? 'secondary' : 'inherit'}>
                                <FavoriteIcon />
                            </IconButton>
                        </ToolTip>
            </React.Fragment>  
            :
            null
        );
        const commentToolbarOption = (
            <React.Fragment>
                {goToProfile}
                {
                    <ToolTip title= {this.state.openComments ? "Fermer la section réactions" : "Ouvrir la section réactions"}>
                        <IconButton color="inherit" onClick={this.toggleMessageBox}>
                            { 
                                this.state.openComments ?
                                <KeyboardArrowDownIcon />
                                :
                                <MessageIcon/>
                            }
                        </IconButton>
                    </ToolTip>
                }
            </React.Fragment>
        );


        return(
            <React.Fragment>
                { errorOnPostCommentModal }
                <YesNoModal
                    state="error"
                    open={this.state.onDelete}
                    onClose={this.OnToggleDelete}
                    onConfirm={this.onDelete}
                    title="Suppression d'une piste"
                    text={["Confirmez-vous la suppression de la piste : ", this.state.post.title]}
                    textBtnClose="Annuler"
                    textBtnConfirm="Supprimer"
                />
                <InfoModal
                    state="error"
                    open={this.state.deleteError}
                    onClose={this.resetErrorState}
                    onConfirm={this.resetErrorState}
                    title="La suppression a échouée"
                    text={this.state.deleteError}
                />

                <Paper className={classes.Post}>
                    <PostFastDisplay 
                        title={post.title}
                        opportunities={post.opportunities}
                        description={post.description}
                    />
                    <div className={classes.PostHead}>
                        <Toolbar className={classes.ToolBar}>
                            {otherPostToolbarOptions}
                            {this.state.me ?
                                myPostToolbarOptions
                            :
                                null
                            }
                            <Typography style={{flexGrow: 1}}></Typography>
                            {
                                commentToolbarOption
                            }
                        </Toolbar>
                    </div>
                    <PostCommentSection 
                        open={this.state.openComments}
                        postId={this.state.post.id}
                    />
                </Paper>
            </React.Fragment>

            
        );
    }

}

const mapStateToProps = state => {
    return {
        token: state.Auth.token,
        appMode: state.AppMode.appMode,
    };
};

const mapDispatchToProps =  dispatch => {
    return {
        addLike: (postId) => dispatch(Notification.addLike(postId)),
        removeLike: (postId) => dispatch(Notification.removeLike(postId)),

        openPost: (postId) => dispatch(Notification.openPost(postId)),
        closePost: (token, postId) => dispatch(Notification.closePost(token, postId)),
    } 
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(withRequestsCanceller(PostDisplay))));