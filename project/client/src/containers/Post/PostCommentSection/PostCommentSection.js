import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Collapse from '@material-ui/core/Collapse';
import openSocket from 'socket.io-client';

import { withRequestsCanceller } from '../../../HOC/RequestsCanceller/RequestsCanceller'
import * as Requests from '../../../Helpers/Requests/Requests';
import Routes from '../../../routing/routes';
import MessageScroller from '../../../Components/Post/MessageScroller/MessageScroller';
import TypeMessage from '../../../Components/Post/TypeMessage/TypeMessage';
import ServerEndPoints from '../../../routing/serverEndpoints';


const commentsLoadedOnFirstLoad = 10;
const commentLoadedOnEach = 10;

//Props
/*
open
postId
likeInfo
*/

const openingTime = 500;

class PostCommentSection extends Component {

    constructor(props){
        super(props);

        this.commentBottomRef = React.createRef();
        this.goToBottomTimeout = null;
        this.state = {
            comments: [],
            openComments: props.open === true ? true : false,
            reachedCommentStart: false,
            reachedCommentEnd: false,
            commentError: false,
            commentText: '',
            commentSendError: null,
            loadedFirstComments: false,

            forceGoDown: false,
        }
    }

    onLoadingMoreCommentTop = (force = false) => {
        if((!this.state.reachedCommentStart || force) && this.state.loadedFirstComments){

            this.loadComments({
                token: this.props.token,
                postId: this.props.postId,
                pageSize: commentLoadedOnEach,
                id: this.state.comments[this.state.comments.length - 1].id,
                after: false,
            })
            .then(res => {
                this.setState({
                    ...this.state,
                    comments: this.state.comments.concat(res.comments),
                    reachedCommentStart: res.comments.length < commentLoadedOnEach ? true : false,
                });
            })
            .catch(err => {
                if(err !== Requests.cancelErrorCode){
                    this.setState({
                        ...this.state,
                        commentError: true,
                    });
                }
            })
        }
    }

    onLoadingMoreCommentBottom = (force = false) => {
        if((!this.state.reachedCommentStart || force) && this.state.loadedFirstComments){
            const lastId = this.state.comments.length > 0 ? this.state.comments[0].id : null;
            this.loadComments({
                token: this.props.token,
                postId: this.props.postId,
                pageSize: commentLoadedOnEach,
                id: lastId,
                after: true,
            })
            .then(res => {
                this.setState({
                    ...this.state,
                    comments: res.comments.concat(this.state.comments),
                    reachedCommentEnd: res.comments.length < commentLoadedOnEach ? true : false,
                });
            })
            .catch(err => {
                if(err !== Requests.cancelErrorCode){
                    this.setState({
                        ...this.state,
                        commentError: true,
                    });
                }
            })
        }
    }

    loadComments = (params) => {
        return Requests.wrappedRequest(
            this.props.signalCancelToken,
            Requests.getPostComments,
            params
        );
    }

    loadCommentsOnMount = () => {
        if(this.props.token){
            this.loadComments({
                token: this.props.token,
                postId: this.props.postId,
                pageSize: commentsLoadedOnFirstLoad
            })
            //Requests.getPostComments(this.props.token, this.props.postId, commentsLoadedOnFirstLoad)
            //Requests.getPostComments(this.props.token, this.props.postId, commentsLoadedOnFirstLoad, 2-1, true)
            .then(res => {
                this.setState({
                    ...this.state,
                    comments: [...res.comments],
                    reachedCommentStart: res.comments.length < commentsLoadedOnFirstLoad ? true : false,
                    reachedCommentEnd: true,
                    loadedFirstComments: true,
                    //reachedCommentStart: true,
                    //reachedCommentEnd: res.comments.length < commentsLoadedOnFirstLoad ? true : false,
                })
            })
            .catch(err => {
                if(err !== Requests.cancelErrorCode){
                    this.setState({
                        ...this.state,
                        commentError: true,
                    });
                }
            })
        }
    }

    onCommentChanged = (e) => {
        this.setState({
            ...this.state,
            commentText: e.target.value,
        });
    }

    onSendComment = () => {
        Requests.wrappedRequest(
            this.props.signalCancelToken,
            Requests.createCommentOnPost,
            {
                token: this.props.token,
                postId: this.props.postId,
                text: this.state.commentText,
            }
        )
        .then(res => {
            this.setState({
                ...this.state,
                commentText: '',
            });
        })
        .catch(err => {
            if(err !== Requests.cancelErrorCode){
                this.setState({
                    ...this.state,
                    commentSendError: true,
                });
            }
        });
    }

    onGoToCommentProfile = (profileId) => {
        this.props.history.push(Routes.profile + profileId);
    }

    goToBottomComment = (params) => {
        this.commentBottomRef.scrollIntoView(params);
    }

    mountSocket = () => {
        this.socket = openSocket(ServerEndPoints.serverDomain);
        this.socket.on('comments', data => {
            if(data.action === 'create'){
                if(data.comment.postId === this.props.postId){
                    const newComments = [...this.state.comments];
                    newComments.unshift(data.comment);
                    this.setState({
                        ...this.state,
                        comments: newComments,
                    })
                }
            }
        });

        this.socket.open();
    }

    unmountSocket = () => {
        this.socket.close();
    }

    componentDidMount(){
        this.loadCommentsOnMount();
        this.mountSocket();
    }

    static getDerivedStateFromProps(props, state){
        const newState = {...state};
        if(props.open !== state.openComments){
            newState.openComments = props.open;
            if(props.open === true){
                newState.forceGoDown = true;
            }
        }
        return newState;
    }

    componentDidUpdate(){
        if(this.state.forceGoDown === true){ //Si on vient d'ouvrir les commentaires -> on va en bas
            this.setState({
                ...this.state, 
                forceGoDown: false,
            }, () => {
                this.goToBottomTimeout = setTimeout(() => { this.goToBottomComment({behavior: 'smooth', block: 'nearest', inline: 'nearest'});}, openingTime); 
            })        
        }
    }

    componentWillUnmount(){
        if(this.goToBottomTimeout){
            clearTimeout(this.goToBottomTimeout);
        }
        this.unmountSocket();
    }

    render(){
        let comments = (
            <Collapse in={this.state.openComments} timeout={openingTime}>
                <MessageScroller
                    userId={+this.props.userId}
                    onUsernameClick={this.onGoToCommentProfile}
                    messages={this.state.comments} 
                    atStart={this.state.reachedCommentStart} 
                    atEnd={this.state.reachedCommentEnd} 
                    onLoadingMoreTop={this.onLoadingMoreCommentTop}
                    onLoadingMoreBottom={this.onLoadingMoreCommentBottom}
                    error={this.state.commentError}
                    locked={!this.props.token}
                    appMode={this.props.appMode}
                    ref={this.messageScroller}
                />
                {
                    this.props.token ?
                    <TypeMessage 
                        text={this.state.commentText} 
                        onChange={this.onCommentChanged}
                        onSend={this.onSendComment}
                    />
                    :
                    null
                }
            </Collapse>
        );

        return(
            <div>
                {comments}
                <div ref={(el) => { this.commentBottomRef = el; }}></div>
            </div>
        );
    }

};

const mapStateToProps = state => {
    return {
        token: state.Auth.token,
        userId: state.Auth.userId,
        appMode: state.AppMode.appMode,
    };
};

export default connect(mapStateToProps, null)(withRouter(withRequestsCanceller(PostCommentSection)));