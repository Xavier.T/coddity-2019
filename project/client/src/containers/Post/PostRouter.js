import React, { Component } from 'react'
import { Switch, Redirect, Route } from 'react-router-dom'
import { connect } from 'react-redux';

import Routes from '../../routing/routes';
import PrivateRoute from '../../Components/Auth/PrivateRoute/PrivateRoute';

import PostUpdatePage from './PostUpdatePage/PostUpdatePage';

import PostProfile from './Post/Post'

class PostRouter extends Component{
    
    render(){
        return (
                <Switch> 
                    
                    <PrivateRoute //Create new post
                        exact 
                        path = { this.props.match.path + '/create' }
                        component= { PostUpdatePage }
                        condition={this.props.isAmbassador && this.props.isAuthenticated}
                    />

                    <PrivateRoute //Edit an existing post
                        path = { this.props.match.path + '/edit/:id' }
                        component= {PostUpdatePage}
                        condition={this.props.isAmbassador && this.props.isAuthenticated}
                    />            

                    <Route //Reach a certain post on a profile
                        path = { this.props.match.path + '/:id' }
                        component= {PostProfile}
                    />  

                    <Redirect to = { Routes.pageNotFound } />

                </Switch>     
        )
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.Auth.token !== null,
        isAmbassador: state.UserData.profileId &&state.UserData.profileId !== -1,
    };
}


export default connect(mapStateToProps, null)(PostRouter);