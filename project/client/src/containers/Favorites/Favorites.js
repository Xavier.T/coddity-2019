import React, { Component } from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

import { withRequestsCanceller } from '../../HOC/RequestsCanceller/RequestsCanceller';
import Waiting from '../../Components/UI/Waiting/Waiting';
import PostFavDisplay from '../../Components/Post/PostFavDisplay/PostFavDisplay';
import * as Requests from '../../Helpers/Requests/Requests';
import Routes from '../../routing/routes';
import accentFold from '../../Helpers/accentFold';
import PageTitle from '../../Components/PageTitle/PageTitle';
import Button from '@material-ui/core/Button';
const favLoadAtOnce = 3;

const styles = ({
    
    Container:{
        width: '80%',
        maxWidth: '1000px',
        margin: 'auto',
    },

});

class Favorites extends Component{

    constructor(props){
        super(props);

        this.state = {
            hasReachedEnd: false,
            favorites: [],
            searchText: '',
        }
    }

    loadMoreFavs = (page) => {
        Requests.wrappedRequest(
            this.props.signalCancelToken,
            Requests.getFavs,
            {
                token: this.props.token,
                page: page,
                pageSize: favLoadAtOnce,
            }
        )
        .then(res => {
            const newFavs = [...this.state.favorites];
            this.setState({
                favorites: newFavs.concat(res.map(post => {return {...post, simpleTitle:this.getStringSimplified(post.title)}} )),
                hasReachedEnd: res.length < favLoadAtOnce,
            });
        })
        .catch(err => {
            if(err !== Requests.cancelErrorCode){
                console.log("ERREUR CHARGEMENT !");
            }
        })
    }

    onSearchTextChange = (e) => {
        this.setState({
            ...this.state,
            searchText: e.target.value,
        })
    }

    goToProfile = (profileId) => {
        this.props.history.push(Routes.profile + profileId);
    }

    goToPost = (postId) => {
        this.props.history.push(Routes.post + postId);
    }

    goToSearch = () => {
        this.props.history.push(Routes.explore);
    }

    getStringSimplified = (title) => {
        let a = accentFold(title);
        a = a.toLocaleLowerCase();
        return a;
    }

    render(){
        const {classes} = this.props;

        const searchBar = (
            <div className={classes.SearchBar}>
                <TextField
                    required
                    id="postName"
                    placeholder="Nom de la piste"
                    margin="normal"
                    variant="outlined"
                    fullWidth
                    value={this.state.searchText}
                    onChange={this.onSearchTextChange}
                />
            </div>
            
        );

        const simpleSearch = this.getStringSimplified(this.state.searchText);
        const favs = (
            <InfiniteScroll
                pageStart={-1}
                loadMore={this.loadMoreFavs}
                hasMore={!this.state.hasReachedEnd}
                loader={<div className="loader" key={0}><Waiting /></div>}
            >
                {
                    this.state.favorites.map((post) => {
                        if(post.simpleTitle.includes(simpleSearch)){
                            return (
                                <div key={post.id}>
                                    <PostFavDisplay
                                        key={post.id}
                                        title={post.title}
                                        onGoToProfile={() => {this.goToProfile(post.profileId)}}
                                        onGoToPost={() => {this.goToPost(post.id)}}
                                    />
                                </div>);
                        }
                        else{
                            return null;
                        }
                        
                    })
                }
            </InfiniteScroll>
        );

        const noPistForTheMoment = (
            this.state.hasReachedEnd && this.state.favorites.length === 0 ?
            <div style={{display: 'flex', flexDirection: 'column'}}>
                <PageTitle
                    title="Aucune piste suivie pour le moment"
                    subtitle="Je suis sûr que tu peux en trouver par ici..."
                />
                <Button variant="contained" color="primary" onClick={this.goToSearch} style={{margin: 'auto'}}>
                    Chercher
                </Button>
            </div>
            
            :
            null
        );

        return (
            <React.Fragment>
                <PageTitle 
                    title="L'enquête avance ?"
                    subtitle="C'est ici que se trouvent toutes les pistes que tu suis"
                />
                <div className={classes.Container}>
                    {searchBar}
                    {favs}
                    {noPistForTheMoment}
                </div>
            </React.Fragment>
        );
    };
}

const mapStateToProps = state => {
    return {
        token: state.Auth.token,
    };
  }

export default connect(mapStateToProps, null)(withStyles(styles)(withRequestsCanceller(Favorites)));