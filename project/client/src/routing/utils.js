import Routes from './routes';

export const goBack = (props) => {
    console.log(props);
    if(props.history.pop){
        props.history.pop();
    }
    else if(props.history.goBack){
        props.history.goBack();
    }
    else{
        props.history.replace(Routes.home);
    }
}

