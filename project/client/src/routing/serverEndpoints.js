import { serverDomain } from '../Helpers/consts/envVars';

export default {
    //serverDomain: 'http://localhost:8080/',
    //serverDomain: 'https://techit-coddity.herokuapp.com/',
    serverDomain: serverDomain,

    auth: {
        login: 'auth/login',
        register: 'auth/signup',
        confirmEmail: 'auth/validation'
    },

    user: {
        info: 'user/info',
        delete: 'user/'
    },

    profile : {
        create : 'profile/create',
        update : 'profile/',
        get : 'profile/',
        delete : 'profile/',
        myprofile : 'profile/me',

        posts : '/posts/', //To use after get
    },

    post : {
        create: 'post/create',
        update: 'post/',
        get: 'post/',
        delete: 'post/',

        getComments: '/comments',
        random: 'post/random',
    },

    opportunity : {
        getAll : 'opportunity/all',
    },

    comment : {
        create: 'comment/create',
    },

    like : {
        create: 'like/create',
        delete: 'like/remove',
        update: 'like/update',
        get: 'like/info',
        getAll: 'like',
        getNotif: 'like/notif',
    },

    discover: {
        timeinfo: 'discover/timeinfo',
        post: 'discover/post',
    },

    searchOpportunity: {
        getList: 'searchOpportunity',
    }
};