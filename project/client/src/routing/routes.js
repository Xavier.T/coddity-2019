const routes = {

    home: "/",

    login: "/auth/login",
    register: "/auth/register",
    confirmMail: "/auth/validation",
    logout: "/auth/logout",

    myparam: '/myparam',

    refresh: "/refresh", //needs next url to go
    pageNotFound: "/notfound",
    serverError: "/servererror",

    createProfile: '/profile/create',
    updateProfile: '/profile/edit/', //needs another :id parameter
    myprofile: '/profile/me',
    profile: '/profile/', //needs another :id parameter

    post: '/post/', //needs another :id parameter
    createPost: '/post/create',
    updatePost: '/post/edit/', //needs another :id parameter

    discover: '/discover',
    explore: '/explore',
    notifications: '/notifications',
    favorites: '/favorites',
}

export default routes;
