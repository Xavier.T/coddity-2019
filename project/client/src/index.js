import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';

//Routing
import { BrowserRouter, Route } from 'react-router-dom';
import ScrollToTop from './Components/UI/ScrollToTop/ScrollToTop';
//Redux
import { Provider } from 'react-redux';
import GlobaleStore from './store/globalStore'

const store = GlobaleStore();
ReactDOM.render(

    <Provider store={store}>
        <BrowserRouter >
            <ScrollToTop>
                <Route path="/" component={App}></Route>
            </ScrollToTop>
        </BrowserRouter>
    </Provider>  
    
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
