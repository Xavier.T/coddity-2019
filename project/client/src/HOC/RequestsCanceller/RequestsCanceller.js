import React, { Component } from 'react';
import Axios from 'axios';


export const withRequestsCanceller = (WrappedComponent) => {
        class RequestsCanceller extends Component {
            constructor(props) {
                super(props);
                this.signalCancel = Axios.CancelToken.source();
            }
        
            componentWillUnmount(){
                this.signalCancel.cancel('canceling requests');
            }
        
            render() {
        
                return (
                    <WrappedComponent signalCancelToken={this.signalCancel.token} {...this.props}/>
                )
            }
        }

        withRequestsCanceller.displayName = `WithRequestsCanceller(${getDisplayName(WrappedComponent)})`;
        return RequestsCanceller;
} ;


function getDisplayName(WrappedComponent) {
    return WrappedComponent.displayName || WrappedComponent.name || 'Component';
  }