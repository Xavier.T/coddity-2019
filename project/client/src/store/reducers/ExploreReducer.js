import * as actionTypes from '../actions/actionTypes';
import updateObject from '../../Helpers/updateObject';

const initialState = {
    selectedOpportunities: [],
}

const saveSearch = (state, action) => {
    return updateObject(state, {
        selectedOpportunities: action.info.selectedOpportunities, 
    });
}

const eraseSearch = (state) => {
    return updateObject(state, {
        selectedOpportunities: [],
    })
}

const reducer = (state = initialState, action) => {
    switch(action.type){
        case actionTypes.EXPLORE_SAVE: return saveSearch(state, action);
        case actionTypes.EXPLORE_RESET: return eraseSearch(state);
        default: return state;
    }
}

export default reducer;