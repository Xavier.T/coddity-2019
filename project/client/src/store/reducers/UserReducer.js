import * as actionTypes from '../actions/actionTypes';
import updateObject from '../../Helpers/updateObject';

const initialState = {
    username: null,
    email: null,
    profileId: null,
    gender: null,
    loading: false,
    error: null,
};

const createUser = (state) => {
    return updateObject(state, {loading: true, error: null});
}

const createUserSucceed = (state, action) => {
    const infos = action.info;
    return updateObject(state, {
        username: infos.username,
        email: infos.email,
        profileId: infos.profileId,
        gender: infos.sexe,
        loading: false,
    });
}

const createUserFailed = (state, action) => {
    return updateObject(state, {
        loading: false,
        error: action.error,
    });
}

const resetUser = (state) => {
    return {
        username: null,
        email: null,
        profileId: null,
        gender: null,
        loading: false,
        error: null,
    };
}

const reducer = (state = initialState, action) => {

    switch (action.type){

        case actionTypes.USERINFO_START: return createUser(state);
        case actionTypes.USERINFO_SUCCESS: return createUserSucceed(state, action);
        case actionTypes.USERINFO_FAILED: return createUserFailed(state, action);
        case actionTypes.USERINFO_RESET: return resetUser(state);

        default:
            return state;
    }
}

export default reducer;