import * as actionTypes from '../actions/actionTypes';

const saveToLocalStorage = (mode) => {
    localStorage.setItem('appMode', mode);
}

const initialState = {
    appMode: 'H',
};

const reducer = (state = initialState, action) => {
    switch (action.type){
        case actionTypes.APPMODE_MALE:
            saveToLocalStorage('H')
            return {appMode: 'H'};

        case actionTypes.APPMODE_FEMALE:
            saveToLocalStorage('F');
            return {appMode: 'F'};

        default:
            return state;
    }
}

export default reducer;