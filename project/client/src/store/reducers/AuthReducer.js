import * as actionTypes from '../actions/actionTypes';
import updateObject from '../../Helpers/updateObject';

const initialState = {
    userId: null,
    token: null,
    loading: false,
    error: null,
};

const authStart = (state) => {
    return updateObject(state, {loading: true, error: null});
}

const authSuccess = (state, action) => {
    return updateObject(state, {
        userId: action.userId,
        token: action.token,
        loading: false, 
        error: null
    });
}

const authFail = (state, action) => {
    return updateObject(state, {
        loading: false,
        error: action.error,
    });
}

const authStop = (state) => {
    return {
        userId: null,
        token: null,
        loading: false,
        error: null,
    };
}

const reducer = (state = initialState, action) => {

    switch (action.type){

        case actionTypes.AUTH_START: return authStart(state);
        case actionTypes.AUTH_SUCCESS: return authSuccess(state, action);
        case actionTypes.AUTH_FAIL: return authFail(state, action);
        case actionTypes.AUTH_STOP: return authStop(state);
        case actionTypes.AUTH_RESET: return authStop(state);

        default:
            return state;
    }
}

export default reducer;