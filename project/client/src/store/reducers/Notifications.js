import * as actionTypes from '../actions/actionTypes';
import updateObject from '../../Helpers/updateObject';

const initialState = {
    currentOpenPosts: [],
    notifs: [],
    favs: [],
};

const removeNotifOnOpenPost = (notifs, postId) => {
    const newNotifs = [...notifs];
    const index = notifs.findIndex(notif => {
        return notif.postId === postId;
    });
    newNotifs.splice(index, 1);
    return newNotifs; 
}

const addedOpenPost = (state, action) => {
    const infos = action.info;
    const newNotifs = removeNotifOnOpenPost(state.notifs, infos.postId);   //Remove notif related to the opened post
    
    if(state.currentOpenPosts.findIndex(id => id === infos.postId) === -1){
        const newCurrentPosts = [...state.currentOpenPosts];     //Add the open post to the list
        newCurrentPosts.push(infos.postId);
        return updateObject(state, {
            notifs: newNotifs,
            currentOpenPosts: newCurrentPosts,
        });
    }
    else{
        return state;
    }
}

const removedOpenPost = (state, action) => {
    const infos = action.info;
    const newOpenPosts = [...state.currentOpenPosts];
    const index = newOpenPosts.findIndex(id => id === infos.postId);
    if(index !== -1){
        newOpenPosts.splice(index, 1);
        return updateObject(state, {
            currentOpenPosts: newOpenPosts,
        });
    }
    else{
        return state;
    }
}

const setNotifs = (state, action) => {
    const infos = action.info;
    //Remove notifs where posts are open.
    const keptNotifs = [];
    infos.notifs.forEach(notif => {
        if(!state.currentOpenPosts.includes(notif.postId)){
            keptNotifs.push(notif);
        }
    })
    return updateObject(state, {
        notifs: keptNotifs,
        favs: infos.favs
    });
}

const addOrAlterNotif = (state, action) => {
    const info = action.info;
    if(state.currentOpenPosts.findIndex(id => id === info.notif.postId) === -1 && state.favs.findIndex(id => id === info.notif.postId) !== -1){ //Si le post n'est pas ouvert et qu'il est dans nos favoris on ajoute
        const updatedNotifs = [...state.notifs]
        const i = state.notifs.findIndex(notif => notif.postId === info.notif.postId);
        if(i !== -1){  //Si on doit mettre la notif à jour
            updatedNotifs[i] = {...updatedNotifs[i], ...info.notif};
        }
        else{   //Si on doit la créer
            updatedNotifs.push(info.notif);
        }
        return updateObject(state, {
            notifs: updatedNotifs,
        })
    }
    else{
        return state;
    }
}

const addFav = (state, action) => {
    const infos = action.info;
    const i = state.favs.findIndex(postId => postId === infos.postId);
    if(i === -1){ //Si on a pas encore ce favoris on l'ajoute
        const newFavs = [...state.favs];
        newFavs.push(infos.postId);
        return updateObject(state, {
            favs: newFavs
        })
    }
    else{
        return state;
    }
}

const removeFav = (state, action) => {
    const infos = action.info;
    const i = state.favs.findIndex((postId) => { return postId === infos.postId; });
    if(i !== -1){ //Si a ce favoris on l'enleve
        const newFavs = [...state.favs];
        newFavs.splice(i, 1);
        return updateObject(state, {
            favs: newFavs
        })
    }
    else{
        return state;
    }
}

const reducer = (state = initialState, action) => {

    switch (action.type){
        case actionTypes.NOTIF_ADD_POST: return addedOpenPost(state, action);
        case actionTypes.NOTIF_REMOVE_POST: return removedOpenPost(state, action);
        case actionTypes.NOTIF_SET: return setNotifs(state, action);
        case actionTypes.NOTIF_ADD: return addOrAlterNotif(state, action);
        case actionTypes.NOTIF_ADD_FAV: return addFav(state, action);
        case actionTypes.NOTIF_REMOVE_FAV: return removeFav(state, action);

        default:
            return state;
    }
}

export default reducer;