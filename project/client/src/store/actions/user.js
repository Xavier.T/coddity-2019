import * as actionTypes from './actionTypes';
import * as Requests from '../../Helpers/Requests/Requests';

export const createUser = () => {
    return{
        type: actionTypes.USERINFO_START,
    };
}

export const createUserSucceed = (info) => {
    return{
        type: actionTypes.USERINFO_SUCCESS,
        info: info,
    };
}

export const createUserFailed = (error) => {
    return {
        type: actionTypes.USERINFO_FAILED,
        error: error,
    };
}

export const resetUser = () => {
    return {
        type: actionTypes.USERINFO_RESET,
    };
}

export const getUserData = (token) => {

    return dispatch => {
        dispatch(createUser());
        Requests.getUserData(token, 
        res => {
            dispatch(createUserSucceed(res));
        }, 
        errs => {
            dispatch(createUserFailed(errs));
        })
    }
}