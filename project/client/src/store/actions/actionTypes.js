export const AUTH_START = 'AUTH_START';
export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const AUTH_FAIL = 'AUTH_FAIL';
export const AUTH_STOP = 'AUTH_STOP';
export const AUTH_RESET = 'AUTH_RESET';

export const APPMODE_MALE = 'APPMODE_MALE';
export const APPMODE_FEMALE = 'APPMODE_FEMALE';

export const USERINFO_START = 'USERINFO_START';
export const USERINFO_SUCCESS = 'USERINFO_SUCCESS';
export const USERINFO_FAILED = 'USERINFO_FAILED';
export const USERINFO_RESET = 'USERINFO_STOP';

export const EXPLORE_SAVE = 'EXPLORE_SAVE';
export const EXPLORE_RESET = 'EXPLORE_RESET';

export const NOTIF_ADD_POST = 'NOTIF_ADD_POST';
export const NOTIF_REMOVE_POST = 'NOTIF_REMOVE_POST';
export const NOTIF_SET = 'NOTIF_SET';
export const NOTIF_ADD = 'NOTIF_ADD';
export const NOTIF_ADD_FAV = 'NOTIF_ADD_FAV';
export const NOTIF_REMOVE_FAV = 'NOTIF_REMOVE_FAV';