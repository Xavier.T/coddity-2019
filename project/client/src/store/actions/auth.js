import * as actionTypes from './actionTypes';
import * as Requests from '../../Helpers/Requests/Requests';

export const authStart = () => {
    return {
        type: actionTypes.AUTH_START,
    };
}

export const authSuccess = (token, userId) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        token: token,
        userId: userId,
    };
}

export const authFail = (error) => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: error,
    }
}

export const authReset = () => {
    return {
        type: actionTypes.AUTH_RESET,
    }
}

export const authStop = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('expirationDate');
    localStorage.removeItem('userId');
    return {
        type: actionTypes.AUTH_STOP,
    }
}

export const authTimeout = (seconds) => {
    return (dispatch) => {
        setTimeout(() => {
            console.log("EXPIRATION !");
            dispatch(authStop());
        }, seconds * 1000);
    }
}

export const auth = (email, password) => {
    return (dispatch) => {
        
        dispatch(authStart()); //Started Authentication
        Requests.postAuth(
            email, 
            password,
            data => {
                dispatch(authSuccess(data.token, data.userId));
                dispatch(authTimeout(data.expiresIn));
                const expirationDate = new Date(new Date().getTime() + data.expiresIn * 1000);
                localStorage.setItem('token', data.token);
                localStorage.setItem('expirationDate', expirationDate);
                localStorage.setItem('userId', data.userId);
            },
            errs => {
                dispatch(authFail(errs));
            }
        );
    }
}

export const checkAuthState = () => {
    return dispatch => {
        dispatch(authStart());
        const token = localStorage.getItem('token');
        const expirationDate = localStorage.getItem('expirationDate');
        const userId = localStorage.getItem('userId');
        if(token && expirationDate && userId){
            const expDate = new Date(expirationDate);
            const today = new Date();
            if(expDate > today){  //Jeton valide
                dispatch(authSuccess(token, userId));
                dispatch(authTimeout( (expDate.getTime() - today.getTime())/1000 ));               
            }
            else{
                dispatch(authStop());
            }
        }
        else{
            dispatch(authStop());
        }
    }
}