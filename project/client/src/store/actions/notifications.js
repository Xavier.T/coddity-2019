import * as actionTypes from './actionTypes';
import openSocket from 'socket.io-client';
import * as Requests from '../../Helpers/Requests/Requests';
import ServerEndPoints from '../../routing/serverEndpoints';

//Socket set up
export const initSocket = () => {
    
    return dispatch => {

        let IOsocket = openSocket(ServerEndPoints.serverDomain);
        IOsocket.on('notifications', data => {
            if(data.action === 'new'){
                dispatch(addOrAlterNotif(data.notif));
            }
        });

    }
    
}

const onClosePost = (postId) => {
    return {
        type: actionTypes.NOTIF_REMOVE_POST,
        info: {
            postId: postId,
        }
    };
}

const onSetNotifications = (notifs, favs) => {
    return {
        type: actionTypes.NOTIF_SET,
        info: {
            notifs: notifs,
            favs: favs,
        }
    }
}

export const openPost = (postId) => {
    return {
        type: actionTypes.NOTIF_ADD_POST,
        info: {
            postId: postId,
        }
    };
}

export const askNotifs = (token) => {
    return dispatch => {
        //send notif request
        Requests.getNotifications(token)
        .then(res => {
            dispatch(onSetNotifications(res.notifs, res.favs));
        })
        .catch(err => {
            console.log(err);
        })
    };
}

const addOrAlterNotif = (notif) => {
    return {
        type: actionTypes.NOTIF_ADD,
        info:{
            notif: notif,
        }
    };
}

export const addLike = (postId) => {
    return {
        type: actionTypes.NOTIF_ADD_FAV,
        info: {
            postId: postId,
        },
    };
}

export const removeLike = (postId) => {
    return {
        type: actionTypes.NOTIF_REMOVE_FAV,
        info: {
            postId: postId,
        },
    };
}

export const closePost = (token, postId) => {   //Signal we've seen the latest content to the server
    return dispatch => {
        Requests.updateNotification(token, postId)
        .then(res => {
            dispatch(onClosePost(postId));
            dispatch(askNotifs(token));
        })
        .catch(err => {
            console.log(err);
        })
    }
}