import * as actionTypes from './actionTypes';

export const toggleAppMode = (appMode) => {
    switch(appMode){
        case 'H':
            return {
                type: actionTypes.APPMODE_FEMALE,
            };
        case 'F':
            return {
                type: actionTypes.APPMODE_MALE,
            };
        default:
            return {
                type: actionTypes.APPMODE_FEMALE,
            };
    }   
};

export const setMode = (appMode) => {
    switch(appMode){
        case 'H':
            return {
                type: actionTypes.APPMODE_MALE,
            };
        case 'F':
            return {
                type: actionTypes.APPMODE_FEMALE,
            };
        default:
            return {};
    }  
}

export const setMaleMode = () => {
    return {
        type: actionTypes.APPMODE_MALE,
    };
}

export const setFemaleMode = () => {
    return {
        type: actionTypes.APPMODE_FEMALE,
    };
}

export const setFromLocalStorage = () => {
    const mode = localStorage.getItem('appMode');
    if(mode){
        return setMode(mode);
    }
    else{
        return setMaleMode();
    }
}