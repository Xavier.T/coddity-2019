import { auth, authStop, checkAuthState, authReset } from './auth';
import { getUserData, resetUser } from './user';
import { toggleAppMode, setMaleMode, setFemaleMode, setMode, setFromLocalStorage } from './appMode';
import { saveSearch, eraseSearch } from './explore';
import { initSocket, openPost, closePost, askNotifs, addLike, removeLike } from './notifications';

export const Authentication = {
    authenticate: auth,
    authReset: authReset,
    logout: authStop,
    checkAuthState: checkAuthState,
};

export const UserData = {
    getUser: getUserData,
    resetUser: resetUser,
};

export const AppMode = {
    toggleAppMode: toggleAppMode,
    setMale: setMaleMode,
    setFemaleMode: setFemaleMode,
    setAppMode: setMode,
    setFromLocaStorage: setFromLocalStorage,
};

export const Explore = {
    saveSearch: saveSearch,
    resetSearch: eraseSearch,
};

export const Notification = {
    initSocket: initSocket,
    openPost: openPost,
    closePost: closePost,
    askNotifs: askNotifs,
    addLike: addLike,
    removeLike: removeLike,
}