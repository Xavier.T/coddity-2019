import * as actionTypes from './actionTypes';

export const saveSearch = (selectedOpportunities) => {
    return {
        type: actionTypes.EXPLORE_SAVE,
        info: {
            selectedOpportunities: [...selectedOpportunities],
        }
    };
}

export const eraseSearch = () => {
    return {
        type: actionTypes.EXPLORE_RESET,
    }
}