import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import AuthReducer from './reducers/AuthReducer';
import AppModeReducer from './reducers/AppModeReducer';
import UserReducer from './reducers/UserReducer';
import ExploreReducer from './reducers/ExploreReducer';
import NotificationsReducer from './reducers/Notifications';

import thunk from 'redux-thunk';



export default () => {
    const rootReducer = combineReducers({
        Auth: AuthReducer,
        AppMode: AppModeReducer,
        UserData: UserReducer,
        Explore: ExploreReducer,
        Notifications: NotificationsReducer,
    })
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    return createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));
};