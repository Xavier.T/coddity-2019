export default (object, newProps) => {
    return {
        ...object,
        ...newProps,
    };
};