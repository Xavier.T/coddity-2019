
export default {

    runValidation: (value, validations) => {
    
        let errs = [];
        let result = true;
        for(var val in validations){
            if(validations[val].check(value, validations[val].args) === false){
                if(!validations[val].err){
                    errs.push("failed at : " + validations[val].check.toString())
                }
                else{
                    errs.push(validations[val].err)
                }
                result = false;
            }
        }

        return {
            valid: result,
            errors: errs
        };
    },


    isEmail: (value) => {
        return /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(value);
    },

    isCorrectPassword: (value) => {
        //1 caractère minuscule
        //1 majuscule
        //1 caractère numérique
        //1 symbole
        //Une longueur minimale de 8
        return /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})/.test(value);
    },

    isAtLeast: (value, arg = {min: 0}) => {
        return value.length >= arg.min;
    },

    isAtMax: (value, arg = {max: 10000}) => {
        return value.length <= arg.max;
    }
};