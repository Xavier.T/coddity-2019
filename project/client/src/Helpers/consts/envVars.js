
export const serverDomain = process.env.NODE_ENV === 'development' ? 'http://localhost:8080/' : 'https://techit-coddity.herokuapp.com/';