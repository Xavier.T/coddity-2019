export const w = {
    A:['Un', 'Une'],
    Man: ['Homme', 'Femme'],
    Explorer:['Explorateur', 'Exploratrice'],
    Ambassador:['Ambassadeur', 'Ambassadrice'],
    Proud:['Fier', 'Fière'],
    Ready:['Prêt', 'Prête'],
    Visitor: ['Visiteur', 'Visiteuse'],
    Surrounded: ['Entouré', 'Entourée'],
}

export const word = (w, mode, lower=false) => {
    const m = mode === 'H' ? 0 : 1;
    if(lower){
        return w[m].toLowerCase();
    }
    else{
        return w[m];
    }
}