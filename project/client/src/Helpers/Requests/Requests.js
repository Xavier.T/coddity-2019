import axios from 'axios';
import ServerEndPoints from '../../routing/serverEndpoints';

export const cancelErrorCode = -1;

//Axios instance with server base url
const axiosInstance = axios.create({
    baseURL: ServerEndPoints.serverDomain,
    timeout: 15000,
  });

//Response Handling
const getErrorsFromResponse = (err, custom) => {
    if(err.response){
        return custom ? custom : err.response.data.errors;
    }
    else{
        return ["La plateforme ne peut traiter la demande pour l'instant !"];
    }
};

const getDataFromReponse = (res) => {
    if(res.data){
        return res.data;
    }
    else{
        return {};
    }
};

//Cancelable requests
export const wrappedRequest = async (signalCancelToken, request, params) => {
    try{
        const res = await request(signalCancelToken, params);
        return(getDataFromReponse(res))
    }    
    catch(err){
        if(axios.isCancel(err)){
            //console.log("Canceled promise !");
            throw cancelErrorCode;
        }
        else{
            //console.log(err);
            throw getErrorsFromResponse(err);
        }
    }
}

//Requests

//Auth ------------------------------------------------------------------------------


 export const postAuth = (email, password, success, error) => {     //REDUX REQUESTS, no need to be cancelled
    axiosInstance.post(ServerEndPoints.auth.login, {
        email: email, 
        password: password
    })
    .then(res => {
        success(getDataFromReponse(res));
    })
    .catch(err => {
        error(getErrorsFromResponse(err, "Les identifiants ne correspondent pas !"));
    });
 };

 export const postRegister = async (cancelToken, params) => {
    return axiosInstance.put(ServerEndPoints.auth.register, {
        email: params.email,
        username: params.username,
        gender: params.gender,
        password: params.password,
        password_confirm: params.password_confirm
    }, {
        cancelToken: cancelToken,
    });
 }

//UserData ----------------------------------------------------------------------------

export const getUserData = (token, success, error) => {     //REDUX REQUESTS, no need to be cancelled
    
    axiosInstance.get(ServerEndPoints.user.info, {
        headers: {'Authorization': "bearer " + token}
    })
    .then(res => {
        success(getDataFromReponse(res));
    })
    .catch(err => {
        error(getErrorsFromResponse(err));
    });
};

export const deleteUserData = async (cancelToken, params) => {
    return axiosInstance.delete(ServerEndPoints.user.delete, {
        headers: {'Authorization': "bearer " + params.token},
        cancelToken: cancelToken,
    });
}


//Profiles ---------------------------------------------------------------------------
export const createProfile = async (cancelToken, args) => {
    return axiosInstance.post(ServerEndPoints.profile.create, {
        firstName: args.firstName,
        lastName: args.lastName,
        sentence: args.sentence,
        description: args.description,
    }, {
        cancelToken: cancelToken,
        headers: {'Authorization': "bearer " + args.token}
    });
}

export const getProfile = async (cancelToken, params) => {
    return axiosInstance.get(ServerEndPoints.profile.get + params.profileId, {
        cancelToken: cancelToken,
    });
}

export const updateProfile = async (cancelToken, params) => {
    return axiosInstance.put(ServerEndPoints.profile.update + params.profileId, {
        firstName: params.firstName,
        lastName: params.lastName,
        sentence: params.sentence,
        description: params.description,
    }, {
        headers: {'Authorization': "bearer " + params.token},
        cancelToken: cancelToken,
    });
}

export const deleteProfile = async (cancelToken, params) => {
    return axiosInstance.delete(ServerEndPoints.profile.delete + params.profileId, {
        headers: {'Authorization': "bearer " + params.token},
        cancelToken: cancelToken,
    });
}

export const getAllPosts = (cancelToken, params) => {
    const endPoint = ServerEndPoints.profile.get + params.profileId + ServerEndPoints.profile.posts + '?page=' + params.page + '&pagesize=' + params.pageSize;
    return axiosInstance.get(endPoint, {
        cancelToken: cancelToken,
    });
}


//Posts
export const getPost = async (cancelToken, params) => {
    return axiosInstance.get(ServerEndPoints.post.get + params.postId, {cancelToken: cancelToken});
}

export const createPost = async (cancelToken, params) => {
    return axiosInstance.post(ServerEndPoints.post.create, {
        title: params.title,
        description: params.description,
        opportunities: params.opportunities.map(opp => opp.id),
    }, {
        headers: {'Authorization': "bearer " + params.token},
        cancelToken: cancelToken,
    });
}

export const updatePost = async (cancelToken, params) => {
    return axiosInstance.put(ServerEndPoints.post.update + params.postId, {
        title: params.title,
        description: params.description,
        opportunities: params.opportunities.map(opp => opp.id),
    }, {
        headers: {'Authorization': "bearer " + params.token},
        cancelToken: cancelToken,
    });
}


export const deletePost = (cancelToken, params) => {
    return axiosInstance.delete(ServerEndPoints.post.delete + params.postId, {
        headers: {'Authorization': "bearer " + params.token},
        cancelToken: cancelToken,
    })
}

export const createCommentOnPost = (cancelToken, params) => {
    return axiosInstance.post(ServerEndPoints.comment.create ,{
        postId: params.postId,
        text: params.text,
    }, {
        headers: {'Authorization': "bearer " + params.token},
        cancelToken: cancelToken,
    })
}

export const getPostComments = async (cancelToken, params) => {
    let base_url = ServerEndPoints.post.get + params.postId + ServerEndPoints.post.getComments + '?';
        
    if(params.pageSize){
        base_url += ('pagesize=' + params.pageSize + '&');
    }
    if(params.id){
        base_url += ('searchId=' + params.id + '&');
        base_url += ('after='+ params.after.toString() + '');
    }
    return axiosInstance.get(base_url, {
        headers: {'Authorization': "bearer " + params.token},
        cancelToken: cancelToken,
    });
}

export const loadRandomPost = async (cancelToken) => {
    return axiosInstance.get(ServerEndPoints.post.random, {cancelToken: cancelToken});
}

//Like Info

export const likePost = async(cancelToken, params) => {
    return axiosInstance.post(ServerEndPoints.like.create, {
        postId: params.postId,
    }, {
        headers: {'Authorization': "bearer " + params.token},
        cancelToken: cancelToken,
    })
}

export const unlikePost = async(cancelToken, params) => {
    return axiosInstance.post(ServerEndPoints.like.delete, {
        postId: params.postId,
    }, {
        headers: {'Authorization': "bearer " + params.token},
        cancelToken: cancelToken,
    })
}

export const loadLikeInfo = async (cancelToken, params) => {
    return axiosInstance.post(ServerEndPoints.like.get, {
        postId: params.postId,
    }, {
        headers: {'Authorization': "bearer " + params.token},
        cancelToken: cancelToken,
    })
}

//Opportunity

export const getAllOpportunities = async (cancelToken, params) => {
    return axiosInstance.get(ServerEndPoints.opportunity.getAll, {
        headers: {'Authorization': "bearer " + params.token},
        cancelToken: cancelToken,
    });
}

//Discover

export const getDiscoverTimeInfo = async (cancelToken) => {
    return axiosInstance.get(ServerEndPoints.discover.timeinfo, {
        cancelToken: cancelToken,
    })
};

export const getDiscoverPostInfo = async (cancelToken) => {
    return axiosInstance.get(ServerEndPoints.discover.post, {
        cancelToken: cancelToken,
    })
};

export const getTimeInfo = () => {
    return new Promise((resolve, reject) => {
        axiosInstance.get(ServerEndPoints.discover.timeinfo)
        .then(res => {
            resolve(getDataFromReponse(res));
        })
        .catch(err => {
            reject(getErrorsFromResponse(err));
        });
    })
}

export const getDiscoverPost = () => {
    return new Promise((resolve, reject) => {
        axiosInstance.get(ServerEndPoints.discover.post)
        .then(res => {
            resolve(getDataFromReponse(res));
        })
        .catch(err => {
            reject(getErrorsFromResponse(err));
        });
    })
}

//Search By Opportunity
export const getPostByOpportunities = async(cancelToken, params) => {
    return axiosInstance.post(ServerEndPoints.searchOpportunity.getList, {
        opportunities: params.opportunities,
    }, {
        headers: {'Authorization': "bearer " + params.token},
        cancelToken: cancelToken,
    })
}

//Favs

export const getFavs = async(cancelToken, params) => {
    let url = ServerEndPoints.like.getAll;
    if(params.page !== null && params.pageSize !== null){
        url += ('?page=' + params.page + '&pagesize=' + params.pageSize);
    }
    //console.log(url);
    return axiosInstance.get(url, {
        headers: {'Authorization': "bearer " + params.token},
        cancelToken: cancelToken,
    });
}

//Notifs

export const getNotifications = (token) => {
    return new Promise((resolve, reject) => {
        axiosInstance.get(ServerEndPoints.like.getNotif, {
            headers: {'Authorization': "bearer " + token},
        })
        .then(res => {
            resolve(getDataFromReponse(res));
        })
        .catch(err => {
            reject(getErrorsFromResponse(err));
        });
    });
}

export const updateNotification = (token, postId) => {
    return new Promise((resolve, reject) => {
        axiosInstance.post(ServerEndPoints.like.update, {
            postId: postId,
        },{
            headers: {'Authorization': "bearer " + token},
        })
        .then(res => {
            resolve(getDataFromReponse(res));
        })
        .catch(err => {
            reject(getErrorsFromResponse(err));
        });
    })
}