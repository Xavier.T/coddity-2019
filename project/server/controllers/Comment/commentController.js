const ErrorHandling = require('../../util/errorHandling/errorHandling')
const { validationResult } = require('express-validator/check')
const Sequelize = require('../../util/database');
const Post = require('../../model/post');
const User = require('../../model/user');
const Comment = require('../../model/comment');
const Profile = require('../../model/profile');
const Notif = require('../../util/notification');
const io = require('../../socket');

exports.createComment = (req, res, next) => {
    //Vérification de la validation des champs
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        throw ErrorHandling.getValidationError(errors.array());
    }

    const userId = req.userId;
    const postId = req.body.postId;
    let foundPost = null;
    let foundUser = null;
    let newComment = null;

    Post.findOne({ 
        where: { 
            id: postId,
        },
    })
    .then(post => {
        if(post){
            
            foundPost = post
            return User.findOne({
                where: {
                    id: userId,
                },
                include: [{
                    model: Profile,
                },]
            })
        }
        else{
            throw ErrorHandling.getSimpleError(404, "Le post à commenter n'existe pas !");
        }
    })
    .then(user => {
        if(user){
            foundUser = user;
            newComment = Comment.build({
                text: req.body.text,
            });
            newComment.setUser(user, {save: false});
            newComment.setPost(foundPost, {save: false});
            return newComment.save();
        }
        else{
            throw ErrorHandling.getSimpleError(404, "Impossible de trouver l'utilisateur qui veut commenter le post !");
        }
    })
    .then((save) => {
        io.getIO().emit('comments', {
            action: 'create',
            comment: {
                ...save.dataValues,
                profileId: foundUser.dataValues.profile ? foundUser.dataValues.profile.dataValues.id : null,
                username: foundUser.dataValues.username,
            },
        });
        io.getIO().emit('notifications', {
            action: 'new',
            notif: Notif(foundPost.id, foundPost.title, false, true),
        });
        res.send({});
    })
    .catch(err => {
        next(err);
    })
};