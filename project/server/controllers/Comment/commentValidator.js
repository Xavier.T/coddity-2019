const { check } = require('express-validator/check');

exports.createValidation = [

    check('text')
    .trim()
    .isLength({min: 1, max: 600})
    .withMessage("Le texte du commentaire doit être compris entre 1 et 600 caractères"),
];
