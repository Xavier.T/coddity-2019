const ErrorHandling = require('../../util/errorHandling/errorHandling')

const Sequelize = require('../../util/database');
const Opportunity = require('../../model/opportunity');

exports.getAll = (req, res, next) => {
    Opportunity.findAll({
        order: [['name', 'ASC']],
    })
    .then(opps => {
        res.send({opportunities: opps});
    })
    .catch(err => {
        next(err);
    })
};