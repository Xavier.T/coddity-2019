const deleteExpiredAccounts = require("./deleteExpiredAccounts");
const discoverController = require('../../controllers/Discover/discoverController');

const deleteExpiredAccountsInterval = 60 * 1000; //Every minutes

module.exports = () => {
    deleteExpiredAccounts(deleteExpiredAccountsInterval);
    discoverController.start();
}