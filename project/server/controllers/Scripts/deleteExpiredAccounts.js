const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const User = require('../../model/user');

module.exports = (interval = 60 * 1000) => {
    setInterval(() => {
        const currentTime = new Date(Date.now());
        User.destroy({
            where: {
                validationTokenExpiration:{
                    [Op.lt]: currentTime
                },
            },
            logging: false,
        })
        .then((nRows) => {
            if(nRows > 0){
                console.log("Deleted "+ nRows + " expired accounts");
            }
            
        })
        .catch(err => {
            console.log("Error during deleting expired accounts at "+ currentTime);
        })

    }, interval);
}