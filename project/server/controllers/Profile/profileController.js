const ErrorHandling = require('../../util/errorHandling/errorHandling')
const { validationResult } = require('express-validator/check')

const Sequelize = require('../../util/database');
//const Op = Sequelize.Op
const Profile = require('../../model/profile');
const User = require('../../model/user');
const Opportunity = require('../../model/opportunity');
const Post = require('../../model/post');

exports.createProfile = (req, res, next) => {

    const errors = validationResult(req);
    if(!errors.isEmpty()){
        throw ErrorHandling.getValidationError(errors.array());
    }

    User.findOne({
        where:{
            id: req.userId,
        },
    })
    .then(user => {
        if(user){
            const prof = Profile.build({
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                sentence: req.body.sentence,
                description: req.body.description,
            });
            return user.setProfile(prof);
        }
        else{
            throw ErrorHandling.getSimpleError(404, "L'utilisateur créateur du profil n'existe pas !");
        }
    })
    .then(() => {
        res.status(200).json({});
    })
    .catch(err => {
        next(err);
    });
};

exports.getMyProfile = (req, res, next) => {
    Profile.findOne({
        where:{
            userId: req.userId,
        },
        include: [{
            model: User,
            attributes: ['id', 'username'],
        }],
        raw: true,
    })
    .then(profile => {
        if(profile){
            res.send({
                userId: profile['user.id'],
                profileId: profile.id,
                username: profile['user.username'],
                firstName: profile.firstName,
                lastName: profile.lastName,
                sentence: profile.sentence,
                description: profile.description, 
            })
        }
        else{
            throw ErrorHandling.getSimpleError(404, "Le profil demandé n'existe pas .");
        }
    })
    .catch(err => {
        next(err);
    })
}

exports.getProfile = (req, res, next) => {
    
    Profile.findOne({
        where:{
            id: req.params.id,
        },
        raw:true,
        include: [{
            model: User,
            attributes: ['id', 'username'],
        }]
    })
    .then(profile => {
        if(profile){
            res.send({
                userId: profile['user.id'],
                username: profile['user.username'],
                profileId: profile.id,
                firstName: profile.firstName,
                lastName: profile.lastName,
                sentence: profile.sentence,
                description: profile.description,
                
            })
        }
        else{
            throw ErrorHandling.getSimpleError(404, "Le profil demandé n'existe pas .");
        }
    })
    .catch(err => {
        next(err);
    })
};

exports.updateProfile = (req, res, next) => {

    const userId = req.userId;
    const profileId = req.params.id;

    Profile.findOne({
        where: {
            id: profileId,
        }
    })
    .then(profile => {
        if(profile){
            if(profile.userId === userId){
                return profile.update({
                    firstName: req.body.firstName,
                    lastName: req.body.lastName,
                    sentence: req.body.sentence,
                    description: req.body.description,
                });
            }
            else{
                throw ErrorHandling.getSimpleError(401, "Vous n'avez pas l'autorisation de modifier ce profil");
            }
        }
        else{
            throw ErrorHandling.getSimpleError(404, "Le profil a mettre à jour n'existe pas.");
        }
    })
    .then(update => {
        res.send({});
    })
    .catch(err => {
        next(err);
    })
};

exports.deleteProfile = (req, res, next) => { 
    Profile.findOne({
        where: {
            id: req.params.id,
        }
    })
    .then(profile => {
        if(profile){
            if(profile.userId === req.userId){
                return profile.destroy();
            }
            else{
                throw ErrorHandling.getSimpleError(401, "Vous n'êtes pas autorisé à supprimer ce profil !");
            }
        }
        else{
            throw ErrorHandling.getSimpleError(404, "Le profil à supprimer n'existe pas.");
        }
    })
    .then(() => {
            res.json({});
    })
    .catch(err => {
        next(err);
    })
};

exports.getAllPosts = (req, res, next) => {

    const profileId = req.params.id;
    let { page, pagesize } = req.query;
    let offset = 0;
    if(page && pagesize){
        pagesize = +pagesize
        offset = +page * pagesize;
    }

    Profile.findOne({
        where: {
            id: profileId,
        },
    })
    .then(profile => {
        if(profile){
            return profile.getPosts(
                {
                    raw: false,
                    include : [{
                        model: Opportunity,
                    }],
                    order: [
                        ['createdAt', 'DESC'],
                        [Opportunity,'name', 'ASC'],
                    ],
                    offset: offset,
                    limit: pagesize,
                });
        }
        else{
            throw ErrorHandling.getSimpleError(404, "Le profil n'existe pas !");
        }
    })
    .then(posts => {
        res.send({posts: posts});
    })
    .catch(err => {
        next(err);
    })
}