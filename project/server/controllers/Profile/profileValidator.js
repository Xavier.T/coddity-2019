const { check } = require('express-validator/check');

exports.createValidation = [

    check('firstName')
    .trim()
    .isLength({min: 1, max: 20}).withMessage("Les prénoms doivent être composés de 1 à 20 caractères."),

    check('lastName')
    .trim()
    .isLength({min: 1, max: 20}).withMessage("Les noms de famille doivent être composés de 1 à 20 caractères."),

    check('sentence')
    .trim()
    .isLength({max: 100}).withMessage("Les phrases personnelles ne doivent pas excéder 255 caractères."),

    check('description')
    .isLength({max: 1000}).withMessage("Le description du profil ne peut excéder 1000 caractères.")
];
