const { check } = require('express-validator/check');

exports.createValidation = [

    check('title')
    .trim()
    .isLength({min: 1, max: 50}).withMessage("L'intitulé de l'expérience professionnelle est requis et ne doit pas excéder 50 caractères"),

    check('opportunities')
    .custom((opportunities) => {
        if(Array.isArray(opportunities) && opportunities.length >= 1 && opportunities.length <= 5){
            for(let i = 0; i < opportunities.length; i++){
                if(opportunities.indexOf(opportunities[i]) !== i){ 
                    return false;
                }
            }
            return true;
        }
        return false;
    }).withMessage("Un post doit contenir entre 1 et 5 opportunités différentes !"),

    check('description')
    .isLength({max: 2500}).withMessage("Le description du post ne peut excéder 2500 caractères.")
];

exports.searchByOpportunity = [

    check('opportunities')
    .custom((opportunities) => {
        if(Array.isArray(opportunities) && opportunities.length >= 1 && opportunities.length <= 5){
            for(let i = 0; i < opportunities.length; i++){
                if(opportunities.indexOf(opportunities[i]) !== i){ 
                    return false;
                }
            }
            return true;
        }
        return false;
    }).withMessage("Une recherche doit contenir entre 1 et 5 opportunités différentes !"),
];
