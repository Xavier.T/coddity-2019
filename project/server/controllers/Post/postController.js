const ErrorHandling = require('../../util/errorHandling/errorHandling')
const { validationResult } = require('express-validator/check')

const Sequelize = require('../../util/database');
const sequelize = require('sequelize');
const Op = sequelize.Op;
const Post = require('../../model/post');
const Profile = require('../../model/profile');
const Opportunity = require('../../model/opportunity');
const User = require('../../model/user');
//const Comment = require('../../model/comment');
const Like = require('../../model/like');
const io = require('../../socket');
const Notif = require('../../util/notification');

exports.createPost = (req, res, next) => {

    const errors = validationResult(req);
    if(!errors.isEmpty()){
        throw ErrorHandling.getValidationError(errors.array());
    }

    const userId = req.userId;
    let createdPost = null;
    let authorProfile = null;
    let AskedOpportunities = null;

    Sequelize.transaction(t => { //Use rollback system if error somewhere

        return Profile.findOne({
            where: {
                userId: userId,
            },
            transaction: t
        })

        .then((profile) => { 
            if(profile){
                authorProfile = profile;
                return Opportunity.findAll({
                    where: {
                        id: {
                            [Op.in]: req.body.opportunities,
                        },
                    },
                    transaction: t,
                });
            }
            else{
                throw ErrorHandling.getSimpleError(404, "Impossible de créer un post sans être ambassadeur !");
            }      
        })

        .then((opportunities) => {  
            if(opportunities.length !== req.body.opportunities.length){
                throw ErrorHandling.getSimpleError(404, "Une opportunité associée au post n'a pas été trouvée !");
            }
            AskedOpportunities = opportunities;
            return authorProfile.createPost({
                title: req.body.title,
                description: req.body.description,
            }, {transaction: t});
        })

        .then((post) => {
            createdPost = post;
            return post.setOpportunities(AskedOpportunities, {transaction: t});
        })

        .then(() => {
            return authorProfile.getUser({ 
                attributes: ['id'],
                transaction: t
            });
        })

        .then(user => {
            if(user){
                const newLike = Like.build({
                    lastpostversionseen: createdPost.dataValues.version,
                    lastmessageseen: 0,
                });
                newLike.setUser(user, {save: false,});
                newLike.setPost(createdPost, {save: false});
                return newLike.save({transaction: t});
            }
            else{
                throw ErrorHandling.getSimpleError(404, "L'utilisateur créateur du post n'existe pas !");
            }
        })
    })
    .then(() => {
        return res.send({});
    })
    .catch(err => {
        next(err);
    })
};

exports.updatePost = (req, res, next) => {

    const errors = validationResult(req);
    if(!errors.isEmpty()){
        throw ErrorHandling.getValidationError(errors.array());
    }

    const userId = req.userId;
    const postId = req.params.id;
    let foundPost = null;
    let AskedOpportunities = null;
    let foundLike = null;

    Sequelize.transaction(t => {
        return Post.findOne({
            where:{
                id: postId,
            },
            include: {
                attributes: ['userId'],
                model: Profile,
                raw: true,  
            },
            transaction: t,
        })
        .then(post => {
            if(post){
                //Vérification de l'autorisation
                if(post.dataValues.profile.dataValues.userId === userId){
                    //Update du post
                    foundPost = post;
                    return post.update({
                        title: req.body.title,
                        description: req.body.description,
                        version: post.dataValues.version+1
                    }, {transaction : t});
                }
                else{
                    throw ErrorHandling.getSimpleError(401, "Vous n'avez pas la permission de mettre à jour ce post !");
                }
            }
            else{
                throw ErrorHandling.getSimpleError(404, "Le post à mettre à jour n'existe pas !");
            }
        })
        .then(() => {
            //Find opportunities
            return Opportunity.findAll({
                where: {
                    id: {
                        [Op.in]: req.body.opportunities,
                    },
                },
                transaction: t,
            })
        })
        .then(opportunities => {
            //Check opportunity validity
            if(opportunities.length !== req.body.opportunities.length){
                throw ErrorHandling.getSimpleError(404, "Une opportunité associée au post n'a pas été trouvée !");
            }
            AskedOpportunities = opportunities;

            //destroy existing opportunities
            return foundPost.removeOpportunities(opportunities, {transaction: t});
        })
        .then(() => {
            //Set new opportunities
            return foundPost.setOpportunities(AskedOpportunities, {transaction: t});
        })

        .then(() => {
            return foundPost.getProfile({
                where:{
                    id: foundPost.profileId,
                }
            });
        })

        .then(profile => {
            return Like.findOne({
                where: {
                    postId: foundPost.id,
                    userId: profile.userId,
                }
            });
        })

        .then(like => {
            if(like){
                //Récupération du dernier message du post
                foundLike = like;
                return foundPost.getComments({
                    attributes: [
                        [sequelize.fn('MAX', sequelize.col('id')),  'MAX'],
                    ],
                    raw: true,
                })
            }
            else{
                throw ErrorHandling.getSimpleError(409, 'Impossible de mettre à jour les informations de like');
            }
        })

        .then((maxComment) => {
            const lastmsgseen = maxComment[0].MAX ? maxComment[0].MAX : 0;
            return foundLike.update({
                lastpostversionseen: foundPost.dataValues.version,
                lastmessageseen: lastmsgseen,
            })
        })
    })
    .then(result => { //Fin de la transaction
        io.getIO().emit('notifications', {
            action: 'new',
            notif: Notif(foundPost.id, foundPost.title, true, false),
        });
        res.send({});
    })
    .catch(err => {
        next(err);
    })
};

exports.getPost = (req, res, next) => {

    const errors = validationResult(req);
    if(!errors.isEmpty()){
        throw ErrorHandling.getValidationError(errors.array());
    }

    const postId = req.params.id;
    let foundPost = null;

    Post.findOne({
        where: {
            id: postId,
        },
    })
    .then(post => {
        if(post){
            foundPost = post;
            return post.getOpportunities({ 
                raw: true, 
                order: [['name', 'ASC'],],
            });
        }
        else{
            throw ErrorHandling.getSimpleError(404, "Le post demandé n'a pas pu être trouvé.")
        }
    })
    .then(opportunities => {
        const PostInfo = foundPost.dataValues;
        const FinalPost = {
            id: PostInfo.id,
            title: PostInfo.title,
            description: PostInfo.description,
            version: PostInfo.version,
            createdAt: PostInfo.createdAt,
            profileId: PostInfo.profileId,
            opportunities: opportunities.map(opp => {
                return {
                    id: opp.id,
                    name: opp.name,
                    description: opp.description,
                }
            })
        };
        res.send(FinalPost);
    })
    .catch(err => {
        next(err);
    })
};

exports.deletePost = (req, res, next) => {

    const errors = validationResult(req);
    if(!errors.isEmpty()){
        throw ErrorHandling.getValidationError(errors.array());
    }

    const userId = req.userId;
    const postId = req.params.id;
    
    Post.findOne({
        where:{
            id: postId,
        },
        include: {
            attributes: ['userId'],
            model: Profile,
            raw: true,  
        },
    })
    .then(post => {
        if(post){
            if(post.dataValues.profile.dataValues.userId === userId){
                return post.destroy();
            }
            else{
                throw ErrorHandling.getSimpleError(401, "Vous n'avez pas la permission de supprimer ce post !");
            }
        }
        else{
            throw ErrorHandling.getSimpleError(404, "Le post à supprimer n'existe pas !");
        }
    })
    .then(() => {
        res.send({});
    })
    .catch(err => {
        next(err);
    })
    
};

exports.getComments = (req, res, next) => {
    
    const postId = req.params.id;
    const nComments = req.query.pagesize ? +req.query.pagesize : 5;
    const id = req.query.searchId ? req.query.searchId : null;
    const after = req.query.after === 'true' ? true : false;
    //Si on a pas de demande particulière, on charge les derniers
    let config = {
        limit: nComments,
        raw: true,
        order: [['id', 'DESC']],
        include: [{
            model: User,
            attributes: ['username'],
            include: [{
                model: Profile,
                attributes: ['id'],
            }]
        }],
    }
    if(id){
        const where = {id:{}};
        if(after === true){
            where.id[Op.gt] = id;
            config.order = [];
        }
        else{
            where.id[Op.lt] = id;
        }
        config.where = where;
    }
    Post.findOne({
        where:{
            id: postId,
        }
    })
    .then(post => {
        if(post){
            return post.getComments(config);
        }
        else{
            throw ErrorHandling.getSimpleError(404, "Le post n'existe pas !");
        }
    })
    .then(comments => {
        const newComments = comments.map((comment) => {
            return {
                id: comment.id,
                text: comment.text,
                createdAt: comment.createdAt,
                profileId: comment['user.profile.id'],
                username: comment['user.username'],
                postId: comment.postId,
            }
        });
        if(after){
            newComments.sort((a, b) => {
                return a.id < b.id;
            });
        }
        
        res.send({
            comments: newComments,
        });
    })
    .catch(err => {
        next(err);
    })
     
}

exports.getRandom = (req, res, next) => {

    let foundPost = null;

    Post.findOne({ 
        order: [Sequelize.fn( 'RAND' )],
    })
    .then((post) => {
        if(post){
            foundPost = post;
            return post.getOpportunities({ 
                raw: true, 
                order: [['name', 'ASC'],],
            });
        }
        else{
            throw ErrorHandling.getSimpleError(404, "Le post n'existe pas !");
        }
    })
    .then(opportunities => {
        const PostInfo = foundPost.dataValues;
        const FinalPost = {
            id: PostInfo.id,
            title: PostInfo.title,
            description: PostInfo.description,
            version: PostInfo.version,
            createdAt: PostInfo.createdAt,
            profileId: PostInfo.profileId,
            opportunities: opportunities.map(opp => {
                return {
                    id: opp.id,
                    name: opp.name,
                    description: opp.description,
                }
            })
        };
        res.send(FinalPost);
    })
    .catch(err => {
        next(err);
    })
}

exports.getByOpportunity = (req, res, next) => {
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        throw ErrorHandling.getValidationError(errors.array());
    }

    const userId = req.userId;
    const oppList = req.body.opportunities;
    const order = req.body.order;

    Post.findAll({
        include: [{
            model: Opportunity,
            where: {
                id: {
                    [Op.in]: oppList,
                },
            },
        },{
            model: Profile,
            attributes: ['userId'],
            where:{
                userId: {
                    [Op.not]: userId,
                }
            },
        }],
        
        limit: 1000,
        raw: true,
    })
    .then(posts => {
        //Count of number of opportunities matching per post
        
        const postMap = new Map();
        let currentCount = null;
        posts.forEach(post => {
            currentCount = postMap.get(post.id);
            if(currentCount){
                postMap.set(post.id, currentCount+1);
            }
            else{
                postMap.set(post.id, 1);
            }
        });

        if(posts.length > 1){
            //Creating the final tab ordered by matching opportunities and then number of opportunities and then by id
            let finalTab = posts.map((post) => {
                return post.id;
            });
            finalTab = [...new Set(finalTab)];
            finalTab.sort((id1, id2) => {
                const diff = postMap.get(id2) - postMap.get(id1);
                if(diff === 0){
                    return id2 - id1;
                }
                else{
                    return diff;
                }
            });

            res.send({
                matchingPosts: finalTab,
            });
        }
        else{
            if(posts.length === 1){
                res.send({
                    matchingPosts: [posts[0].id],
                });
            }
            else{
                res.send({
                    matchingPosts: [],
                })
            }
        }
        

    })
    .catch(err => {
        next(err);
    })
}