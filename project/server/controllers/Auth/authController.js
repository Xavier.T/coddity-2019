const { validationResult } = require('express-validator/check')
const bcrypt = require('bcryptjs')
const crypto = require('crypto')
const jwt = require('jsonwebtoken')
const secret = require('./jwtinfo')

const ErrorHandling = require('../../util/errorHandling/errorHandling')
const Sequelize = require('sequelize')
const Op = Sequelize.Op
const mailing = require('../../util/mail/mailing')
const User = require('../../model/user');
const AppRoutes = require('../../routes/reactApp');



exports.signup = (req, res, next) => {

    //Vérification de la validation des champs
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        throw ErrorHandling.getValidationError(errors.array());
    }

    //Add user to database with the validation_token and 
    const username = req.body.username;
    const email = req.body.email
    const gender = req.body.gender
    let password = req.body.password
    let validationToken = null
    const delay = 15; //15 minutes pour valider l'inscription
    let validationTokenExpiration = new Date(Date.now() + delay * 60000); 

    bcrypt.hash(password, 12)//hash du mot de passe
    .then(hash => {
        password = hash
    })
    .then(() => {
        return crypto.randomBytes(48);
    })
    .then((buffer) => {
        validationToken = buffer.toString('hex');
        return User.create({
            email : email,
            username: username,
            password: password,
            sexe: gender,
            validationToken: validationToken,
            validationTokenExpiration: validationTokenExpiration
        });  
    })
    .then(() => {
        return mailing.transporter.sendMail(mailing.mails.signup_validation(email, username, AppRoutes.domain + AppRoutes.confirmMail +"?validationToken="+validationToken+"&username="+username))
        //return "ok";
    })
    .then(() => {
        res.status(200).json({delay: delay})
    })
    .catch((err) => {
        next(err);
    })
    
}

exports.validate_signup = (req, res, next) => {

    if(req.body.validationToken && req.body.username){
        const token = req.body.validationToken
        const username = req.body.username
        const time = new Date(Date.now())
        
        User.findOne({
            where : {
                username: username,
                validationToken: token,
                validationTokenExpiration : { [Op.gt] : time }
            },
            raw: true,
        })
        .then(user => {
            if(user){ //Si on a bien un utilisateur en attente de confirmation
                return User.update({
                    validationToken: null,
                    validationTokenExpiration: null
                }, {where : {id: user.id}})
            }
            else{
                throw ErrorHandling.getSimpleError(422, "Impossible de procéder à la validation !"); 
            }
        })
        .then(() => {
            res.status(200).json({});
        })
        .catch(err => {
            next(err)
        })
    }
    else{
        throw ErrorHandling.getSimpleError(422, "Impossible de procéder à la validation !");
    }
}

exports.login = (req, res, next) => {

    //Vérification de la validation des champs
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        throw ErrorHandling.getValidationError(errors.array());
    }

    //Vérification correspondance e-mail et password
    const email = req.body.email
    const password = req.body.password
    let userId = null

    User.findOne({
        attributes: ['id', 'username', 'email', 'password', 'validationToken'], 
        where: {
            email : email
        },
        raw: true
    })
    .then((user) => {
        if(user){
            if(!user.validationToken){
                userId = user.id
                username = user.username
                return bcrypt.compare(password, user.password)
            }
        }
        throw ErrorHandling.getSimpleError(401, "Les identifiants de connexion sont invalides.");
    })
    .then(comparaison => {
        if(comparaison){
            //create JWT token
            const nhours = 3;
            const token = jwt.sign(
                { email: email, userId: userId}, 
                secret.JWTsecret,
                { expiresIn: nhours+'h' },
            );
            return res.status(200).json({ token: token, userId: userId, expiresIn: 3600*nhours});
        }
        throw ErrorHandling.getSimpleError(401, "Les identifiants de connexion sont invalides.");
    })
    .catch((err) => {
        next(err);
    })

}

/*
exports.resetpswask = (req, res, next) => {
    
    const userId = req.userId
    let email
    let resetToken

    User.findOne({
        where: {
            id: userId
        }, 
        raw: true
    })
    .then(user => {
        if(user){
            email = user.email
            return crypto.randomBytes(48)
        }

        const E = new Error()
        ErrorHandling.fillError(E, 409, "Unknown logged user !")
        throw E
    })
    .then((buffer) => {
        resetToken = buffer.toString('hex');
        return User.update({
            resetPasswordToken: resetToken,
            resetPasswordTokenExpiration: new Date(Date.now() + 30 * 60000) //30 minutes pour changer de mot de passe
        }, 
        { 
            where: {
                id: userId
        }})
    })
    .then(() => {
        return mailing.transporter.sendMail(mailing.mails.reset_password(email, Routing.app + Routing.auth.self + Routing.auth.resetpsw.self +"?resetToken="+resetToken+"&id="+userId))
    })
    .catch(err => {
        ErrorHandling(E, 500, "Couldn't send reset password mail !")
    })

}

exports.resetpsw = (req, res, next) => {
    
}
*/