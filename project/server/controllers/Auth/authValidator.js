const { check } = require('express-validator/check')
const User = require('../../model/user');

exports.signup_validation = [
    check('username')
        .trim()
        .custom((username) => {
            
            return User.findOne({ where: { username: username }})
            .then(user => {
                if(user){
                    return Promise.reject()
                }
            })
        }).withMessage("Le nom d'utilisateur déjà utilisé.")
        .isLength({ min: 5, max: 20 }).withMessage("Le nom d'utilisateur est trop court (3 caractères minimum)."),
    
    check('email')
        .trim()
        .custom((email) => {
            return User.findOne({ where: { email: email }})
            .then(email => {
                if(email){
                    return Promise.reject()
                }
            })
        }).withMessage("L'email est déjà utilisé.")
        .isEmail().withMessage("Le format de l'email est incorrect."),
    
    check('gender')
        .isLength({min:1, max:1}).withMessage("Le genre de l'utilisateur est invalide.")
        .custom((gender) => {
            return (gender === 'H' || gender === 'F' || gender === 'O');
        }),

    check('password')
        .trim()
        .matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})/).withMessage("Le format du mot de passe n'est pas conforme."),

    check('password_confirm')
        .trim()
        .custom((value, {req}) => {
            if(value !== req.body.password){
                console.log(value, req.body.password)
                return false;
            }
            return true;
        }).withMessage("Les mots de passe ne correspondent pas.")

]

exports.login_validation = [
    check('email')
        .trim()
        .isEmail().withMessage("Le format de l'email incorrect !"),
    check('password')
        .trim()
        .isLength({ min: 8 }).withMessage("Le mot de passe est trop court !")
]