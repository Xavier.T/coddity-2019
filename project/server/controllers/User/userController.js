const ErrorHandling = require('../../util/errorHandling/errorHandling');
const Sequelize = require('sequelize');
//const Op = Sequelize.Op
const User = require('../../model/user');
const Profile = require('../../model/profile');


exports.getUserData = (req, res, next) => {

    User.findOne({
        where: {
            id: req.userId,
        },
        include: [Profile],
        raw: true,
    })
    .then(user => {
        if(user){
            res.send({
                username: user.username,
                email: user.email,
                sexe: user.sexe,
                profileId: user['profile.id'] ? user['profile.id'] : -1,
            })
        }
        else{
            throw ErrorHandling.getSimpleError(404, "L'utilisateur demandé est inconnu !");
        }
    })
    .catch(err => {
        next(err);
    })
}

exports.deleteUser = (req, res, next) => {
    
    User.destroy({
        where: {
            id: req.userId,
        }
    })
    .then(nRowDeleted => {
        if(nRowDeleted === 1){
            res.json({});
        }
        else{
            throw ErrorHandling.getSimpleError(404, "L'utilisateur à supprimer n'existe pas.");
        }
    })
    .catch(err => {
        next(err);
    });
    
}