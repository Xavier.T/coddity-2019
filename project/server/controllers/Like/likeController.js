const ErrorHandling = require('../../util/errorHandling/errorHandling');
const { validationResult } = require('express-validator/check')
const sequelize = require('sequelize');
const Op = sequelize.Op;
const Like = require('../../model/like');
const Post = require('../../model/post');
const User = require('../../model/user');
const Profile = require('../../model/profile');
const Comment = require('../../model/comment');

exports.create = (req, res, next) => {
    const userId = req.userId;
    const postId = req.body.postId;
    let foundPost = null;
    let foundUser = null;

    Post.findOne({
        attributes: ['id', 'version'],
        where: {
            id: postId,
        }
    })
    .then(post => {
        if(post){
            foundPost = post;
            return User.findOne({
                attributes: ['id'],
                where:{
                    id: userId,
                }
            });
        }
        else{
            throw ErrorHandling.getSimpleError(404, 'Le post à liker n\'existe pas !');
        }
    })
    .then(user => {
        if(user){
            foundUser = user;
            return Like.findOne({
                where:{
                    userId: user.dataValues.id,
                    postId: foundPost.dataValues.id,
                }
            });   
        }
        else{
            throw ErrorHandling.getSimpleError(404, 'L\'utilisateur likant le post n\'existe pas !');
        }
    })
    .then(like => {
        if(!like){
            //Récupération du dernier message du post
            return foundPost.getComments({
                attributes: [
                    [sequelize.fn('MAX', sequelize.col('id')),  'MAX'],
                ],
                raw: true,
            })
        }
        else{
            throw ErrorHandling.getSimpleError(409, 'Un like a déjà été donné !');
        }
    })
    .then((maxComment) => {
        const lastmsgseen = maxComment[0].MAX ? maxComment[0].MAX : 0;
        console.log(foundPost.dataValues);
        const newLike = Like.build({
            lastpostversionseen: foundPost.dataValues.version,
            lastmessageseen: lastmsgseen
        });
        newLike.setUser(foundUser, {save: false,});
        newLike.setPost(foundPost, {save: false});
        return newLike.save();
    })
    .then(() => {
        res.send({});
    })
    .catch(err => {
        next(err);
    })
}

exports.update = (req, res, next) => {
    const userId = req.userId;
    const postId = req.body.postId;

    let foundPost = null;
    let foundUser = null;
    let foundLike = null;

    Post.findOne({
        attributes: ['id', 'version'],
        where: {
            id: postId,
        }
    })
    .then(post => {
        if(post){
            foundPost = post;
            return User.findOne({
                attributes: ['id'],
                where:{
                    id: userId,
                }
            });
        }
        else{
            throw ErrorHandling.getSimpleError(404, 'Le post à liker n\'existe pas !');
        }
    })
    .then(user => {
        if(user){
            foundUser = user;
            return Like.findOne({
                where:{
                    userId: user.dataValues.id,
                    postId: foundPost.dataValues.id,
                }
            });   
        }
        else{
            throw ErrorHandling.getSimpleError(404, 'L\'utilisateur likant le post n\'existe pas !');
        }
    })
    .then(like => {
        if(like){
            //Récupération du dernier message du post
            foundLike = like;
            return foundPost.getComments({
                attributes: [
                    [sequelize.fn('MAX', sequelize.col('id')),  'MAX'],
                ],
                raw: true,
            })
        }
        else{
            throw ErrorHandling.getSimpleError(409, 'Impossible de mettre à jour les informations de like');
        }
    })
    .then((maxComment) => {
        const lastmsgseen = maxComment[0].MAX ? maxComment[0].MAX : 0;
        return foundLike.update({
            lastpostversionseen: foundPost.dataValues.version,
            lastmessageseen: lastmsgseen,
        })
    })
    .then(() => {
        res.send({});
    })
    .catch(err => {
        next(err);
    })
}

exports.remove = (req, res, next) => {
    const userId = req.userId;
    const postId = req.body.postId;

    Post.findOne({
        where:{
            id: postId,
        },
    })
    .then(post => {
        if(post){
            return post.getProfile({raw: true,});
        }
        else{
            throw ErrorHandling.getSimpleError(404, 'Le post à liker n\'existe pas !');
        }
    })
    .then(profile => {
        if(profile.userId === userId){
            throw ErrorHandling.getSimpleError('401', "Impossible d'enlever un de ses posts des favoris !");
        }
        else{
            return Like.destroy({
                where:{
                    postId: postId,
                    userId: userId,
                }
            });
        }
        
    })
    .then(nbRow => {
        if(nbRow !== 0){
            res.send({});
        }
        else{
            throw ErrorHandling.getSimpleError(404, 'Ce post n\'a jamais été liké !');
        }
    })
    .catch(err => {
        next(err);
    })
}

exports.get = (req, res, next) => {
    const userId = req.userId;
    const postId = req.body.postId;
    Like.findOne({
        where: {
            userId: userId,
            postId: postId,
        },
        raw: true,
    })
    .then(like => {
        if(like){
            res.send(like);
        }
        else{
            res.send({})
        }
    })
}

exports.getAll = (req, res, next) => {
    const userId = req.userId;
    let { page, pagesize } = req.query;
    let offset = 0;
    if(page && pagesize){
        pagesize = +pagesize
        offset = +page * pagesize;
    }


    User.findOne({
        where: {
            id: userId,
        },
    })
    .then(user => {
        if(user){
            return user.getLikes(
                {
                    include: [
                        {
                            model: Post,
                            attributes: ['id', 'profileId', 'title'],
                            include: [{
                                model: Profile,
                                attributes: ['userId'],
                                where: {
                                    userId : {
                                        [sequelize.Op.not]: userId,
                                    }
                                }
                            }],
                            required: true,
                        }
                    ],
                    order: [
                        ['createdAt', 'DESC'],
                    ],
                    where: {

                    },
                    offset: offset,
                    limit: pagesize,
                    raw: true,
                });
        }
        else{
            throw ErrorHandling.getSimpleError(404, "L'utilisateur n'existe pas !");
        }
    })
    .then(posts => {
        res.send(posts.map(post => {
            return {
                id: post['post.id'],
                title: post['post.title'],
                profileId: post['post.profileId']
            }
        }));
    })
    .catch(err => {
        next(err);
    })
}

exports.getNotifications = (req, res, next) => {
    const userId = req.userId;
    Like.findAll({
        attributes: ['id', 'lastpostversionseen', 'lastmessageseen'],
        where:{
            userId: userId,
        },
        include: [
            {
                model: Post,
                attributes: [
                    'id',
                    'version',
                    'profileId',
                    'title',
                    [sequelize.fn('IFNULL', sequelize.fn('MAX', sequelize.col('post->comments.id')), 0), 'maxcomid'],
                ],
                include: [{
                    model: Comment,
                    attributes: [],
                }],
            }
        ],
        group: [sequelize.col('id')],
        raw: true,
    })
    .then(likes => {
        const notifs = [];
        const favs = [];
        likes.forEach(like => {
            const notif = {
                id: like.id,
                postId: like['post.id'],
                postTitle: like['post.title'],
                profileId: like['profileId'],
                update: false,
                comments: false,
            };
            let add = false;
            if(like.lastpostversionseen < like['post.version']){
                add = true;
                notif.update = true;
            }
            if(like.lastmessageseen < like['post.maxcomid']){
                add = true;
                notif.comments = true;
            }
            if(add === true){
                notifs.push(notif);
            }
            favs.push(like['post.id']);
        });
        res.send({
            notifs: notifs,
            favs: favs,
        })
    })
    .catch(err => {
        next(err);
    })
}