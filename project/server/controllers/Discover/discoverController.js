const ErrorHandling = require('../../util/errorHandling/errorHandling');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const Post = require('../../model/post');
//const Opportunity = require('../../model/opportunity');

let d = new Date(Date.now()); //Date will be setup at the first post choice in start function
const i = 60 * 1000; //5 seconds, stored in milliseconds
let currentPost = null; //no current post


exports.start = () => {
    setInterval(setPostInfo, i);
    setPostInfo()
    .then(() => {
        d = new Date(Date.now());
    })
}

exports.getTimeInfo = (req, res, next) => {
    res.send({
        startDate: d.getTime(),
        interval: i,
    });
}

exports.getCurrentPost = (req, res, next) => {
    if(currentPost){
        return res.send(currentPost);
    }
    else{
        return next(ErrorHandling.getSimpleError('404', 'Pas de post à découvrir pour l\'instant !'));
    }
}

function setPostInfo(){
    return new Promise((resolve, reject) => {
        const oldId = currentPost ? currentPost.id : -1;
        let foundPost = null;
        Post.findOne({ 
            order: [[ Sequelize.fn('RAND') ]],
            where: {
                id: {
                    [Op.not]: oldId,
                }
            },
            logging: false,
        }).then(post => {
            if(post){
                foundPost = post;
                return post.getOpportunities({ 
                    raw: true, 
                    order: [['name', 'ASC']],
                    logging: false,
                });
            }
            else{
                throw ErrorHandling.getSimpleError(404, 'Il n\'y a pas encore de posts à découvrir !');
            }
        })
        .then(opportunities => {
            const PostInfo = foundPost.dataValues;
            const FinalPost = {
                id: PostInfo.id,
                title: PostInfo.title,
                description: PostInfo.description,
                version: PostInfo.version,
                createdAt: PostInfo.createdAt,
                profileId: PostInfo.profileId,
                opportunities: opportunities.map(opp => {
                    return {
                        id: opp.id,
                        name: opp.name,
                        description: opp.description,
                    }
                })
            };
            currentPost = FinalPost;
            resolve();
        })
        .catch(err => {
            //console.log("Impossible d'actualiser le post de Discover");
            //console.log(err);
        });
    })
    
}