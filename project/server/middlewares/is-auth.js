const jwt = require('jsonwebtoken')
const secret = require('../controllers/Auth/jwtinfo')
const ErrorHandling = require('../util/errorHandling/errorHandling')

module.exports = (req, res, next) => {

    const authHeader = req.get('Authorization');
    if(!authHeader){
        throw ErrorHandling.getAuthenticationError();
    }

    const token = req.get('Authorization').split(' ')[1];

    let decodedToken;
    try{
        decodedToken = jwt.verify(token, secret.JWTsecret);
    }
    catch (err) {
        throw ErrorHandling.getSimpleError(500, "Un problème durant l'authentification est survenu !");
    }

    if(!decodedToken){
        throw ErrorHandling.getAuthenticationError();
    }

    req.userId = decodedToken.userId;
    return next();
}