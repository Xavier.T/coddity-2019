exports.service = process.env.MAIL_SERVICE;
exports.email = process.env.MAIL_ADDRESS;
exports.password = process.env.MAIL_PASSWORD;