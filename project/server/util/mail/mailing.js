const mailconfig = require('./config')
const nodemailer = require('nodemailer')

exports.transporter = nodemailer.createTransport({
    service : mailconfig.service,
    auth: {
        user: mailconfig.email,
        pass: mailconfig.password
    }
})

//Different possible mails

exports.mails = {

    signup_validation: (email, username, validation_url) => {
        return mailOptions = {
            from: mailconfig.email,
            to: email,
            subject: username + ', Entrez dans la tech !',
            html: `<h1>Merci de votre inscription sur Tech' it !</h1>
                   <p>Il ne vous reste plus qu'une étape avant de pouvoir naviguer sur le site. </p>
                   <p>Confirmez votre inscription par ici : <a href="`+validation_url+`">`+validation_url+`</a> </p>
                   `
                   
          }
    },

    reset_password: (email, resetUrl) => {
        return mailOptions = {
            from: mailconfig.email,
            to: email,
            subject: 'Tech\'it : Changement de votre mot de passe',
            html: `<p>Afin de changer votre mot de passe, merci de suivre le lien suivant : </p>
                   <p><a href="`+resetUrl+`">`+resetUrl+`</a> </p>
                   `
          }
    }
}