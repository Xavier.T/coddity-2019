
const Sequelize = require('sequelize')

dbname=process.env.DB_NAME;
user=process.env.DB_USER;
password=process.env.DB_PASSWORD;
host=process.env.DB_HOST;

const sequelize = new Sequelize(dbname, user, password, {
    host: host,
    dialect: 'mysql',
    query: {raw: false},
    define: {
        charset: 'utf8mb4',
    }
});



sequelize.authenticate()
.then(() => {
    console.log("Connexion à la base de donnée !");
})
.catch(() => {
    console.log("Impossible de se connecter à la base de donnée !")
});


module.exports = sequelize;