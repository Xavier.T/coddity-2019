exports.fillError = (err, statusCode, message) => {
    if(!err.statusCode){
        err.statusCode = statusCode
    }
    if(!err.message){
        err.messages = message
    }
};

exports.getValidationError = (validationErrors) => {
    const err = new Error();
    err.statusCode = 422;
    err.messages = validationErrors.map((err) => err.msg);
    return err;
};

exports.getSimpleError = (statusCode, message) => {
    const err = new Error();
    err.statusCode = statusCode;
    err.messages = [message];
    return err;
};

exports.getAuthenticationError = () => {
    const err = new Error();
    err.statusCode = 401;
    err.messages = ["Route interdite aux requêtes non identifiables"];
    return err;
};