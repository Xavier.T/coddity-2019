module.exports = (postId, postTitle, update = false, comments = false) => {
    if(update === true){
        return {
            postId: postId,
            postTitle: postTitle,
            update: update,
        }
    }
    else if(comments === true){
        return {
            postId: postId,
            postTitle: postTitle,
            comments: comments,
        }
    }
    
}