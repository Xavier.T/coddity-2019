const rootDir = require('../util/path')

module.exports = {
    app: 'http://localhost:8080',

    auth: {
        self: '/auth',
        signup : {
            self:'/signup'
        },
        validation : {
            self:'/validation'
        },
        login : {
            self: '/login'
        },
        resetpsw : {
            self: '/resetpsw'
        }
    },

    user: {
        self: '/user',

        info: {
            self: '/info',
        },

        delete: {
            self: '',
        }
    },

    profile: {
        self: '/profile',

        create: {
            self: '/create',
        },
        update: {
            self: '/:id',
        },
        get: {
            self: '/:id',
        },
        delete: {
            self: '/:id',
        },

        myprofile: {
            self: '/me',
        },

        posts: {
            self: '/:id/posts',
        }
    },

    post:{
        self: '/post',

        create: {
            self: '/create',
        },
        update: {
            self: '/:id',
        },
        get: {
            self: '/:id',
        },
        delete: {
            self: '/:id',
        },

        comments: {
            self: '/:id/comments',
        },
        random: {
            self: '/random',
        }
    },

    opportunity:{
        self: '/opportunity',

        getAll: {
            self: '/all',
        }
    },

    comment:{
        self: '/comment',

        create: {
            self: '/create',
        }
    },

    like: {
        self: '/like',

        create: {
            self: '/create',
        },
        update: {
            self: '/update',
        },
        remove: {
            self: '/remove',
        },
        get: {
            self: '/info',
        },
        getAll: {
            self: '',
        },
        getNotifications: {
            self: '/notif'
        },
    },

    discover:{
        self: '/discover',

        timeInfo: {
            self: '/timeinfo',
        },
        discoverPost: {
            self: '/post',
        }
    },

    searchOpportunity:{
        self: '/searchOpportunity',

        get: {
            self: '',
        }
    }
}