const express = require('express');
let opportunity_router = express.Router();
const Routing = require('./routingMap');

const opportunityController = require('../controllers/Opportunity/opportunityController');
const isAuth = require('../middlewares/is-auth');

opportunity_router.get(Routing.opportunity.getAll.self, isAuth, opportunityController.getAll);

//delete a post
module.exports = opportunity_router;