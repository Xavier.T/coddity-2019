const express = require('express')
let auth_router = express.Router()
const Routing = require('./routingMap')

const authController = require('../controllers/Auth/authController')
const authValidator = require('../controllers/Auth/authValidator')
const isAuth = require('../middlewares/is-auth')

//Validate created account
auth_router.post(Routing.auth.validation.self, authController.validate_signup)

//Register a new member validator and routing
auth_router.put(Routing.auth.signup.self, authValidator.signup_validation, authController.signup)

//login route, handle JWT
auth_router.post(Routing.auth.login.self, authValidator.login_validation, authController.login)

//Password reset ask
//auth_router.get(Routing.auth.resetpsw.self, isAuth, authController.resetpswask)

//Password reset
//auth_router.post(Routing.auth.resetpsw.self, authController.resetpsw)

module.exports = auth_router;