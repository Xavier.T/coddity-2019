const routes = {

    domain: process.env.REACT_APP_BASEURL,

    home: "/",

    register: "/auth/register",
    confirmMail: "/auth/validation",
    login: "/auth/login",
}

module.exports = routes;
