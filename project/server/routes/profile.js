const express = require('express')
let profile_router = express.Router()
const Routing = require('./routingMap')

const isAuth = require('../middlewares/is-auth')
const profileValidator = require('../controllers/Profile/profileValidator');
const profileController = require("../controllers/Profile/profileController");

//Route pour créer un profil
profile_router.post(Routing.profile.create.self, isAuth, profileValidator.createValidation, profileController.createProfile);

//Route pour récupérer son profil
profile_router.get(Routing.profile.myprofile.self, isAuth, profileController.getMyProfile);

//Route pour update un profil son profil
profile_router.put(Routing.profile.update.self, isAuth, profileValidator.createValidation, profileController.updateProfile);

//Route pour récupérer les infos d'un profil
profile_router.get(Routing.profile.get.self, profileController.getProfile);

//Route pour supprimer son profil
profile_router.delete(Routing.profile.delete.self, isAuth, profileController.deleteProfile);

//Route pour récupérer les posts d'un profil
profile_router.get(Routing.profile.posts.self, profileController.getAllPosts);

module.exports = profile_router;