const express = require('express');
let opportunitySearch_router = express.Router();
const Routing = require('./routingMap');

const postController = require('../controllers/Post/postController');
const postValidation = require('../controllers/Post/postValidator');
const isAuth = require('../middlewares/is-auth');

opportunitySearch_router.post(Routing.searchOpportunity.get.self, isAuth, postValidation.searchByOpportunity, postController.getByOpportunity);

//delete a post
module.exports = opportunitySearch_router;