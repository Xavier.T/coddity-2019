const express = require('express')
let user_router = express.Router()
const Routing = require('./routingMap')

const isAuth = require('../middlewares/is-auth')
const userController = require("../controllers/User/userController");

user_router.get(Routing.user.info.self, isAuth, userController.getUserData);

user_router.delete(Routing.user.delete.self, isAuth, userController.deleteUser);

module.exports = user_router;