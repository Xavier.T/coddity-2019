const express = require('express');
let post_router = express.Router();
const Routing = require('./routingMap');

const postController = require('../controllers/Post/postController');
const postValidator = require('../controllers/Post/postValidator');
const isAuth = require('../middlewares/is-auth');

//Create a new post
post_router.post(Routing.post.create.self, isAuth, postValidator.createValidation, postController.createPost)

//Update an existing post
post_router.put(Routing.post.update.self, isAuth, postValidator.createValidation, postController.updatePost)

//get random post
post_router.get(Routing.post.random.self, postController.getRandom);

//get a post
post_router.get(Routing.post.get.self, postController.getPost)

//delete a post
post_router.delete(Routing.post.delete.self, isAuth, postController.deletePost)

//get last comments
post_router.get(Routing.post.comments.self, isAuth, postController.getComments)


module.exports = post_router;