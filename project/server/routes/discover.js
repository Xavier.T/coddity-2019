const express = require('express');
let discover_router = express.Router();
const Routing = require('./routingMap');

const discoverController = require('../controllers/Discover/discoverController');

discover_router.get(Routing.discover.timeInfo.self, discoverController.getTimeInfo);
discover_router.get(Routing.discover.discoverPost.self, discoverController.getCurrentPost);

//delete a post
module.exports = discover_router;