const express = require('express');
let like_router = express.Router();
const Routing = require('./routingMap');
const isAuth = require('../middlewares/is-auth');
const likeController = require('../controllers/Like/likeController');

like_router.get(Routing.like.getAll.self, isAuth, likeController.getAll);
like_router.get(Routing.like.getNotifications.self, isAuth, likeController.getNotifications)

like_router.post(Routing.like.create.self, isAuth, likeController.create);
like_router.post(Routing.like.remove.self, isAuth, likeController.remove);
like_router.post(Routing.like.get.self, isAuth, likeController.get);
like_router.post(Routing.like.update.self, isAuth, likeController.update);

//delete a post
module.exports = like_router;