const express = require('express');
let comment_router = express.Router();
const Routing = require('./routingMap');

const commentController = require('../controllers/Comment/commentController');
const commentValidator = require('../controllers/Comment/commentValidator');
const isAuth = require('../middlewares/is-auth');

comment_router.post(Routing.comment.create.self, isAuth, commentValidator.createValidation, commentController.createComment);

//delete a post
module.exports = comment_router;