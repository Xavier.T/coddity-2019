const Routing = require('./routingMap')
const AuthRoute = require('./auth');
const UserRoute = require('./user');
const ProfileRoute = require('./profile');
const PostRoute = require('./post');
const OpportunityRoute = require('./opportunity');
const CommentRoute = require('./comment');
const DiscoverRoute = require('./discover');
const OpportunitySearchRoute = require('./opportunitySearch');
const LikeRoute = require('./like');
//const path = require('path')

module.exports = (app) => {

    //Authentification routes
    app.use(Routing.auth.self, AuthRoute);
    app.use(Routing.user.self, UserRoute);
    app.use(Routing.profile.self, ProfileRoute);
    app.use(Routing.post.self, PostRoute);
    app.use(Routing.opportunity.self, OpportunityRoute);
    app.use(Routing.comment.self, CommentRoute);
    app.use(Routing.discover.self, DiscoverRoute);
    app.use(Routing.like.self, LikeRoute);
    app.use(Routing.searchOpportunity.self, OpportunitySearchRoute);
} 