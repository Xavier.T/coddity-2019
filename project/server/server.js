const express = require('express')

const Routes = require('./routes/routes')
const bodyParser = require('body-parser')
const Db = require('./util/database');
const ModelRelations = require('./model/relation');
const filling = require('./model/startFill/main');
const scripts = require('./controllers/Scripts/run');
const helmet = require('helmet');

//Setup app
const app = express()
//Protecting the app
app.use(helmet());

//Avoid CORS errors
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
  });

//Parse json requests
app.use(bodyParser.json())

//Setup routes and error middleware
Routes(app)
app.use((err, req, res, next) => {

    if(!err.statusCode){
        console.log("ERREUR EXTERNE : ", err);
        res.status(500).json({errors: ["Nous ne pouvons accéder à votre requête suite à un disfonctionnement du serveur"]});
    }
    else{
        console.log("ERREURS INTERNES : ", err.messages);
        res.status(err.statusCode).json( { errors : err.messages } );
    }
    
})

ModelRelations.setupRelations();
Db.sync()
.then(() => {
    console.log("Synchronisation effectuée !")
    //Filling tables with preset
    return filling()
})
.then(scripts)
.then(() => {
    const server = app.listen(process.env.PORT || 8080);
    const io = require('./socket').init(server);
    io.on('connection', socket => {
    })
})
.catch((err) => {
    console.log("Erreur lors de la synchronisation : " + err)
})

 