const Profile = require('./profile');
const User = require('./user');
const Post = require('./post');
const Opportunity = require('./opportunity');
const Comment = require('./comment');
const Like = require('./like')


exports.setupRelations = () => {
    //User and profile
    User.hasOne(Profile, {foreignKey: {allowNull: false, unique: true, onDelete:'cascade'}});
    Profile.belongsTo(User, {foreignKey: {allowNull: false, unique: true, onDelete:'cascade'}});

    //Post and profile
    Profile.hasMany(Post, {foreignKey: {allowNull: false, onDelete:'cascade'}});
    Post.belongsTo(Profile);

    //Post and opportunity
    Post.belongsToMany(Opportunity, {through: "postopportunity", timestamps: false,});
    Opportunity.belongsToMany(Post, {through: "postopportunity", timestamps: false,});

    //User and comment
    User.hasMany(Comment, {foreignKey: {allowNull: false, onDelete: 'cascade'}});
    Comment.belongsTo(User);

    //Post and comment
    Post.hasMany(Comment, {foreignKey: {allowNull: false, onDelete:'cascade'}});
    Comment.belongsTo(Post);

    //User and Post likes
    //Like.belongsToMany()

    User.hasMany(Like, {allowNull: false, onDelete:'cascade', hooks: true});
    Post.hasMany(Like, {allowNull: false, onDelete:'cascade', hooks: true});
    Like.belongsTo(User);
    Like.belongsTo(Post);

}