const Sequelize = require('sequelize')
const sequelize = require('../util/database');

const Comment = sequelize.define('comment', {
    id : {
        type: Sequelize.INTEGER,
        primaryKey : true,
        autoIncrement: true
    },
    text : {
        type: Sequelize.TEXT,
        allowNull: false,
    },
});



module.exports = Comment