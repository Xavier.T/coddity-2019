const Sequelize = require('sequelize')
const sequelize = require('../util/database');
const Post = require('./post');

const Like = sequelize.define('like', {
    id : {
        type: Sequelize.INTEGER,
        primaryKey : true,
        autoIncrement: true
    },
    lastpostversionseen : {
        type: Sequelize.INTEGER,
    },
    lastmessageseen : {
        type: Sequelize.INTEGER,
    }

});


module.exports = Like