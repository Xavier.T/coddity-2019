const Sequelize = require('sequelize')
const sequelize = require('../util/database');

const Profile = sequelize.define('profile', {
    id : {
        type: Sequelize.INTEGER,
        primaryKey : true,
        autoIncrement: true
    },
    firstName : {
        type: Sequelize.STRING,
        allowNull: false,
    },
    lastName : {
        type: Sequelize.STRING,
        allowNull: false,
    },
    sentence : {
        type: Sequelize.STRING,
        allowNull: true,
    },
    description: {
        type: Sequelize.TEXT,
        allowNull: false,
    }
});



module.exports = Profile