const Sequelize = require('sequelize')
const sequelize = require('../util/database');

const User = sequelize.define('user', {
    id : {
        type: Sequelize.INTEGER,
        primaryKey : true,
        autoIncrement: true
    },
    username : {
        type : Sequelize.STRING,
        allowNull: false,
        unique : true
    },
    email : {
        type: Sequelize.STRING,
        allowNull: false,
        unique : true
    },
    password : {
        type : Sequelize.STRING,
        allowNull : false
    },
    sexe : {
        type : Sequelize.CHAR,
        allowNull : false,
    },
    validationToken : {
        type : Sequelize.STRING,
        allowNull : true
    },
    validationTokenExpiration : {
        type : Sequelize.DATE,
        allowNull : true
    },
    resetPasswordToken : {
        type : Sequelize.STRING,
        allowNull: true
    },
    resetPasswordTokenExpiration : {
        type : Sequelize.DATE,
        allowNull : true
    }
})

module.exports = User