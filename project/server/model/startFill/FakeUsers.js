const psw = '$2a$12$k5eMTHpFI/UBmOHMR/xMveHydi/7YUU1sqfq2H42Es7HqA5bfRKsm';
let userId = 1;
let profileId = 1;
let postId = 1;

class FakeUser{

    constructor(
        username,
        email,
        password,
        sexe,
    ){
        this.userId = userId;
        userId+=1;

        this.username = username;
        this.email = email;
        this.password = password;
        this.sexe = sexe;

        this.postsList = [];
    }

    setProfile(firstName, lastName, sentence, description){
        this.profileId = profileId;
        profileId+=1;

        this.firstName = firstName;
        this.lastName = lastName;
        this.sentence = sentence;
        this.description = description;
    }

    addPost(title, description){
        this.postsList.push({
            id: postId,
            title: title,
            description: description,
            version: 1,
        })
        postId+=1;
    }

    generateUserRow(){
        return {
            id: this.userId,
            username: this.username,
            email: this.email,
            password: this.password,
            sexe: this.sexe,
        }
    }

    generateProfileRow(){
        return {
            id: this.profileId,
            firstName: this.firstName,
            lastName: this.lastName,
            sentence: this.sentence,
            description: this.description,
            userId: this.userId,
        }
    }

    generatePostsList(){
        return this.postsList;
    }

}

//Fake utilisateurs
const fakeUsers = [];

//Emmanuelle
const Emmanuelle = new FakeUser('emmanuelle', 'fakemail1@gmail.com', psw, 'F');
Emmanuelle.setProfile('Emmanuelle', 'Coddity', "J'ai toujours veillé à m'éclater dans ce que je faisais ...", 
`
# Ma présentation rapide !

### Cursus scolaire

- DEUG d'économie
- ENSIA (école d'ingénieur en statistique)

### Pourquoi la technologie ?

Je n'ai pas vraiment décidé, les choses se sont faites un peu "comme ça". J'ai toujours veillé à m'éclater dans ce que je faisais au niveau de mes études ou de mon métier pour plus tard. Je me suis intéressée à la data parce que je sentais bien que cela devenait un enjeu, et il est vrai que des compétences techniques aident pour appréhender des sujets data vraiment intéressants en termes de volume et de variété de données.

### Pourquoi suis-je sur Tech' it ?

Dans les filières techniques, je pense qu'il manque des projections visibles assez tôt : des perspectives de métiers et des sujets concrets à aborder. Je pense qu'il faut améliorer cette visibilité le plus possible, c'est pour cela que je partage mon expérience sur la plateforme !
`);
Emmanuelle.addPost('Chief Marketing Officer', 'test');

//Ismael
const Ismael = new FakeUser('ismael', 'fakemail2@gmail.com', psw, 'H');
Ismael.setProfile('Ismael', 'Coddity', 'Les débuts de l\'internet grand public, c\était génial !', 
`
# Ma présentation rapide !

### Mon parcours

- Grande école d'ingénieur
- Conseil pendant une dizaine d'année d'équipes qui faisaient du logiciel
- CTO à Amazing Content

### Pourquoi la technologie ?

En sortant d'école d'ingénieur, on était dans les débuts de l'internet grand public et je trouvais ça génial. J'adore lire et il y avait une masse incroyable de contenu qui s'ouvrait à moi en quelques clics. C'est ce qui m'attirait.

### Comment suis-je devenu CTO ?

J'ai entendu parlé de ce métier assez rapidement et ça m'attirait. De plus, j'aime le fait de résoudre des problèmes et de créer quelque chose. Ce sont ces aspects pâte à modeler, maquette, DIY, qui me plaisent, le fait de construire quelque chose. Dans mon cas, c'est aussi pour le rythme, j'aime voir que les choses avancent très vite. En début de journée tu as 1000 problèmes à résoudre, et en fin de journée tu en as déjà réglé 900, c'est très gratifiant.
`);

//Louis
const Louis = new FakeUser('louis', 'fakemail3@gmail.com', psw, 'H');
Louis.setProfile('Louis', 'Coddity', 'De forts liens sont à tisser entre la tech et les sciences sociales',
`
# Ma présentation rapide !

### Mon parcours

- Deux ans de classe préparatoire
- Centrale Nantes, spé management de l'économie
- Directeur du développement et de la croissance au sein d'une startup

### Pourquoi la technologie ?

Je me suis longtemps posé la question. Je suis parti un peu à l'aveugle dans la voir royale : Bac S < Prépa < école d'ingé mais sans trop savoir pourquoi. Et en fait, j'ai réalisé que la gymnastique intellectuelle dans la tech était celle qui me correspondait le mieux. Ce que j'aime aujourd'hui, c'est la complexité de la déclinaison de la vision en méthodologie et en un produit, ainsi que la complexité du cycle de vente. 

###  Si je pouvais tout refaire ...

Je pense que je prendrais plus de temps pour travailler les sciences sociales (anthropologie, socio, psycho), parce que je me rends compte que dans tous les grands axes que je couvre dans mon travail, il y a des liens forts à tisser entre la tech et ces sciences.
`);

//Lucie
const Lucie = new FakeUser('lucie', 'fakemail4@gmail.com', psw, 'F');
Lucie.setProfile('Lucie', 'Coddity', 'Joindre utile et agréable dans le travail est indispensable', 
`
# Ma présentation rapide !

### Mon parcours

- Science Po pendant 5 ans, dont 2 ans de master en journalisme
- Journaliste au Figaro depuis 6 ans au sein de la rubrique média et technologies

### Technologie et journalisme...

J’ai toujours été passionnée de technologies. J’étais également très forte en mathématiques et en écriture. Finalement, le journalisme m’intéressait et j’ai fait un parcours assez classique : cinq ans d'études à Sciences Po, qui comprenaient deux ans de master en journalisme. Pendant mes études, j’ai conservé cette passion pour les nouvelles technologies: j’allais beaucoup sur des forums, je trifouillais les ordinateurs, etc. En devenant journaliste, j’ai remarqué qu’il y avait une réelle demande de la part des médias généralistes de traiter davantage ce genre de sujets et qu’il y avait peu de journalistes qui s’y connaissaient suffisamment. J’ai donc pu joindre “l’utile à l’agréable” et me spécialiser dans ce domaine. 
`);

//Marie
const Marie = new FakeUser('marie', 'fakemail5@gmail.com', psw, 'F');
Marie.setProfile('Marie', 'Coddity', 'Longtemps j’ai eu le cliché de l’ingénieur informaticien des années 2000 ...', 
`
# Ma présentation rapide !

### Mon parcours

- Formation d'ingénieur généraliste
- CTO dans une start-up

### Mon arrivée dans la tech

Quand mes parents ont eu un ordinateur à la maison, j’ai tout de suite trouvé ça cool et j’ai voulu tout explorer. Je faisais partie de ces enfants qui ouvraient tous les dossiers dans l’ordi sans comprendre ce qui se passait ! Cependant, j'ai décidé de m'orienter dans ce milieu assez tard. Longtemps j’ai eu le cliché de l’ingénieur informaticien des années 2000 qui vivait entouré de ses serveurs et de son imprimante.

### Ce que j'aurais aimé savoir grâce à Tech' it

Quand j’étais ado, j’ai cru que c’était trop dur donc je n’ai pas pris cette direction. Plus tard, j’ai cru que j’avais “du retard” irrattrapable pour devenir développeuse, ce qui était faux bien entendu. J’aurais aimé savoir que l’on pouvait s’y mettre à n’importe quel âge, et avoir un aperçu des différents métiers.
`);

//Marie-André
const MarieAndre = new FakeUser('marieandrea', 'fakemail6@gmail.com', psw, 'F');
MarieAndre.setProfile('Marie-Andréa', 'Coddity', 'je pense qu’hommes ou femmes, on a tous une approche différente.', 
`
# Ma présentation rapide !

### Mon parcours

- École d'ingénieur avec option BI (Business Intelligence)
- Travail dans une ESN

### La technologie, une récente histoire

J'ai décidé de m'orienter après le lycée mais sans vraiment m'orienter, je ne savais pas quoi faire. On m'a conseillé de faire une classe préparatoire, ce que j'ai fait, puis j'ai intégré mon école avec la spé BI. Je n'avais jamais entendu parler de mon métier avant ma dernière année d'étude. Celui-ci requiert des qualités techniques et fonctionnelles, ce qui me plaisait beaucoup, je me suis donc lancée. J'ai réalisé que je n'avais pas besoin d'être dans du code tout le temps. On peut avoir beaucoup de relations sociales avec mon poste actuel.
`);

//Mathild
const Mathilde = new FakeUser('mathilde', 'fakemail7@gmail.com', psw, 'F');
Mathilde.setProfile('Mathilde', 'Coddity', "On n’est pas du tout orientées vers ce genre d’études ... Même dans les forums !", 
`
# Ma présentation rapide !

### Mon parcours

- École d'ingénieur
- Consultante junior en cyber sécurité
- Consulting technique autour d'infrastructures à clés publiques

### La tech c'est formidable !

L’informatique est un milieu de passionnés, et lorsqu’on arrive dedans un peu “par hasard”, ou je dirais sur le tard, on démarre de très loin ! Certains camarades développent depuis des années… J’imagine que si je m’étais intéressée plus tôt au domaine, ça aurait été plus simple sur de nombreux aspects. De plus, c’est un peu flou : tout le monde connaît plus ou moins ce qu’est l’informatique mais pas forcément ce qu’on peut y faire concrètement. Et c’est un domaine qui ne donne pas très envie si on ne s’y intéresse pas vraiment. Ce qui m'a motivé à m'orienter dans ce domaine d'activité c'est que j'aime les mathématiques ainsi que l'aspect Do It Yourself de la chose, de bidouiller des solutions.
`);

//Olivia
const Olivia = new FakeUser('olivia', 'fakemail8@gmail.com', psw, 'F');
Olivia.setProfile('Olivia', 'Coddity', "J'aime bien les ordinateurs, j'ai déjà codé un peu en HTML et ça me plaît un petit peu", 
`
# Ma présentation rapide !

### Mon parcours

- Bac S
- IUT informatique
- Développement/Validation de tests fonctionnels

### Ce qui m'a mit sur la voie de la tech

Je l’ai découvert à l’IUT. Venant d’un petit village de campagne, je ne connaissais pas les métiers de l’ingénieur. Pour moi, la classe prépa était quelque chose de grand mais très concurrentiel, alors je n’ai pas voulu tenter. Je n’avais donc aucune idée de la direction que je prenais : quand on n’a peu ou pas d’exemple dans son entourage proche, c’est difficile de savoir ce à quoi s’attendre. Je suis donc entrée à l’IUT en me disant “j ’aime bien les ordinateurs, j’ai déjà codé un peu en HTML, et ça me plait un petit peu” .

### Mes premiers essais

J'ai fait du HTML au début des années 2000, c’était la mode des blogs personnels, où on racontait sa vie. Il n’y avait pas d’outil à l’époque, il fallait développer à la main. Je me souviens que j’essayais de rendre beau mon blog !
`);

//Sirine
const Sirine = new FakeUser('sirine', 'fakemail9@gmail.com', psw, 'F');
Sirine.setProfile('Sirine', 'Coddity', "Toutes les femmes ont un rôle à jouer. Il ne s’agit pas que d’accuser les hommes ...", 
`
# Ma présentation rapide !

### Mon parcours

- École préparatoire à Tunis
- École d'ingénieur en télécom
- Ingénieur d'études et développement

### Pourquoi la tech ?

J’ai suivi une école préparatoire à Tunis puis une école d’ingénieur en télécom. Les cours de développement m’ont beaucoup plu. Avant cela, je ne pensais pas que je pourrais faire du développement, je n’y avais même jamais pensé. Je pense que c’est parce que je ne connaissais pas beaucoup de développeurs dans mon entourage et parce qu’on ne m’avait pas beaucoup parlé de ce métier. J’avais juste l’image de ce que j’avais vu dans des films ou autres, et ça me paraissant être un truc 100% pour des geeks et surtout pour les hommes. Et pourtant, au lycée, je me souviens que j’étais très forte en informatique, en algorithmique… Mais je n’avais jamais pensé à cela comme carrière. C’est bien en école d’ingénieur que j’ai découvert cette voie et décidé de m’y orienter.
`);

//Théo
const Theo = new FakeUser('theo', 'fakemail10@gmail.com', psw, 'H');
Theo.setProfile('Theo', 'Coddity', "Il y a une part de magie dans le dev !", 
`
# Ma présentation rapide

### Mon parcours

- École du web
- Développeur web dans une petite ESN

### Mon initiation au web

Initialement, je n’y connaissais rien et ça ne me venait pas à l’idée de tout savoir. C’est réellement en terminale que j’ai commencé à comprendre, et c’est encore plus tard que j’ai vraiment découvert tout ce qu’on pouvait faire. Et j’ai encore tellement de choses à découvrir.

### La vie professionnelle sans hésiter !

A l’école, c’est la concurrence qui m’a dérangé. On avait un classement par semestre et ca ne me plaisait pas. Certaines semaines étaient très intensives, et ne reflètent pas forcément la réalité du travail. On avait un temps très réduit pour produire des app de qualité. Dans la vie professionnelle, on définit des échéances cohérentes avec le projet et on prépare l’équipe pour faire en sorte d’adopter une bonne communication.
`);

//Véronique
const Veronique = new FakeUser('veronique', 'fakemail11@gmail.com', psw, 'F');
Veronique.setProfile('Veronique', 'Coddity', "Les réactions des hommes et des femmes ne sont pas les même et c’est important d’avoir les deux.", 
`
# Ma présentation rapide

### Mon parcours

- IUT
- 35 ans dans la data
- Consultante architecte data

### Une femme dans la data ?

Quand j’ai fait mes études, dans les années 80s, c’était compliqué de faire un choix complètement réfléchi car il n’y avait pas vraiment d’informatique. Moi j’étais une matheuse, une scientifique depuis le lycée, et donc j’ai commencé par un IUT. J’ai vu que c’était plus le côté technique, scientifique et algorithmique qui m’intéressait, donc j’ai continué dans cette voie-là. Par la suite, dans mon parcours professionnel, j’ai souvent axé mes choix sur la technique. 

J’aurais pu m’orienter vers une formation MIAGE, mais j’ai préféré aller vers la technique. J’ai longtemps hésité entre les télécoms et les bases de données d’ailleurs, passant de l’un à l’autre au fur et à mesure de mes expériences. J’ai finalement retenu les BDD car la discipline ne se résume pas uniquement à la technique : quand on s’occupe des données on s’occupe aussi du fonctionnel.
`);


//Ajouts
fakeUsers.push(Emmanuelle);
fakeUsers.push(Ismael);
fakeUsers.push(Louis);
fakeUsers.push(Lucie);
fakeUsers.push(Marie);
fakeUsers.push(MarieAndre);
fakeUsers.push(Mathilde);
fakeUsers.push(Olivia);
fakeUsers.push(Sirine);
fakeUsers.push(Theo);
fakeUsers.push(Veronique);
module.exports = fakeUsers;