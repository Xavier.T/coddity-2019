const User = require('../user');
const Profile = require('../profile');


const fakeusers = require('./FakeUsers');

const userList = fakeusers.map(fake => {
    return fake.generateUserRow();
})

const profileList = fakeusers.map(fake => {
    return fake.generateProfileRow();
})

const postList = [];
fakeusers.forEach(fake => {
    postList.concat(fake.generatePostsList()); 
})

module.exports = () => {
    return new Promise((resolve, reject) => {

        User.bulkCreate(userList, {logging: false})
        .then(() => {
            return Profile.bulkCreate(profileList, {logging: false})
        })
        .then(() => {
            return 
        })
        .then(resolve)
        .catch(err => {
            console.log("COULDN'T CREATE USERS");
            //console.log(err);
            resolve();
        });
    });
}