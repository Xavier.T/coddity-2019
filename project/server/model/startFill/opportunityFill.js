const Opportunity = require('../opportunity');

const opportunityList = [
    [1, 'Relation client', 'Être en relation directe avec des clients.'],
    [2, 'Management', 'Pouvoir diriger un groupe de personne plus ou moins grand.'],
    [3, 'Télé-Travail', 'Pouvoir travailler depuis chez soi'],
    [4, 'Missions à l\'international', 'Pouvoir travailler à l\'étranger lors de certaines missions'],
    [5, 'Partage de connaissance', 'Devoir pour partager ses connaissances à d\'autres personnes'],
    [6, 'Formation constante', 'Se former est un des aspects les plus important du métier'],
    [7, 'Rédaction', 'Savoir rédiger est plus que nécéssaire pour le métier'],
    [8, 'Horaires libres', 'Vous fixez vos propres horaires, l\'important est que le travail soit fait'],
    [9, 'Création de contenu', 'Créer et partager du contenu est essentiel pour l\'activité'],
    [10, 'Impacter le marché', 'Avoir un impact économique grâce à ses actions sur un secteur donné'],
    [11, 'Négociation', 'Avoir l\'occasion de négocier au travail'],
    [12, 'Analyse stratégique', 'Devoir analyser pour anticiper le futur'],
    [13, 'Veille technologique', 'Être à jour sur les nouvelles technologies'],
    [14, 'Projet d\'envergure', 'Travailler sur un projet pouvant impacter énormément de monde'],
    [15, 'Conception logiciel', 'Concevoir un logiciel en amont du développement'],
    [16, 'Technique', 'Avoir l\'opportunité de maîtriser des concepts technique'],
    [17, 'Faire parler les chiffres', 'Analyser des données est un point fondamental'],
]

module.exports = () => {
    return new Promise((resolve, reject) => {
        const bulk = [];
        opportunityList.forEach((opp) => {
            bulk.push({
                id: opp[0],
                name: opp[1],
                description: opp[2],
            });
        })
        
        Opportunity.bulkCreate(bulk, {logging: false})
        .then(resolve)
        .catch(err => {
            console.log("COULDN'T CREATE OPPORTUNITIES");
            resolve();
        });
    });
}