const Sequelize = require('sequelize')
const sequelize = require('../util/database');

const Opportunity = sequelize.define('opportunity', {
    id : {
        type: Sequelize.INTEGER,
        primaryKey : true,
        autoIncrement: true
    },
    name : {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
    },
    description : {
        type: Sequelize.STRING,
        allowNull: false,
    },
}, {
    timestamps: false,
});



module.exports = Opportunity