const Sequelize = require('sequelize')
const sequelize = require('../util/database');

const Post = sequelize.define('post', {
    id : {
        type: Sequelize.INTEGER,
        primaryKey : true,
        autoIncrement: true
    },
    title : {
        type: Sequelize.STRING,
        allowNull: false,
    },
    description : {
        type: Sequelize.TEXT,
        allowNull: false,
    },
    version : {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 1,
    },
});



module.exports = Post