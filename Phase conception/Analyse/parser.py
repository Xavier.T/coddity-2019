questions = []
answers = []

with open('Analyse_sujet.txt', 'r', encoding='utf-8') as f:
    content = f.read()
    lines = [l.strip() for l in content.split("\n")]
    
    i = 0
    #On atteint la définition des questions
    while lines[i] != 'Questions':
        i += 1
    i += 1
    
    
    #On récupère les questions
    while(lines[i] != ''): 
        questions.append(lines[i])
        answers.append([])
        i += 1
    

    #On récupère les réponses pour chacune
    while i < len(lines)-1:
        try:
            question_category = int(lines[i][0])
            i += 1
            while(lines[i][0] == '-'):
                answers[question_category-1].append(lines[i])
                i+= 1
        except:
            i += 1

with open('Synthese_generee.txt', 'w', encoding='utf-8') as f:
    for i in range(len(questions)):
        f.write(questions[i] + '\n\n')
        for j in range(len(answers[i])):
            f.write(answers[i][j] + '\n')
        f.write('\n\n\n')